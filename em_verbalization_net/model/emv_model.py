import os
from abc import ABC
from argparse import Namespace
from typing import List

import torch
from fairseq import checkpoint_utils
from fairseq.models import CompositeEncoder, register_model, FairseqEncoderDecoderModel, register_model_architecture
from fairseq.models.transformer import EncoderOut as TransformerEncOut
from fairseq.models.transformer import TransformerModel
from torch import nn

from .em import EpisodeDictionary, LstmEmEncoder, \
    LinearEmEncoder, EpisodeAutoDecoder
from .em.em_encoder_base import set_default_embedding_args
from .emv_task import EmvTask

# noinspection PyTypeChecker
_episodes_dict: EpisodeDictionary = None


class SeparateEncoders(CompositeEncoder):

    def forward(self, src_tokens, src_lengths, **kwargs):
        encoder_out = {}
        for key in self.encoders:
            if key == 'src':
                encoder_out['src'] = self.encoders['src'](src_tokens, src_lengths)
            else:
                tokens = kwargs[key + '_tokens']
                lengths = kwargs[key + '_lengths']

                lengths, sort_order = lengths.sort(descending=True)
                tokens = tokens.index_select(0, sort_order)
                _ascending_numbers, reverse_sort_order = sort_order.sort()

                out = self.encoders[key](tokens, lengths)
                encoder_out[key] = self.encoders[key].reorder_encoder_out(out, reverse_sort_order)

        return encoder_out


class BaseEmvModel(FairseqEncoderDecoderModel, ABC):

    @staticmethod
    def add_model_args(em_encoder_class, parser):
        em_encoder_class.add_args(parser)
        parser.add_argument('--print-additional-output', action='store_true', default=False,
                            help='print additional model outputs (use for eval only!)')
        parser.add_argument('--pretrained-episode-autoenc', default=None, type=str, metavar='FILE',
                            help='File to use to initialize episode encoder and decoder weights.'
                                 'The file is expected to be a checkpoint saved '
                                 'from training EpisodeAutoEncoderModel')
        parser.add_argument('--freeze-parameters', type=str, metavar='P1,...',
                            help='Freeze the given parameters, i.e. set their requires_grad flag to False.'
                                 ' Parameters are given as a comma separated list of parameter name prefixes.'
                                 ' Check model.named_parameters() for what keys to use.')

    @staticmethod
    def build_compound_encoder(em_encoder_class, question_encoder, episodes_dict: EpisodeDictionary, args):
        assert episodes_dict is not None
        episode_enc = em_encoder_class.build_model(args, episodes_dict)
        return SeparateEncoders({
            'episode': episode_enc,
            'src': question_encoder,
        })

    @staticmethod
    def build_additional_decoders(args, task: EmvTask):
        D = task.tgt_time_ref_vocab.representation_size

        class TgtTimeRefProjection(nn.Module):

            def __init__(self) -> None:
                super().__init__()
                self.scorer = nn.Linear(args.encoder_embed_dim, 1, bias=False)
                self.softmax = nn.Softmax(dim=1)
                self.output_layer = nn.Linear(args.encoder_embed_dim,
                                              D * 60,
                                              bias=True)

            def forward(self, all_encoder_out):
                speech_encoder_out: TransformerEncOut = all_encoder_out['src']
                speech_enc_tensor: torch.Tensor = speech_encoder_out.encoder_out  # S x B x C
                speech_enc_tensor = speech_enc_tensor.transpose(0, 1)  # B x S x C
                scores = self.scorer(speech_enc_tensor)  # B x S x 1
                weights = self.softmax(scores)  # B x S x 1
                weighted_sum = torch.sum(weights * speech_enc_tensor, dim=1)  # B x C
                B = weighted_sum.size(0)
                return self.output_layer(weighted_sum).reshape(B, D, 60)

        return {
            'episode': EpisodeAutoDecoder(args.episode_enc_output_dim,
                                          task.episodes_dictionary,
                                          only_symbolic_data=getattr(args, 'only_symbolic_data', False),
                                          exclude_symbolic_data=getattr(args, 'exclude_symbolic_data', False)),
            'time_ref': TgtTimeRefProjection(),
        }

    def __init__(self, encoder, decoder, additional_decoders=None, print_additional_output=False):
        super().__init__(encoder, decoder)
        self.additional_decoders = additional_decoders
        self.print_additional_output = print_additional_output
        self._append_to_results_buffer = None

    def get_append_to_results_buffer(self):
        return self._append_to_results_buffer

    def set_append_to_results_buffer(self, value):
        self._append_to_results_buffer = value

    @property
    def additional_decoders(self):
        return self._additional_decoders

    @additional_decoders.setter
    def additional_decoders(self, new):
        assert not hasattr(self, '_additional_decoders') or len(self._additional_decoders) == 0, \
            'Additional decoders should only be set once!'
        self._additional_decoders = new or {}
        for key, module in self.additional_decoders.items():
            self.add_module(f'additional_decoder_{key}', module)

    def init_pretrained_episode_autoencoder(self, args):
        # It's ok to also perform this in eval mode since fairseq loads checkpoint data _after_ task.build_model
        # But sometimes, it may lead to errors because we try to load a pretrained_episode_autoenc from a path
        # which is not valid anymore (arguments are saved as part of the checkpoint). Therefore, pretrained model
        # loading can be explicitly disabled (i.e. to just use the model config from checkpoint afterwards)
        if args.pretrained_episode_autoenc and len(os.getenv('BLOCK_PRETRAINED_EPISODE_AUTOENC_LOADING', '')) == 0:
            checkpoint = checkpoint_utils.load_checkpoint_to_cpu(args.pretrained_episode_autoenc)
            encoder_dict, decoder_dict = {}, {}
            key: str
            for key, value in checkpoint['model'].items():
                if key.startswith('encoder.'):
                    encoder_dict[key[8:]] = value
                elif key.startswith('auto_decoder.'):
                    decoder_dict[key[13:]] = value

            encoder: nn.Module = self.encoder.encoders['episode']
            decoder: nn.Module = self.additional_decoders['episode']
            encoder.load_state_dict(encoder_dict)
            decoder.load_state_dict(decoder_dict)
            print(f'| Loaded pretrained episode '
                  f'encoder & decoder from {args.pretrained_episode_autoenc}')

    def freeze_model_params(self, args):
        freeze: List[str] = list(filter(lambda s: len(s.strip()) > 0,
                                        getattr(args, 'freeze_parameters', '').split(',')))
        assert all(any(param.startswith(to_freeze) for param, v in self.named_parameters()) for to_freeze in freeze)
        for name, p in self.named_parameters():
            p.requires_grad &= not any(name.startswith(u) for u in freeze)

        for name, p in self.named_parameters():
            if p.requires_grad:
                # print(name)
                pass

    def forward(self, src_tokens, src_lengths, prev_output_tokens, **kwargs):
        encoder_out = self.encoder(src_tokens, src_lengths=src_lengths, **kwargs)
        speech_decoder_out = self.forward_decoder(prev_output_tokens,
                                                  encoder_out=encoder_out, **kwargs)
        outputs = {
            'answer': speech_decoder_out[0]
        }
        for key, decoder in self.additional_decoders.items():
            if key in encoder_out:
                decoder_in = encoder_out[key]
            else:
                decoder_in = encoder_out
            outputs[key] = decoder(decoder_in)
            if self.print_additional_output:
                print(f'Model {key} output: {outputs[key]}')

        # To implement semantic evaluation during valid run
        if self.get_append_to_results_buffer():
            bsz = src_tokens.size(0)
            for i in range(bsz):
                self.get_append_to_results_buffer()((src_tokens[i],
                                                     prev_output_tokens[i],
                                                     speech_decoder_out[0][i].max(dim=1).indices))

        return (outputs, *speech_decoder_out[1:])

    def _cat_padding_masks(self, mask1_BxS1, mask2_S2xB: torch.Tensor, seq1_length, seq2_length, batch_size):
        # padding masks have size B x S1 / S2 x B
        # output is B x (S1 + S2)
        device = next(self.parameters()).device
        if mask1_BxS1 is None and mask2_S2xB is None:
            return None
        elif mask1_BxS1 is None:
            mask1_BxS1 = torch.zeros(batch_size, seq1_length, dtype=torch.bool, device=device)
        elif mask2_S2xB is None:
            mask2_S2xB = torch.zeros(seq2_length, batch_size, dtype=torch.bool, device=device)
        return torch.cat((mask1_BxS1, mask2_S2xB.t()), dim=1)

    def forward_decoder(self, prev_output_tokens, **kwargs):
        encoder_out: dict = kwargs['encoder_out']
        episodes_out: torch.Tensor
        lstm_out = encoder_out['episode']
        episodes_out, episodes_hidden, episodes_cell = lstm_out['encoder_out']
        episodes_padding_mask = lstm_out['encoder_padding_mask']
        question_enc: TransformerEncOut = encoder_out['src']

        decoder_in = TransformerEncOut(
            encoder_out=torch.cat((question_enc.encoder_out, episodes_out), dim=0),
            encoder_padding_mask=self._cat_padding_masks(
                question_enc.encoder_padding_mask, episodes_padding_mask,
                seq1_length=question_enc.encoder_out.size(0),
                seq2_length=episodes_out.size(0),
                batch_size=episodes_out.size(1)
            ),
            encoder_embedding=question_enc.encoder_embedding,
            encoder_states=question_enc.encoder_states
        )
        kwargs_copy = dict(kwargs)
        del kwargs_copy['encoder_out']
        decoder_out, attn = self.decoder(prev_output_tokens, encoder_out=decoder_in, **kwargs_copy)
        # Not returning the attention data is a dirty fix to make evaluation working
        # Otherwise, the sequence generator is confused because we extend the decoder input size,
        # thus the decoder attention dimension is bigger than the input sentence.
        # The downside of this is that we cannot print alignments in eval mode
        return decoder_out, None


@register_model('prototype_emv')
class PrototypeEmvModel(BaseEmvModel, TransformerModel):

    @staticmethod
    def add_args(parser):
        TransformerModel.add_args(parser)
        BaseEmvModel.add_model_args(LstmEmEncoder, parser)

    @classmethod
    def build_model(cls, args, task):
        global _episodes_dict
        _episodes_dict = task.episodes_dictionary
        model: BaseEmvModel = super().build_model(args, task)
        model.print_additional_output = args.print_additional_output
        model.additional_decoders = super().build_additional_decoders(args, task)
        model.init_pretrained_episode_autoencoder(args)
        model.freeze_model_params(args)
        return model

    @classmethod
    def build_encoder(cls, args, src_dict, embed_tokens):
        question_encoder = super().build_encoder(args, src_dict, embed_tokens)
        return super().build_compound_encoder(
            args.em_enc_class,
            question_encoder, _episodes_dict, args)


@register_model_architecture('prototype_emv', 'prototype_emv_linear_architecture')
def prototype_emv_linear_architecture(args: Namespace):
    set_default_embedding_args(args)
    args.episode_enc_hidden_dim = getattr(args, 'episode_enc_hidden_dim', 32)
    args.episode_enc_output_dim = getattr(args, 'episode_enc_output_dim',
                                          getattr(args, 'encoder_embed_dim', 512))
    transformer_dropout = getattr(args, 'dropout', 0.15)
    args.episode_enc_dropout = getattr(args, 'episode_enc_dropout',
                                       transformer_dropout)
    args.em_enc_class = LinearEmEncoder


@register_model_architecture('prototype_emv', 'prototype_emv_lstm_architecture')
def prototype_emv_lstm_architecture(args: Namespace):
    prototype_emv_linear_architecture(args)
    args.em_enc_class = LstmEmEncoder
    args.episode_enc_embed_dim = getattr(args, 'episode_enc_embed_dim', 32)
    args.episode_enc_num_layers = getattr(args, 'episode_enc_num_layers', 2)
