import sys
from typing import Dict, Tuple, List, Any

import torch
from fairseq import utils
from fairseq.criterions import FairseqCriterion
from torch import nn, Tensor

_already_logged_no_target_for_model_output_keys = set()


class _FakeModel(nn.Module):

    def __init__(self, model, net_out, target):
        super().__init__()
        self.model = model
        self.net_out = net_out
        self.target = target

    def forward(self, **unused):
        return self.net_out

    def get_normalized_probs(self, net_output, log_probs, sample=None):
        return self.model.get_normalized_probs(net_output, log_probs, sample=sample)

    def get_targets(self, *unused):
        return self.target

    @property
    def decoder(self):
        return self.model.decoder


class CombinedLoss(FairseqCriterion):

    @staticmethod
    def add_args_for(parser, underlying_names: List[str]):
        for name in underlying_names:
            parser.add_argument('--loss-weight-' + name.replace('_', '-'),
                                default=1 / len(underlying_names), type=float, metavar='W',
                                help=f'Weight of {name} loss')

    def __init__(self, args, task, underlying_criteria: Dict[str, FairseqCriterion],
                 weights: Dict[str, float] = None):
        super().__init__(args, task)
        self.underlying_criteria = underlying_criteria
        self.weights = weights or {k: 1 / len(underlying_criteria)
                                   for k in underlying_criteria.keys()}
        assert self.weights.keys() == self.underlying_criteria.keys()
        assert abs(sum(self.weights.values()) - 1.0) < 0.00001, str(self.weights)

    def forward(self, model, sample, reduce=True):
        sample = dict(sample)
        net_outputs: Tuple[Dict[str, Tensor], ...] = model(**sample['net_input'])
        targets: Dict[str, Tensor] = sample['targets']

        first_key = next(iter(net_outputs[0].keys()))
        bsz = targets[first_key].size(0)
        loss = net_outputs[0][first_key].new(1 if reduce else bsz).float().zero_()

        sample_size = 0
        logging_outputs = {}
        for key, o in net_outputs[0].items():
            if key not in targets:
                if key not in _already_logged_no_target_for_model_output_keys:
                    print(f'No target for model output '
                          f'"{key}" = {o.shape if torch.is_tensor(o) else o}, '
                          f'ignoring from now on.')
                    _already_logged_no_target_for_model_output_keys.add(key)
                continue
            if self.weights[key] == 0:
                continue
            t = targets[key]
            m = _FakeModel(model, (o, *net_outputs[1:]), t)
            sample['target'] = t
            sample['targets'] = t  # in case there is a nested combined loss
            try:
                l, ss, sub_log = self.underlying_criteria[key](m, sample, reduce)
            except:
                print('Exception in sub-loss', key, file=sys.stderr)
                raise
            logging_outputs[key] = sub_log
            loss += l * self.weights[key]
            sample_size += ss

        sample_size /= len(targets)

        logging_output = self._flatten(logging_outputs)
        logging_output['loss'] = utils.item(loss.data) if reduce else loss.data
        return loss, sample_size, logging_output

    @staticmethod
    def _unflatten(dictionary: Dict[str, Any]) -> Dict[str, Dict[str, Any]]:
        def _insert(d: Dict, keys, v):
            if len(keys) == 1:
                d[keys[0]] = v
            else:
                d.setdefault(keys[0], {})
                _insert(d[keys[0]], keys[1:], v)

        result: Dict[str, Dict[str, Any]] = {}
        for key, value in dictionary.items():
            _insert(result, key.split('.'), value)
        return result

    def _flatten(self, dicts: Dict[str, Dict[str, Any]], simplify=False) -> Dict[str, Any]:
        result = {
            'loss': 0.,
            'ntokens': 0,
            'nsentences': 0,
            'sample_size': 0,
        }
        special_keys = set(result.keys())
        for p in special_keys:
            first = next(iter(dicts.values()))[p]
            # Avoid rounding errors due to multiplication with weights if all share the same value
            if all(d[p] == first for d in dicts.values()):
                result[p] = first
            else:
                for key, dictionary in dicts.items():
                    result[p] += self.weights[key] * dictionary[p]

        for key, dictionary in dicts.items():
            for sub_key, value in dictionary.items():
                if simplify and sub_key in special_keys and value == result[sub_key]:
                    # No need to repeat something like "nsentences" if its the same for every sub-loss
                    continue
                result[f'{key}.{sub_key}'] = value

        return result

    def aggregate_logging_outputs(self, logging_outputs):
        unflattened = [CombinedLoss._unflatten(o) for o in logging_outputs if len(o) > 0]
        aggregated = {}
        for key, criterion in self.underlying_criteria.items():
            if all(key not in o for o in unflattened):
                # This criterion is optional and was not present in this run
                continue
            assert all(key in o for o in unflattened)
            logs = [o[key] for o in unflattened]
            if isinstance(criterion, CombinedLoss):
                aggregated[key] = criterion.aggregate_logging_outputs(logs)
            else:
                aggregated[key] = criterion.__class__.aggregate_logging_outputs(logs)
        flatten = self._flatten(aggregated, simplify=True)
        return flatten
