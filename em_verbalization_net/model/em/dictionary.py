import json
from datetime import datetime
from pathlib import Path

import torch
from fairseq.data import Dictionary
from typing import List, Dict

_EPS = 0.00001
_REF_YEAR = 2020
_EPISODE_STATUS_DICT = {
    'success': 0,
    'abort': 1,
    'failure': 2,
}
_EPISODE_STATUS_DICT_REVERSE = {
    0: 'success',
    1: 'abort',
    2: 'failure'
}
_ACTION_STATUS_DICT = {
    'started': 0,
    'success': 1,
    'failure': 2,
}
_ACTION_STATUS_DICT_REVERSE = {
    0: 'started',
    1: 'success',
    2: 'failure'
}
_POSE_TRACKING_POINTS = [
    'LAnkle', 'LBigToe', 'LEar', 'LElbow', 'LEye', 'LHeel', 'LHip', 'LKnee', 'LShoulder', 'LSmallToe',
    'LWrist', 'MidHip', 'Neck', 'Nose', 'RAnkle', 'RBigToe', 'REar', 'RElbow', 'REye', 'RHeel', 'RHip',
    'RKnee', 'RShoulder', 'RSmallToe', 'RWrist'
]
_KINEMATIC_UNIT_JOINTS_ARMAR3 = [
    'Cameras', 'Elbow L', 'Elbow R', 'Eye_Left', 'Eye_Right', 'Hand Palm 2 L', 'Hand Palm 2 R', 'Head_Tilt',
    'Hip Pitch', 'Hip Roll', 'Hip Yaw', 'Index L J0', 'Index L J1', 'Index R J0', 'Index R J1', 'Middle L J0',
    'Middle L J1', 'Middle R J0', 'Middle R J1', 'Neck_1_Pitch', 'Neck_2_Roll', 'Neck_3_Yaw', 'Pinky L J0',
    'Pinky L J1', 'Pinky R J0', 'Pinky R J1', 'Ring L J0', 'Ring L J1', 'Ring R J0', 'Ring R J1', 'Shoulder 1 L',
    'Shoulder 1 R', 'Shoulder 2 L', 'Shoulder 2 R', 'Thumb L J0', 'Thumb L J1', 'Thumb R J0', 'Thumb R J1',
    'Underarm L', 'Underarm R', 'Upperarm L', 'Upperarm R', 'VirtualCentralGaze', 'Wrist 1 L', 'Wrist 1 R', 'Wrist 2 L',
    'Wrist 2 R'
]


class EpisodeDictionary(Dictionary):

    @staticmethod
    def tokenize_episode_json(line):
        try:
            episode_list = json.loads(line)
            symbols = []
            for episode in episode_list:
                symbols.append(episode['goal'])
                for key_frame in episode['keyframes'].values():
                    if 'actions' in key_frame:
                        for action in key_frame['actions']:
                            name = action['name']
                            symbols += name.split()
                    if 'objects' in key_frame:
                        for obj in key_frame['objects']:
                            symbols.append(obj['name'])
            return symbols
        except:
            print('Failed to tokenize line', line)
            raise

    def __init__(self,
                 img_embedding_file: Path = None,
                 pad='<pad>', eos='</s>', unk='<unk>', bos='<s>', extra_special_symbols=None):
        super().__init__(pad, eos, unk, bos, extra_special_symbols)
        if isinstance(img_embedding_file, str):
            img_embedding_file = Path(img_embedding_file)
        self.img_embedding_file = img_embedding_file
        self.img_embeddings = None

    def _get_meta(self):
        if self.img_embedding_file:
            return ['#img_embedding_file'], [str(self.img_embedding_file)],
        else:
            return [], []

    def _load_meta(self, lines):
        header: str = lines[0]
        if header.startswith('#img_embedding_file'):
            self.img_embedding_file = Path(header[header.find(' '):].strip())
            return 1
        else:
            return 0

    def lazy_load_img_embeddings(self):
        if self.img_embedding_file and not self.img_embeddings:
            assert self.img_embedding_file.is_file(), self.img_embedding_file
            print('| Loading image embeddings from "' + str(self.img_embedding_file) + '" ...', end=' ', flush=True)
            self.img_embeddings = torch.load(self.img_embedding_file, map_location='cpu')
            print(len(self.img_embeddings), 'entries', flush=True)
        return self.img_embeddings is not None

    def encode_line(self, line: str, line_tokenizer=None, add_if_not_exist=True,
                    consumer=None, append_eos=False, reverse_order=False) -> torch.FloatTensor:
        episode_list: List[Dict] = json.loads(line)

        n_frames = sum(len(e['keyframes']) for e in episode_list)
        # noinspection PyArgumentList
        out = torch.FloatTensor(n_frames + len(episode_list) if append_eos else n_frames, self.representation_size)

        i = 0
        for episode in episode_list:
            keyframes = episode['keyframes']
            episode_goal = episode.get('goal')
            episode_status = episode.get('status')
            for timestamp, key_frame in keyframes.items():
                self._set_keyframe_representation(timestamp, key_frame, episode_goal, episode_status, out[i])
                i += 1
            if append_eos:
                out[i].fill_(self.eos())
                i += 1
        return out

    def _set_keyframe_representation(self, timestamp: str, keyframe: dict,
                                     episode_goal: str, episode_status: str, out: torch.Tensor):
        def to_tensor(_list):
            return torch.tensor(_list, dtype=torch.float, device=out.device)

        abs_time = datetime.strptime(timestamp, '%Y %m %d %w %H %M %S')

        # Datetime info
        out[0] = abs_time.weekday()
        out[1] = abs_time.year - _REF_YEAR
        out[2] = abs_time.month
        out[3] = abs_time.day
        out[4] = abs_time.hour
        out[5] = abs_time.minute
        out[6] = abs_time.second

        goal_idx = 7
        episode_status_idx = goal_idx + 1
        action_start = episode_status_idx + 1
        action_status_idx = action_start + 6
        action_end = action_start + 7
        objects_symb_start = action_end
        objects_subsymb_start = objects_symb_start + 4
        platform_start = objects_subsymb_start + 6
        platform_last = platform_start + 6
        pose_start = platform_last + 1
        pose_end = pose_start + len(_POSE_TRACKING_POINTS) * 7
        joints_start = pose_end
        joints_end = joints_start + len(_KINEMATIC_UNIT_JOINTS_ARMAR3) * 7
        img_start = joints_end
        img_end = img_start + (64 if self.img_embedding_file else 1)

        # Some index consistency checks
        assert img_end == self.representation_size
        assert self.datetime_slice.stop == goal_idx
        assert self.symbolic_scene_info_slice.start == goal_idx
        assert self.symbolic_scene_info_slice.stop == objects_subsymb_start
        assert self.subsymbolic_info_slice.start == objects_subsymb_start
        assert self.subsymbolic_info_slice.stop == joints_end
        assert self.latent_data_slice.start == img_start
        assert self.latent_data_slice.stop == img_end
        ranges = [(0, goal_idx), (goal_idx, action_start), (action_start, action_end),
                  (objects_symb_start, objects_subsymb_start),
                  (objects_subsymb_start, platform_start), (platform_start, platform_last + 1),
                  (pose_start, pose_end), (joints_start, joints_end), (img_start, img_end)]
        assert all(len([r for r in ranges if r[0] <= i < r[1]]) == 1
                   for i in range(self.representation_size))

        out[goal_idx] = self.index(episode_goal) if episode_goal else self.pad()
        if out[goal_idx] == self.unk():
            print(f'Warning! Replaced goal by unk! "{episode_goal}"')
        out[episode_status_idx] = _EPISODE_STATUS_DICT[episode_status] if episode_status else self.pad()

        # Actions (only one slot till now)
        actions = keyframe.get('actions', [])
        if len(actions) > 0:
            action = actions[0]
            action_and_args = action['name'].split()
            for j in range(0, 6):  # Allow action name + up to 5 arguments per action
                if j >= len(action_and_args):
                    out[action_start + j] = self.pad()
                else:
                    out[action_start + j] = self.index(action_and_args[j])
            out[action_status_idx] = _ACTION_STATUS_DICT[action['status']]
        else:
            out[action_start:action_end] = self.pad()

        # Objects (two slots per key frame)
        # First start with the symbolic information of both object slots
        # Then add both sub-symbolic object positions (to better separate both parts)
        objects = keyframe.get('objects', [])
        objects += [None for i in range(len(objects), 2)]
        for i, obj in enumerate(objects[:2]):
            if obj is None:
                out[objects_symb_start + i * 2] = self.pad()
                out[objects_symb_start + i * 2 + 1] = self.pad()
                out[objects_subsymb_start + i * 3: objects_subsymb_start + i * 3 + 3] = self.pad()
            else:
                out[objects_symb_start + i * 2] = self.index(obj['name'])
                out[objects_symb_start + i * 2 + 1] = 0 if obj['type'] == 'updated' else 1
                out[objects_subsymb_start + i * 3: objects_subsymb_start + i * 3 + 3] = to_tensor(obj['position'])

        # Platform
        platform_pos = keyframe.get('platform', [])
        out[platform_start:platform_last] = to_tensor(platform_pos if len(platform_pos) == 6 else [0] * 6)
        out[platform_last] = 1 if 'platform' in keyframe else 0

        # OpenPose
        poses = keyframe.get('poses', {})
        for i, point in enumerate(_POSE_TRACKING_POINTS):
            base = pose_start + i * 7
            if point in poses:
                out[base] = poses[point]['confidence']
                out[base + 1: base + 4] = to_tensor(poses[point]['local'])
                out[base + 4: base + 7] = to_tensor(poses[point]['global'])
            else:
                out[base: base + 7] = 0

        # Robot Joints
        joints = keyframe.get('joints', {})
        for i, joint in enumerate(_KINEMATIC_UNIT_JOINTS_ARMAR3):
            base = joints_start + i * 7
            if joint in joints:
                out[base: base + 7] = to_tensor(joints[joint])
            else:
                out[base: base + 7] = 0

        # Image embedding, if present
        if self.lazy_load_img_embeddings() and 'img_id' in keyframe:
            out[img_start:img_end] = self.img_embeddings[keyframe['img_id']]
        else:
            out[img_start:img_end] = 0

        # TODO add speech

        # If adding something, don't forget to change representation_size below!

    def string(self, tensor, bpe_symbol=None, escape_unk=False):
        assert torch.is_tensor(tensor) and tensor.dim() == 2 and tensor.size(1) == self.representation_size, \
            f'Tensor should be SeqLength x RepresentationSize, is {tensor.size()}'
        return '[\n\t' + ',\n\t'.join(self._decode_keyframe(frame) for frame in tensor) + '\n]'

    def _decode_keyframe(self, frame: torch.FloatTensor) -> str:
        if all(abs(i - self.eos()) < _EPS for i in frame):
            return '<eos_frame>'
        if all(abs(i - self.pad()) < _EPS for i in frame):
            return '<pad_frame>'

        def item(i) -> int:
            return round(frame[i].item())

        def ritem(i) -> float:
            return round(frame[i].item(), ndigits=2)

        time_string = str(item(1) + _REF_YEAR) + ' ' + ' '.join(
            str(item(i)) if item(i) >= 10 else f'0{item(i)}'
            for i in range(2, 7)
        )

        result = ''
        if frame[7] == self.pad():
            result += ' <no goal>'
        else:
            result += f' <goal {item(7)}>'
        result += ' e:' + _EPISODE_STATUS_DICT_REVERSE.get(item(8), 'inv')
        if frame[9] == self.pad():
            result += ' <no action>'
        else:
            for j in range(9, 15):  # Action
                if frame[j] == self.pad():
                    break
                elif frame[j] < 0:
                    result += ' <inv>'
                else:
                    result += ' ' + self[item(j)]
            result += ' ' + _ACTION_STATUS_DICT_REVERSE.get(item(15), 'inv')
        for obj in range(2):
            symb_base = 16 + obj * 2
            if frame[symb_base] == self.pad():
                break
            result += ' ' + self[item(symb_base)]
            result += (' new' if item(symb_base + 1) == 1 else ' upd')
            pos_base = 20 + obj * 3
            result += f' @({ritem(pos_base)}, {ritem(pos_base + 1)}, {ritem(pos_base + 2)})'
        if item(32) == 1:
            result += f' platform@({ritem(26)}, {ritem(27)}, {ritem(28)})'
        if any(abs(e) > _EPS for e in frame[33:208]):
            result += ' <pose information>'
        if any(abs(e) > _EPS for e in frame[208:537]):
            result += ' <joints information>'
        return time_string + result

    def compare(self, hypo, target, print_sample=False, normalized_datetimes=False):
        assert all(k in h
                   for k in ['symbolic_scores', 'datetime_scores', 'subsymbolic_reconstruction']
                   for h in [hypo, target]), f'{hypo.keys()}, {target.keys()}'
        batch_size = target['symbolic_scores'].size(0)
        seq_len = target['symbolic_scores'].size(1)

        goal_score, episode_status_score = 0, 0
        action_score, arguments_score, arguments_count, action_status_score = 0, 0, 0, 0
        object_score, object_status_score = 0, 0
        days_diff, minutes_diff = 0, 0
        dt_scores = [0, 0, 0, 0, 0, 0]
        subsymb_reconstruction_difference = 0
        latent_reconstruction_difference = 0
        for b in range(batch_size):
            for i in range(seq_len):
                symb_info_hypos = hypo['symbolic_scores'][b, i].max(dim=1).indices
                symb_info_targets = target['symbolic_scores'][b, i]

                if symb_info_hypos[0] == symb_info_targets[0]:
                    goal_score += 1
                if symb_info_hypos[1] == symb_info_targets[1]:
                    episode_status_score += 1

                if symb_info_hypos[2] == symb_info_targets[2]:
                    action_score += 1
                for j in range(3, 8):
                    if symb_info_hypos[j] == self.pad() and symb_info_targets[j] == self.pad():
                        break
                    arguments_count += 1
                    if symb_info_hypos[j] == symb_info_targets[j]:
                        arguments_score += 1
                if symb_info_hypos[8] == symb_info_targets[8]:
                    action_status_score += 1

                if symb_info_hypos[9] == symb_info_targets[9]:
                    object_score += 1
                if symb_info_hypos[11] == symb_info_targets[11]:
                    object_score += 1
                if symb_info_hypos[10] == symb_info_targets[10]:
                    object_status_score += 1
                if symb_info_hypos[12] == symb_info_targets[12]:
                    object_status_score += 1

                hypo_dates = hypo['datetime_scores'][b, i].max(dim=1).indices
                difference = hypo_dates - target['datetime_scores'][b, i]
                difference_in_days = abs(difference[0] * 360 + difference[1] * 30 + difference[2])
                difference_in_mins = abs(difference[3] * 60 + difference[4] + difference[5] / 60)
                days_diff += difference_in_days
                minutes_diff += difference_in_mins
                for k in range(len(dt_scores)):
                    dt_scores[k] += int(difference[k] == 0)

                subsymb_reconstruction_difference += torch.sum(
                    abs(target['subsymbolic_reconstruction'][b, i]
                        - hypo['subsymbolic_reconstruction'][b, i])
                )
                latent_reconstruction_difference += torch.sum(
                    abs(target['latent_reconstruction'][b, i]
                        - hypo['latent_reconstruction'][b, i])
                )

                if b == 0 and print_sample:
                    targets = (
                        'Target', symb_info_targets,
                        target['datetime_scores'][b, i]
                    )
                    outputs = ('Output', symb_info_hypos, hypo_dates)
                    for name, symb_scores, datetime_tensor in [targets, outputs]:
                        print(name, b, i)
                        print('  Goal  :', self[symb_scores[0]])
                        print('  EpStat:', _EPISODE_STATUS_DICT_REVERSE.get(symb_scores[1].item(), 'inv'))
                        print('  Action:', self[symb_scores[2]])
                        print('  AcStat:', _ACTION_STATUS_DICT_REVERSE.get(symb_scores[8].item(), 'inv'))
                        print('  Args  :', ', '.join(self[symb_scores[k]] for k in range(3, 8)))
                        print('  DT    :', ' '.join(str(round(x.item())) for x in datetime_tensor))
        n = seq_len * batch_size
        return {'goal_score': action_score / n,
                'episode_status_score': action_score / n,
                'action_score': action_score / n,
                'action_status_score': action_status_score / n,
                'arguments_score': arguments_score / arguments_count if arguments_count > 0 else 1,
                'object_score': object_score / (2 * n),
                'object_status_score': object_status_score / (2 * n),
                'days_diff': days_diff.item() / n,
                'minutes_diff': minutes_diff.item() / n,
                'year_score': dt_scores[0] / n,
                'month_score': dt_scores[1] / n,
                'day_score': dt_scores[2] / n,
                'hour_score': dt_scores[3] / n,
                'minutes_score': dt_scores[4] / n,
                'second_score': dt_scores[5] / n,
                'subsymb_reconstruction_difference': subsymb_reconstruction_difference.item() / n,
                'latent_reconstruction_difference': latent_reconstruction_difference.item() / n}

    @property
    def representation_size(self):
        return 33 \
               + len(_POSE_TRACKING_POINTS) * 7 \
               + len(_KINEMATIC_UNIT_JOINTS_ARMAR3) * 7 \
               + (64 if self.img_embedding_file else 1)

    @property
    def datetime_slice(self):
        return slice(0, 7)

    @property
    def symbolic_scene_info_slice(self):
        return slice(7, 20)

    @property
    def subsymbolic_info_slice(self):
        return slice(20, 537)

    @property
    def latent_data_slice(self):
        return slice(537, self.representation_size)


class TimeRefDictionary(Dictionary):

    def encode_line(self, line: str, line_tokenizer=None, add_if_not_exist=True,
                    consumer=None, append_eos=False, reverse_order=False) -> torch.IntTensor:
        line = line.strip()
        abs_time = datetime.strptime(line, '%Y %m %d %w %H %M %S')
        out = torch.tensor(
            [
                abs_time.year - _REF_YEAR,
                abs_time.month,
                abs_time.day,
                abs_time.hour,
                abs_time.minute,
                abs_time.second,
                abs_time.weekday(),
            ],
            dtype=torch.int
        )
        # noinspection PyTypeChecker
        return out

    @property
    def representation_size(self):
        return self.encode_line(datetime.now().strftime('%Y %m %d %w %H %M %S')).size(0)
