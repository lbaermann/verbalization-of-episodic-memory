import torch.nn.functional as F
from fairseq.models import FairseqEncoder
from torch import nn

from . import LstmEmEncoder
from .dictionary import EpisodeDictionary
from .em_encoder_base import EmEncoderBase


class LinearEmEncoder(FairseqEncoder, EmEncoderBase):

    @staticmethod
    def add_args(parser):
        EmEncoderBase.add_common_args(parser)

    @staticmethod
    def build_model(args, episodes_dict):
        return LinearEmEncoder(
            episodes_dict,
            **EmEncoderBase.get_constructor_kw_args(args),
            dropout=args.episode_enc_dropout
        )

    def __init__(self, dictionary: EpisodeDictionary,
                 emb_size_dt=32, emb_size_symb=32, emb_size_subsymb=64, emb_size_latent=32,
                 hidden_size=64, internal_output_size=512,
                 output_size=512, dropout=0.1, dropout_symb_emb=0.1):
        assert isinstance(dictionary, EpisodeDictionary)
        assert output_size % internal_output_size == 0
        super().__init__(dictionary)
        self.padding_idx = dictionary.pad()
        self.dropout = dropout
        self.hidden_size = hidden_size
        self.internal_output_size = internal_output_size
        self.output_size = output_size
        self.dropout_symb_emb = dropout_symb_emb
        emb_size = self.create_embedding_layers(emb_size_dt, emb_size_symb, emb_size_subsymb, emb_size_latent)
        self.merge = nn.Linear(emb_size, hidden_size, bias=True)
        self.output = nn.Linear(hidden_size, internal_output_size, bias=False)

    def forward(self, src_tokens, src_lengths=None, **kwargs):
        # src_tokens has shape B x S x R
        src_tokens = src_tokens.type_as(next(self.parameters()))
        embedding = self.embed(src_tokens)
        x = self.input(embedding)
        x = F.leaky_relu(x)
        x = F.dropout(x, p=self.dropout, training=self.training)
        x = self.output(x)
        x = self.stretch_output_if_necessary(x)
        x.transpose_(0, 1)
        return self.finalize_encoder_output(x, src_tokens, final_output=(x, x, x))

    def reorder_encoder_out(self, encoder_out, new_order):
        # noinspection PyCallByClass
        return LstmEmEncoder.reorder_encoder_out(self, encoder_out, new_order)
