import os
from argparse import Namespace
from pathlib import Path
from random import Random
from typing import Dict

import torch
import torch.nn.functional as F
from fairseq import checkpoint_utils
from fairseq.criterions.cross_entropy import CrossEntropyCriterion
from fairseq.models import BaseFairseqModel, register_model, register_model_architecture
from fairseq.options import get_generation_parser, parse_args_and_arch
from fairseq.tasks import FairseqTask, register_task
from torch import nn, multiprocessing

from model.em.datetime_loss import DatetimeLoss
from model.em.em_encoder_base import set_default_embedding_args
from model.em.episode_dataset import EpisodeDataset
from model.em.episode_regularizing_task import EpisodeRegularizingTaskMixin
from model.em.mse_loss import MSELoss
from . import LstmEmEncoder, EpisodeDictionary, LinearEmEncoder
from ..combined_loss import CombinedLoss


def _slen(s: slice):
    return s.stop - s.start


class EpisodeAutoDecoder(nn.Module):

    def __init__(self, hidden_dim, episodes_dict: EpisodeDictionary,
                 only_symbolic_data=False, exclude_symbolic_data=False) -> None:
        super().__init__()
        assert not (only_symbolic_data and exclude_symbolic_data), 'These options are exclusive.'
        self.episodes_dict = episodes_dict
        self.hidden_dim = hidden_dim
        self.only_symbolic_data = only_symbolic_data
        self.exclude_symbolic_data = exclude_symbolic_data
        T, D, I, V, U, L = self._get_dimensions()
        self.datetime_classes_layer = nn.Linear(
            hidden_dim, D * T,
            bias=True)
        if not self.exclude_symbolic_data:
            self.symbolic_classes_layer = nn.Linear(
                hidden_dim, I * V,
                bias=False)
        if not self.only_symbolic_data:
            self.subsymbolic_reconstruction_layer = nn.Linear(
                hidden_dim, U,
                bias=True)
            self.latent_reconstruction_layer = nn.Linear(
                hidden_dim, L,
                bias=True)

    def forward(self, episode_enc):
        """
        Returns dictionary with results:
        'symbolic_scores': Tensor of dimension B x S x I x V : Scores for NLLLoss for symbolic data
        'datetime_scores': Tensor of dimension B x S x D x T : Absolute Datetime scores (including weekdays)
        'subsymbolic_reconstruction': Tensor of dimension B x S x U : subsymbolic data reconstruction
        where
        B = Batch size
        S = Sequence length (of symbolic EM)
        I = Number of symbolic information per sequence (action + objects)
        V = symbolic EM vocabulary size
        D = Vector size for datetime representation
        U = sUb-symbolic vector size
        L = latent data vector size
        T = 60 (number of possible classes for datetime fields)
        Note that R = I + D + U, where R is the representation size of the EpisodeDictionary
        """
        hidden_state = episode_enc['encoder_out'][0]  # S x B x H

        if hidden_state.size(-1) > self.hidden_dim:
            assert hidden_state.size(-1) % self.hidden_dim == 0
            # Trim repeated tensor again
            hidden_state = hidden_state[:, :, :self.hidden_dim]

        h = hidden_state.transpose(0, 1)  # B x S x H
        B = h.size(0)
        S = h.size(1)
        T, D, I, V, U, L = self._get_dimensions()
        datetime_scores = self.datetime_classes_layer(h).reshape(B, S, D, T)

        if self.exclude_symbolic_data:
            symbolic_info_scores = h.new_zeros((B, S, I, V))
        else:
            symbolic_info_scores = self.symbolic_classes_layer(h).reshape(B, S, I, V)

        if self.only_symbolic_data:
            subsymbolic_reconstruction = h.new_zeros((B, S, U))  # B x S x U
            latent_reconstruction = h.new_zeros((B, S, L))  # B x S x L
        else:
            subsymbolic_reconstruction = self.subsymbolic_reconstruction_layer(h)  # B x S x U
            latent_reconstruction = self.latent_reconstruction_layer(h)  # B x S x U

        return {
            'symbolic_scores': symbolic_info_scores,
            'datetime_scores': datetime_scores,
            'subsymbolic_reconstruction': subsymbolic_reconstruction,
            'latent_reconstruction': latent_reconstruction
        }

    def _get_dimensions(self):
        T = 60
        D = _slen(self.episodes_dict.datetime_slice)
        I = _slen(self.episodes_dict.symbolic_scene_info_slice)
        V = len(self.episodes_dict)
        U = _slen(self.episodes_dict.subsymbolic_info_slice)
        L = _slen(self.episodes_dict.latent_data_slice)
        return T, D, I, V, U, L


def _cls_episode_auto_encoder_model(em_encoder_class):
    class EpisodeAutoEncoderModel(BaseFairseqModel):
        @staticmethod
        def add_args(parser):
            em_encoder_class.add_args(parser)
            parser.add_argument('--encoder-embed-dim', type=int, metavar='N',
                                help='encoder embedding dimension')
            parser.add_argument('--latent-embed-init-weights', type=str, default=None, metavar='FILE',
                                help='File to initialize latent embed layer')

        @classmethod
        def build_model(cls, args, task):
            dictionary = task.episodes_dictionary
            decoder = EpisodeAutoDecoder(args.episode_enc_output_dim, dictionary,
                                         only_symbolic_data=getattr(args, 'only_symbolic_data', False),
                                         exclude_symbolic_data=getattr(args, 'exclude_symbolic_data', False))
            model = EpisodeAutoEncoderModel(em_encoder_class.build_model(args, dictionary), decoder)
            if args.latent_embed_init_weights and Path(args.latent_embed_init_weights).is_file():
                print('| Initializing latent embed dim from pretrained file', args.latent_embed_init_weights)
                pretrained_latent_embed_layer: nn.Linear = torch.load(args.latent_embed_init_weights)
                latent_embed_layer: nn.Linear = model.encoder.embedding_latent
                if latent_embed_layer.in_features == 1 != pretrained_latent_embed_layer.in_features:
                    assert False, 'EM dictionary is not configured with image embeddings, ' \
                                  'should not use --latent-embed-init-weights'
                assert latent_embed_layer.in_features == pretrained_latent_embed_layer.in_features, \
                    f'latent {latent_embed_layer.in_features} != {pretrained_latent_embed_layer.in_features} pretrained'
                assert latent_embed_layer.out_features == pretrained_latent_embed_layer.out_features, \
                    f'latent {latent_embed_layer.out_features} != {pretrained_latent_embed_layer.out_features} pretrained'
                model.encoder.embedding_latent = pretrained_latent_embed_layer
            return model

        def __init__(self, encoder, decoder):
            super().__init__()
            self.encoder = encoder
            self.auto_decoder = decoder

        def forward(self, src_tokens, src_lengths, **kwargs):
            encoder_out = self.encoder(src_tokens, src_lengths)
            out = self.auto_decoder(encoder_out)
            return out,

        def get_normalized_probs(self, net_output, log_probs, sample=None):
            return super().get_normalized_probs(net_output[0], log_probs, sample)

    return EpisodeAutoEncoderModel


LinearEpisodeAutoEncoderModel = _cls_episode_auto_encoder_model(LinearEmEncoder)
LstmEpisodeAutoEncoderModel = _cls_episode_auto_encoder_model(LstmEmEncoder)
register_model('episode_auto_encoder_linear')(LinearEpisodeAutoEncoderModel)
register_model('episode_auto_encoder_lstm')(LstmEpisodeAutoEncoderModel)


class EpisodeReconstructionLoss(CombinedLoss):

    def __init__(self, args, task):
        if getattr(args, 'exclude_symbolic_data', False):
            weights = {
                'symbolic_scores': 0,
                'datetime_scores': 0.8,
                'subsymbolic_reconstruction': 0.1,
                'latent_reconstruction': 0.1
            }
        elif getattr(args, 'only_symbolic_data', False):
            weights = {
                'symbolic_scores': 0.8,
                'datetime_scores': 0.2,
                'subsymbolic_reconstruction': 0.0,
                'latent_reconstruction': 0.0
            }
        else:
            weights = {
                'symbolic_scores': 0.8,
                'datetime_scores': 0.19,
                'subsymbolic_reconstruction': 0.009,
                'latent_reconstruction': 0.001
            }

        super().__init__(args, task, underlying_criteria={
            'symbolic_scores': CrossEntropyCriterion(args, task),
            'datetime_scores': DatetimeLoss(args, task),
            'subsymbolic_reconstruction': MSELoss(args, task),
            'latent_reconstruction': MSELoss(args, task, actual_loss_func=F.l1_loss)
        }, weights=weights)


@register_task('episode_auto_encoder_task')
class EpisodeAutoEncoderTask(FairseqTask, EpisodeRegularizingTaskMixin):

    @staticmethod
    def add_args(parser):
        DatetimeLoss.add_args(parser)
        parser.add_argument('data', metavar='FILE',
                            help='file prefix for data')
        EpisodeAutoEncoderTask.add_dataset_args(parser)
        EpisodeRegularizingTaskMixin.add_args(parser)

    @staticmethod
    def add_dataset_args(parser):
        parser.add_argument('--num-generator-workers', type=int,
                            default=1, metavar='N',
                            help='number of subprocesses to use for async data generation')
        parser.add_argument('--regenerate-interval', type=int,
                            default=3, metavar='N',
                            help='number of epochs to keep a generated dataset until generating a new one')
        parser.add_argument('--epoch-size-factor', type=int,
                            default=100, metavar='N',
                            help='size factor for generate_episodes')
        parser.add_argument('--valid-size-factor', type=int,
                            default=5, metavar='N',
                            help='size factor for generate_episodes for valid set')
        parser.add_argument('--max-history-length', type=int,
                            default=5, metavar='N',
                            help='max history length for generate_episodes')
        parser.add_argument('--max-history-length-valid', type=int,
                            default=10, metavar='N',
                            help='max history length for valid split')

    @classmethod
    def setup_task(cls, args, **kwargs):
        em_vocab = EpisodeDictionary.load(os.path.join(args.data.split(':')[0], 'dict.episodes.txt'))
        print('| [EM] dictionary: {} types'.format(len(em_vocab)))
        return EpisodeAutoEncoderTask(args, em_vocab)

    def __init__(self, args, em_vocab: EpisodeDictionary):
        super().__init__(args)
        self.data_dir = Path(args.data.split(':')[0])
        assert self.data_dir.is_dir()
        self.episodes_vocab = em_vocab
        self.regenerate_interval = args.regenerate_interval
        if self.regenerate_interval <= 0:
            raise ValueError(self.regenerate_interval)
        self.regenerate_counter = 0

        self.force_synchronous_dataset_generation = True
        self.init_episode_data_regularization()
        self.force_synchronous_dataset_generation = False

        num_workers = getattr(args, 'num_generator_workers', 1)
        self.dataset_buffer = multiprocessing.Queue(maxsize=max(min(num_workers * 2, 15), 5))
        self.buffer_filler_workers = [multiprocessing.Process(target=self._fill_dataset_buffer,
                                                              args=(num_workers, i), daemon=True)
                                      for i in range(num_workers)]
        for p in self.buffer_filler_workers:
            p.start()

    def load_dataset(self, split, epoch=0, combine=False, **kwargs):
        if split == 'train' and not self.force_synchronous_dataset_generation:
            if self.regenerate_counter == 0 or split not in self.datasets:
                self.regenerate_counter = self.regenerate_interval - 1  # this epoch counts as well, so -1
                dataset_tensor = self.dataset_buffer.get(block=True)
                dataset = self._dataset_from_tensor(dataset_tensor)
            else:
                self.regenerate_counter -= 1
                return
        else:
            dataset = self._dataset_from_tensor(self._generate_dataset_tensor(epoch, split))

        if split in self.datasets:
            previous = self.datasets[split]
            if previous in self.dataset_to_epoch_iter:
                prev_iter = self.dataset_to_epoch_iter[previous]
                delattr(prev_iter, 'dataset')
                delattr(prev_iter, 'collate_fn')
                del self.dataset_to_epoch_iter[previous]
            del self.datasets[split]
        if torch.cuda.is_available():
            torch.cuda.empty_cache()

        self.datasets[split] = dataset

    def _fill_dataset_buffer(self, n, r):
        i = 0
        while True:
            dataset = self._generate_dataset_tensor(n * i + r, 'train')
            self.dataset_buffer.put(dataset, block=True)
            i += 1

    def _generate_dataset_tensor(self, epoch, split):
        from data_generator.episodes.generator import generate_histories_based_on
        size_factor = self.args.epoch_size_factor if split == 'train' else self.args.valid_size_factor
        max_history_length = self.args.max_history_length if split == 'train' else self.args.max_history_length_valid
        if self.force_synchronous_dataset_generation:
            # This is during episode regularization initialization.
            # Repeating dataset does not lead to anything here, except for unnecessary effort
            size_factor = 1
            max_history_length = 1
        base_file = self.data_dir / f'{split}.episodes-None.episodes'
        episode_lists = generate_histories_based_on(base_file, size_factor, max_history_length, Random(epoch),
                                                    silent=not self.force_synchronous_dataset_generation)
        episode_strings = ('[' + ', '.join(e.encode() for e in ep_list) + ']' for ep_list in episode_lists)
        episodes_encoded = [self.episodes_vocab.encode_line(e) for e in episode_strings]
        group_by_length = {
            i: [x.unsqueeze(0) for x in episodes_encoded if len(x) == i]
            for i in {len(x) for x in episodes_encoded}
        }
        large_tensors = {
            i: torch.cat(tensors)
            for i, tensors in group_by_length.items()
        }
        if torch.cuda.is_available() and not getattr(self.args, 'cpu', False):
            for i, t in list(large_tensors.items()):
                large_tensors[i] = t.cuda()
        return large_tensors

    def _dataset_from_tensor(self, tensors_grouped):
        episodes = [t[i] for length, t in tensors_grouped.items() for i in range(len(t))]
        dataset = EpisodeDataset(episodes, self.episodes_vocab, self.episode_data_regularization)
        return dataset

    def get_initializing_episode_dataset(self) -> EpisodeDataset:
        return self.datasets['train']

    def build_criterion(self, args):
        return EpisodeReconstructionLoss(args, self)

    def aggregate_logging_outputs(self, logging_outputs, criterion):
        if isinstance(criterion, CombinedLoss):
            return criterion.aggregate_logging_outputs(logging_outputs)
        else:
            return super().aggregate_logging_outputs(logging_outputs, criterion)

    @property
    def source_dictionary(self):
        return None

    @property
    def target_dictionary(self):
        return None

    @property
    def episodes_dictionary(self):
        return self.episodes_vocab


@register_model_architecture('episode_auto_encoder_linear', 'episode_auto_encoder_linear_arch')
def episode_autoencoder_linear(args: Namespace):
    set_default_embedding_args(args)
    args.episode_enc_hidden_dim = getattr(args, 'episode_enc_hidden_dim', 32)
    args.episode_enc_output_dim = getattr(args, 'episode_enc_output_dim', args.encoder_embed_dim)
    transformer_dropout = getattr(args, 'dropout', 0.15)
    args.episode_enc_dropout = getattr(args, 'episode_enc_dropout',
                                       transformer_dropout)


@register_model_architecture('episode_auto_encoder_lstm', 'episode_auto_encoder_lstm_arch')
def episode_autoencoder_lstm(args: Namespace):
    set_default_embedding_args(args)
    args.episode_enc_embed_dim = getattr(args, 'episode_enc_embed_dim', 32)
    args.episode_enc_num_layers = getattr(args, 'episode_enc_num_layers', 2)
    episode_autoencoder_linear(args)


def eval_model():
    parser = get_generation_parser()
    parser.add_argument('--eval-arch', type=str, help='Arch of the model to eval')
    EpisodeAutoEncoderTask.add_dataset_args(parser)
    args = parse_args_and_arch(parser)
    args.cpu = True

    eval_arch = getattr(args, 'eval_arch', None)
    checkpoint = checkpoint_utils.load_checkpoint_to_cpu(args.path, arg_overrides=args.__dict__)
    args = checkpoint['args']
    task = EpisodeAutoEncoderTask.setup_task(args)
    if 'emv' in args.arch:
        assert eval_arch, 'Need to explicitly set episode_auto_encoder_*_arch ' \
                          'to eval when loading from emv model!'
        print('Converting from EMV model!')
        autoenc_dict = {}
        for k, v in checkpoint['model'].items():
            enc_key = 'encoder.episode.'
            if k.startswith(enc_key):
                autoenc_dict[k.replace(enc_key, 'encoder.')] = v
            elif k.startswith('additional_decoder_episode'):
                k = k.replace('additional_decoder_episode', 'auto_decoder')
                autoenc_dict[k] = v
        print(autoenc_dict.keys())
    else:
        autoenc_dict = checkpoint['model']

    args.arch = eval_arch if eval_arch else args.arch
    model = task.build_model(args)
    model.load_state_dict(autoenc_dict)

    test_dataset_seed = 123456
    task.load_dataset(epoch=test_dataset_seed, split='test')
    dataset = task.dataset('test')
    iterator = task.get_batch_iterator(dataset=dataset, max_sentences=args.max_sentences).next_epoch_itr(shuffle=False)
    results: Dict[str, float] = {}
    for i, sample in enumerate(iterator):
        output = model(**sample['net_input'])[0]
        target = sample['targets']

        target = task.reverse_output_episode_regularization(target)
        output = task.reverse_output_episode_regularization(output)
        compare = task.episodes_dictionary.compare(
            output, target,
            print_sample=i % 1000 == 0,
            normalized_datetimes=getattr(args, 'normalize_datetime_fields', False))

        for k, v in compare.items():
            results.setdefault(k, 0)
            results[k] += v
    for k in results.keys():
        results[k] /= len(iterator)
        results[k] = round(results[k], ndigits=3)
    print(results)


if __name__ == '__main__':
    eval_model()
