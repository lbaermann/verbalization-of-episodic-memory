import torch
import torch.nn.functional as F
from torch import nn, Tensor

from model.em import EpisodeDictionary


class EmEncoderBase:

    @staticmethod
    def add_common_args(parser):
        parser.add_argument('--episode-enc-hidden-dim', type=int, metavar='N',
                            help='Hidden dim for episode data encoder')
        parser.add_argument('--episode-enc-output-dim', type=int, metavar='N',
                            help='Output dim for episode data encoder')
        parser.add_argument('--episode-enc-dropout', type=float, metavar='D',
                            help='Dropout for episode data encoder')
        parser.add_argument('--episode-enc-symb-embed-dropout', type=float, metavar='D',
                            help='Dropout for symbolic data embedding vector')

        parser.add_argument('--episode-enc-embed-dim-dt', type=int, metavar='N',
                            help='Embedding size for episode datetime data')
        parser.add_argument('--episode-enc-embed-dim-symb', type=int, metavar='N',
                            help='Embedding size for episode symbolic data')
        parser.add_argument('--episode-enc-embed-dim-subsymb', type=int, metavar='N',
                            help='Embedding size for episode subsymbolic data')
        parser.add_argument('--episode-enc-embed-dim-latent', type=int, metavar='N',
                            help='Embedding size for episode latent data')

        parser.add_argument('--only-symbolic-data', action='store_true', default=False,
                            help='Ignore all episode data except Date-Time & Symbolic information')
        parser.add_argument('--exclude-symbolic-data', action='store_true', default=False,
                            help='Exclude all symbolic episode data (except Date-Time)')

    @staticmethod
    def get_constructor_kw_args(args):
        return dict(emb_size_dt=args.episode_enc_embed_dim_dt,
                    emb_size_symb=1 if args.exclude_symbolic_data else args.episode_enc_embed_dim_symb,
                    emb_size_subsymb=1 if args.only_symbolic_data else args.episode_enc_embed_dim_subsymb,
                    emb_size_latent=1 if args.only_symbolic_data else args.episode_enc_embed_dim_latent,
                    dropout_symb_emb=args.episode_enc_symb_embed_dropout,
                    hidden_size=args.episode_enc_hidden_dim,
                    internal_output_size=args.episode_enc_output_dim,
                    output_size=args.encoder_embed_dim)

    dictionary: EpisodeDictionary
    dropout_symb_emb: float
    training: bool

    # noinspection PyAttributeOutsideInit
    def create_embedding_layers(self, emb_size_dt, emb_size_symb, emb_size_subsymb, emb_size_latent):
        def slice_len(s: slice):
            return s.stop - s.start

        self.embedding_datetime = nn.Linear(slice_len(self.dictionary.datetime_slice),
                                            emb_size_dt, bias=True)
        self.embedding_symbolic = nn.Linear(slice_len(self.dictionary.symbolic_scene_info_slice),
                                            emb_size_symb, bias=True)
        self.embedding_subsymbolic = nn.Linear(slice_len(self.dictionary.subsymbolic_info_slice),
                                               emb_size_subsymb, bias=True)
        self.embedding_latent = nn.Linear(slice_len(self.dictionary.latent_data_slice),
                                          emb_size_latent, bias=True)
        return emb_size_dt + emb_size_symb + emb_size_subsymb + emb_size_latent

    def embed(self, src_tokens: Tensor):
        dt_embedding = self.embedding_datetime(src_tokens[..., self.dictionary.datetime_slice])

        if self.embedding_symbolic.out_features == 1:  # --exclude-symbolic-data
            symb_embedding = torch.zeros_like(src_tokens[..., 0].unsqueeze(dim=-1))
        else:
            symb_embedding = self.embedding_symbolic(src_tokens[..., self.dictionary.symbolic_scene_info_slice])

        if self.embedding_subsymbolic.out_features == 1 == self.embedding_latent.out_features:  # --only-symbolic-data
            subsymb_embedding = torch.zeros_like(src_tokens[..., 0].unsqueeze(dim=-1))
            latent_embedding = torch.zeros_like(src_tokens[..., 0].unsqueeze(dim=-1))
        else:
            subsymb_embedding = self.embedding_subsymbolic(src_tokens[..., self.dictionary.subsymbolic_info_slice])
            latent_embedding = self.embedding_latent(src_tokens[..., self.dictionary.latent_data_slice])

        symb_embedding = F.dropout(symb_embedding, p=self.dropout_symb_emb, training=self.training)

        embedding = torch.cat((dt_embedding, symb_embedding, subsymb_embedding, latent_embedding), dim=2)
        return embedding

    # noinspection PyUnresolvedReferences
    def finalize_encoder_output(self, x, src_tokens, final_output):
        bsz, seqlen = src_tokens.size(0), src_tokens.size(1)

        assert list(x.size()) == [seqlen, bsz, self.output_size]

        # Normal: src_tokens : B x T
        # Here  : src_tokens : B x T x F
        # Want  : enc_padding_mask: T x B with enc_padding_mask[t, b] == 1  <=> src_tokens[b, t, x] == pad (x = 0..F)
        encoder_padding_mask = (src_tokens.transpose(0, 1) == self.padding_idx).all(2)
        assert list(encoder_padding_mask.size()) == [seqlen, bsz]

        return {
            'encoder_out': final_output,
            'encoder_padding_mask': encoder_padding_mask if encoder_padding_mask.any() else None
        }

    # noinspection PyUnresolvedReferences
    def stretch_output_if_necessary(self, x):
        if self.internal_output_size < self.output_size:
            # Repeat values of x
            factor = self.output_size // self.internal_output_size
            x = torch.cat([x] * factor, dim=2)
        return x


def set_default_embedding_args(args, dt=32, symb=32, subsymb=64, latent=32):
    args.episode_enc_embed_dim_dt = getattr(args, 'episode_enc_embed_dim_dt', dt)
    args.episode_enc_embed_dim_symb = getattr(args, 'episode_enc_embed_dim_symb', symb)
    args.episode_enc_embed_dim_subsymb = getattr(args, 'episode_enc_embed_dim_subsymb', subsymb)
    args.episode_enc_embed_dim_latent = getattr(args, 'episode_enc_embed_dim_latent', latent)
    args.episode_enc_symb_embed_dropout = getattr(args, 'episode_enc_symb_embed_dropout', 0.0)
