from typing import Iterator, Callable

import torch
from torch import Tensor, BoolTensor
from torch.nn import Module, Parameter


class RegularizationLayer(Module):

    def __init__(self, size: int, mask: Tensor) -> None:
        """
        @param size: size of the input vectors
        @param mask: mask of which indices _not_ to regularize. 1 = don't regularize, 0 = regularize.
        """
        super().__init__()
        assert mask.size(0) == size
        self.mask = mask
        self.mean_vector = Parameter(Tensor(size), requires_grad=False)
        self.std_dev_vector = Parameter(Tensor(size), requires_grad=False)

    def forward(self, input: Tensor) -> Tensor:
        output = input - self.mean_vector.expand_as(input).to(input.device)
        output = output / self.std_dev_vector.expand_as(input).to(input.device)

        # Apply mask by replacing values in output with values from input where mask == 1
        mask = self.mask.expand_as(output).to(input.device)
        output = output - mask * output + mask * input
        return output

    def init_on_dataset(self, get_tensor_iterator: Callable[[], Iterator[Tensor]]):
        tensor_iterator = get_tensor_iterator()
        first = next(tensor_iterator)
        count = 0
        sum_values = torch.zeros(first.size(-1), dtype=torch.float, device=first.device)

        def handle_tensor_mean(t: Tensor):
            nonlocal sum_values, count
            count += len(t.view(-1, t.size(-1)))
            sum_values += torch.sum(t, list(range(0, t.ndim - 1)))

        handle_tensor_mean(first)
        for x in tensor_iterator:
            handle_tensor_mean(x)

        if count == 0:
            self.mean_vector.data.zero_()
        else:
            self.mean_vector.data = sum_values / count
        mean = self.mean_vector.data

        squared_dev_sum = torch.zeros_like(sum_values)
        for x in get_tensor_iterator():
            local_sum = torch.sum(x, list(range(0, x.ndim - 1)))
            squared_dev_sum += (local_sum - mean) ** 2

        if count <= 1:
            self.std_dev_vector.data = torch.ones_like(self.std_dev_vector.data)
        else:
            self.std_dev_vector.data = squared_dev_sum / (count - 1)
            self.std_dev_vector.data.sqrt_()
            for i, x in enumerate(self.std_dev_vector.data):
                if x == 0:
                    self.std_dev_vector.data[i] = 1

    def create_reverse(self):
        regularized_count = 0
        for x in self.mask:
            if x == 0:
                regularized_count += 1

        layer = ReverseRegularizationLayer(regularized_count)
        layer.init_from_partner(self)
        return layer


class ReverseRegularizationLayer(RegularizationLayer):
    """
    ReverseRegularizationLayer does not use its own mask - it expects its input to be all regularized parts of the
    net output concatenated to one vector.
    """

    def __init__(self, size: int) -> None:
        super().__init__(size, torch.zeros(size, dtype=torch.bool))

    def forward(self, input: Tensor) -> Tensor:
        output = input * self.std_dev_vector.expand_as(input).to(input.device)
        output = output + self.mean_vector.expand_as(input).to(input.device)
        return output

    def init_from_partner(self, partner: RegularizationLayer):
        my_idx = 0
        for partner_idx, x in enumerate(partner.mask):
            if x == 0:
                self.mean_vector.data[my_idx] = partner.mean_vector.data[partner_idx]
                self.std_dev_vector.data[my_idx] = partner.std_dev_vector.data[partner_idx]
                my_idx += 1
        assert my_idx == self.mask.size(0)
