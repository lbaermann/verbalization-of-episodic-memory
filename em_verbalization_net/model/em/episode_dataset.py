import torch
from fairseq.data import FairseqDataset
from typing import List

from model.em import EpisodeDictionary
from model.em.regularize import RegularizationLayer


class EpisodeDataset(FairseqDataset):

    def __init__(self, dataset, vocab: EpisodeDictionary,
                 episode_data_regularization: RegularizationLayer = None):
        self.episodes_dict = vocab
        self.dataset = dataset
        self.episode_data_regularization = episode_data_regularization

    def __getitem__(self, index):
        return {
            'id': index,
            'episode': self.dataset[index],
        }

    def collater(self, samples):
        if len(samples) == 0:
            return {}

        # noinspection PyCallByClass
        src_tokens = self.regularize_and_collate_episodes(
            [s['episode'] for s in samples],
            pad_idx=self.episodes_dict.pad(),
        )  # B x S x R

        # noinspection PyArgumentList
        lengths = self.get_episode_lenghts(samples)
        # noinspection PyArgumentList
        ids = torch.tensor([s['id'] for s in samples], dtype=torch.long, device=src_tokens.device)

        lengths, sort_order = lengths.sort(descending=True)
        src_tokens = src_tokens.index_select(0, sort_order)
        ids = ids.index_select(0, sort_order)

        return {
            'id': ids,
            'nsentences': len(samples),
            'ntokens': sum(len(s['episode']) for s in samples),
            'net_input': {
                'src_tokens': src_tokens,
                'src_lengths': lengths,
            },
            'targets': self.get_autoencoder_targets(src_tokens)
        }

    def get_episode_lenghts(self, samples):
        return torch.tensor([
            s['episode'].size(0) // self.episodes_dict.representation_size
            if s['episode'].dim() == 1
            else s['episode'].size(0)
            for s in samples
        ], dtype=torch.long, device=samples[0]['episode'].device)

    def get_autoencoder_targets(self, src_tokens):
        dt = self.episodes_dict.datetime_slice
        symb = self.episodes_dict.symbolic_scene_info_slice
        subsymb = self.episodes_dict.subsymbolic_info_slice
        latent = self.episodes_dict.latent_data_slice

        if self.episode_data_regularization:
            src_tokens = self.episode_data_regularization(src_tokens)

        symbolic_targets = src_tokens[..., symb].type(torch.long).contiguous()
        abs_times = src_tokens[..., dt].type(torch.long).contiguous()
        subsymbolic_targets = src_tokens[..., subsymb].type(torch.float).contiguous()
        latent_targets = src_tokens[..., latent].type(torch.float).contiguous()
        return {
            'symbolic_scores': symbolic_targets,
            'datetime_scores': abs_times,
            'subsymbolic_reconstruction': subsymbolic_targets,
            'latent_reconstruction': latent_targets
        }

    def regularize_and_collate_episodes(self, episodes: List[torch.Tensor], pad_idx):
        """Regularize and convert a list of 2d episodes into a padded 3d tensor
        Args:
            episodes (list): list of 2d frames of size L[i] x repr_size. Where L[i] is
                length of i-th episode and repr_size is static dimension of key frame representation
        Returns:
            3d tensor of size len(episodes) x len_max x repr_size where len_max is max of L[i]
        """
        repr_size = self.episodes_dict.representation_size
        if len(episodes) == 0:
            return torch.zeros((0, 0, repr_size))

        assert all(e.ndim == episodes[0].ndim for e in episodes)
        if episodes[0].ndim == 1:
            # The vectors were flattened by preprocessing. Reshape to intended format
            episodes = [e.reshape(-1, repr_size) for e in episodes]

        len_max = max(e.size(0) for e in episodes)
        res = episodes[0].new(len(episodes), len_max, repr_size).fill_(pad_idx)

        for i, v in enumerate(episodes):
            res[i, :v.size(0)] = v

        if self.episode_data_regularization:  # Also regularize padding
            res = self.episode_data_regularization(res)

        return res

    def __len__(self):
        return len(self.dataset)

    def num_tokens(self, index):
        return self.dataset[index].size(0)

    def size(self, index):
        return self.num_tokens(index)

    def prefetch(self, indices):
        pass
