import torch.nn.functional as F
from fairseq.criterions.cross_entropy import CrossEntropyCriterion

from model.em.mse_loss import MSELoss


class DatetimeLoss(CrossEntropyCriterion):

    def __init__(self, args, task):
        super().__init__(args, task)

    def compute_loss(self, model, net_output, sample, reduce=True):
        net_output = net_output[0]
        net_output = net_output.view(-1, net_output.size(-1))
        target = sample['target'].view(-1)
        loss = F.nll_loss(
            F.log_softmax(net_output, dim=1),
            target,
            ignore_index=self.padding_idx,
            reduction='sum' if reduce else 'none',
        )
        return loss, loss
