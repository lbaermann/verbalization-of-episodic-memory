import math

import torch.nn.functional as F
from fairseq import utils
from fairseq.criterions import FairseqCriterion


class MSELoss(FairseqCriterion):
    """
    Implements MSE loss as fairseq criterion.
    Note that there is also a constructor argument switching from MSE (=L2) to (smooth) L1 loss.
    """

    def __init__(self, args, task, actual_loss_func=F.mse_loss):
        super().__init__(args, task)
        self.actual_loss_func = actual_loss_func

    def forward(self, model, sample, reduce=True):
        """Compute the loss for the given sample.

        Returns a tuple with three elements:
        1) the loss
        2) the sample size, which is used as the denominator for the gradient
        3) logging outputs to display while training
        """
        net_output = model(**sample['net_input'])[0]
        loss = self.compute_loss(net_output, sample['target'], reduce=reduce)
        sample_size = sample['target'].size(0) if self.args.sentence_avg else sample['ntokens']
        logging_output = self.create_logging_output(loss, sample, sample_size, net_output, reduce)
        return loss, sample_size, logging_output

    def compute_loss(self, net_output, target, reduce=True):
        target = target.view(-1)
        output = net_output.reshape(-1)
        loss = self.actual_loss_func(
            output,
            target,
            reduction='sum' if reduce else 'none',
        )
        return loss

    def create_logging_output(self, loss, sample, sample_size, net_output, reduce):
        logging_output = {
            'loss': utils.item(loss.data) if reduce else loss.data,
            'ntokens': sample['ntokens'],
            'nsentences': sample['target'].size(0),
            'sample_size': sample_size,
        }
        return logging_output

    @staticmethod
    def aggregate_logging_outputs(logging_outputs):
        loss_sum = sum(log.get('loss', 0) for log in logging_outputs)
        ntokens = sum(log.get('ntokens', 0) for log in logging_outputs)
        nsentences = sum(log.get('nsentences', 0) for log in logging_outputs)
        sample_size = sum(log.get('sample_size', 0) for log in logging_outputs)
        agg_output = {
            'loss': loss_sum / sample_size / math.log(2) if sample_size > 0 else 0.,
            'ntokens': ntokens,
            'nsentences': nsentences,
            'sample_size': sample_size,
        }
        if len(logging_outputs) > 0:
            for key in logging_outputs[0].keys():
                if key not in agg_output and all(key in log for log in logging_outputs):
                    agg_output[key] = sum(log[key] for log in logging_outputs) / len(logging_outputs)
        return agg_output
