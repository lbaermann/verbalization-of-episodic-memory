from argparse import Namespace
from pathlib import Path
from typing import Dict, Any, Callable, Optional

import torch

from model.em import EpisodeDictionary
from model.em.episode_dataset import EpisodeDataset
from model.em.regularize import RegularizationLayer, ReverseRegularizationLayer


class EpisodeRegularizingTaskMixin:
    args: Namespace
    load_dataset: Callable
    episodes_dictionary: EpisodeDictionary
    get_batch_iterator: Callable
    datasets: Dict[str, Any]
    episode_data_regularization: Optional[RegularizationLayer]
    episode_subsymb_reverse_regularization: ReverseRegularizationLayer

    @staticmethod
    def add_args(parser):
        parser.add_argument('--episode-data-regularization-file', default=None, type=str,
                            help='file where the regularization of subsymbolic episode data is stored.')

    get_initializing_episode_dataset: Callable[[], EpisodeDataset]

    def init_episode_data_regularization(self):
        self.episode_data_regularization = None

        if self.args.episode_data_regularization_file:
            reg_file = Path(self.args.episode_data_regularization_file)
        elif hasattr(self.args, 'save_dir'):  # Default during fairseq-train
            reg_file = Path(self.args.save_dir) / 'episode_data_regularization.pt'
        elif hasattr(self.args, 'path'):  # Default during fairseq-generate
            reg_file = Path(self.args.path).parent / 'episode_data_regularization.pt'
        else:
            assert False, '--episode-data-regularization-file needs to be set if neither ' \
                          '--save-dir (during train) or --path (during inference) is given'

        symbolic_length = self.episodes_dictionary.subsymbolic_info_slice.start
        if not reg_file.exists():
            print(f'| File {reg_file} does not exist, initializing episode data regularization from train set...')
            self.load_dataset('train', epoch=0)
            reg_layer = RegularizationLayer(self.episodes_dictionary.representation_size, mask=torch.cat([
                torch.ones(symbolic_length),
                torch.zeros(self.episodes_dictionary.subsymbolic_info_slice.stop - symbolic_length),
                torch.ones(self.episodes_dictionary.representation_size
                           - self.episodes_dictionary.subsymbolic_info_slice.stop)
            ]))
            reg_layer.init_on_dataset(
                lambda: (sample['net_input']['src_tokens'] for sample in
                         self.get_batch_iterator(self.get_initializing_episode_dataset())
                             .next_epoch_itr(shuffle=False)))
            self.episode_data_regularization = reg_layer
            torch.save(self.episode_data_regularization, reg_file)
            print('| initialized regularization on train dataset')
            # Forget the dataset so that it can be reloaded with correct regularization
            self.datasets.clear()
            if hasattr(self, 'dataset_to_epoch_iter') and isinstance(self.dataset_to_epoch_iter, Dict):
                self.dataset_to_epoch_iter.clear()
        else:
            if torch.cuda.is_available():
                self.episode_data_regularization = torch.load(reg_file)
            else:
                self.episode_data_regularization = torch.load(reg_file, map_location='cpu')
            print(f'| initialized episode data regularization from {reg_file}')

        self.episode_subsymb_reverse_regularization = self.episode_data_regularization.create_reverse()
        if torch.cuda.is_available():
            self.episode_subsymb_reverse_regularization.cuda()

    def reverse_episode_regularization(self, episode):
        cut_start = self.episodes_dictionary.subsymbolic_info_slice.start
        cut_end = self.episodes_dictionary.subsymbolic_info_slice.stop
        reverse = self.episode_subsymb_reverse_regularization
        return torch.cat(
            (episode[..., :cut_start], reverse(episode[..., cut_start:cut_end]), episode[..., cut_end:]),
            dim=1)

    def reverse_output_episode_regularization(self, episode_output):
        subsymbolic_data = episode_output['subsymbolic_reconstruction']
        reverse = self.episode_subsymb_reverse_regularization
        reverse_regularized = reverse(subsymbolic_data)
        return {
            'symbolic_scores': episode_output['symbolic_scores'],
            'datetime_scores': episode_output['datetime_scores'],
            'subsymbolic_reconstruction': reverse_regularized,
            'latent_reconstruction': episode_output['latent_reconstruction'],
        }
