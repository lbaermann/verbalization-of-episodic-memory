import torch
import torch.nn.functional as F
from fairseq.models.lstm import LSTMEncoder
from torch import nn

from .dictionary import EpisodeDictionary
from .em_encoder_base import EmEncoderBase


class LstmEmEncoder(LSTMEncoder, EmEncoderBase):

    @staticmethod
    def add_args(parser):
        EmEncoderBase.add_common_args(parser)
        parser.add_argument('--episode-enc-num-layers', type=int, metavar='N',
                            help='Number of layers for episode data encoder LSTM')

    @staticmethod
    def build_model(args, episodes_dict):
        return LstmEmEncoder(
            episodes_dict,
            **EmEncoderBase.get_constructor_kw_args(args),
            num_layers=args.episode_enc_num_layers,
            dropout_in=args.episode_enc_dropout,
            dropout_out=args.episode_enc_dropout,
            padding_value=episodes_dict.pad(),
        )

    def __init__(self, dictionary: EpisodeDictionary,
                 emb_size_dt=128, emb_size_symb=128, emb_size_subsymb=256, emb_size_latent=64,
                 hidden_size=512, internal_output_size=512, output_size=512,
                 num_layers=1, dropout_in=0.1, dropout_out=0.1, dropout_symb_emb=0.1, padding_value=0.):
        assert isinstance(dictionary, EpisodeDictionary)
        assert output_size % internal_output_size == 0
        super().__init__(
            dictionary,
            embed_dim=emb_size_dt + emb_size_symb + emb_size_subsymb + emb_size_latent,
            hidden_size=hidden_size,
            num_layers=num_layers,
            dropout_in=dropout_in, dropout_out=dropout_out,
            bidirectional=False, left_pad=False,
            padding_value=padding_value,
            pretrained_embed=nn.Identity()  # Use our custom embedding
        )
        self.create_embedding_layers(emb_size_dt, emb_size_symb, emb_size_subsymb, emb_size_latent)
        self.internal_output_size = internal_output_size
        self.output_size = output_size
        self.dropout_symb_emb = dropout_symb_emb
        self.output_layer = nn.Linear(hidden_size, internal_output_size, bias=False)

    def forward(self, src_tokens: torch.Tensor, src_lengths):
        # Most of this is copy & paste from superclass to allow making minor modifications
        # e.g. to remove constraint that src_tokens has dimension B x T
        #       (here it is B x T x F, so embed is simply a linear)
        bsz, seqlen = src_tokens.size(0), src_tokens.size(1)
        src_tokens = src_tokens.type_as(next(self.parameters()))

        # embed tokens
        x = self.embed(src_tokens)
        x = F.dropout(x, p=self.dropout_in, training=self.training)

        # B x T x C -> T x B x C
        x = x.transpose(0, 1)

        # pack embedded source tokens into a PackedSequence
        packed_x = nn.utils.rnn.pack_padded_sequence(x, src_lengths.data.tolist())

        # apply LSTM
        state_size = self.num_layers, bsz, self.hidden_size
        h0 = x.new_zeros(*state_size)
        c0 = x.new_zeros(*state_size)
        packed_outs, (final_hiddens, final_cells) = self.lstm(packed_x, (h0, c0))

        # unpack outputs and apply dropout
        x, _ = nn.utils.rnn.pad_packed_sequence(packed_outs, padding_value=self.padding_value)
        x = F.dropout(x, p=self.dropout_out, training=self.training)
        assert list(x.size()) == [seqlen, bsz, self.output_units]

        x = self.output_layer(x)
        x = self.stretch_output_if_necessary(x)
        return self.finalize_encoder_output(x, src_tokens, (x, final_hiddens, final_cells))
