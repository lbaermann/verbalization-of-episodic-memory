import json
from argparse import ArgumentParser
from pathlib import Path

import torch
import torch.nn as nn
import numpy as np
from click import progressbar
from torch import optim
from typing import Dict, List


class LatentEmbedAutoEncoder(nn.Module):

    def __init__(self, latent_dim, embed_dim) -> None:
        super().__init__()
        self.embed = nn.Linear(in_features=latent_dim, out_features=embed_dim, bias=True)
        self.activation = nn.LeakyReLU(0.1)
        self.reverse = nn.Linear(in_features=embed_dim, out_features=latent_dim, bias=True)

    def forward(self, latent_vector):
        embed = self.embed(latent_vector)
        hidden = self.activation(embed)
        return self.reverse(hidden)


def main(input_file: Path, net_output_file: Path, preprocess_output_file: Path,
         train_ids_file: Path = None,
         latent_dim=1024, embed_dim=32, num_epochs=10, lr=0.01, use_pca=False):
    data = torch.load(input_file)
    assert isinstance(data, Dict)
    assert isinstance(next(iter(data.values())), torch.Tensor)
    if torch.cuda.is_available():
        data = {k: v.cuda() for k, v in data.items()}
    eval_data, train_data = split_train_eval(data, train_ids_file)
    print(f'Loaded {len(train_data)} train / {len(eval_data)} eval images, starting training...')

    if use_pca:
        net = train_pca(embed_dim, latent_dim, train_data)
    else:
        net = LatentEmbedAutoEncoder(latent_dim, embed_dim)
        if torch.cuda.is_available():
            net = net.cuda()
        optimizer = optim.Adam(net.parameters(), lr=lr)
        criterion = nn.MSELoss()
        train(criterion, train_data, net, optimizer, num_epochs)

    save_embedding_layer(net, net_output_file)
    create_preprocessed_file(data, net, preprocess_output_file)
    print()
    print('Evaluation')
    print('On train...')
    evaluate(train_data, latent_dim, net)
    print('On eval...')
    evaluate(eval_data, latent_dim, net)


def create_preprocessed_file(data, net, preprocess_output_file):
    print(f'Creating preprocessed file {preprocess_output_file}')
    preprocessed_data = {}
    with progressbar(data.items()) as data_iterator:
        for key, latent in data_iterator:
            preprocessed_data[key] = net.embed(latent).detach().cpu()
    torch.save(preprocessed_data, preprocess_output_file)


def save_embedding_layer(net, net_output_file):
    # Move to CPU for saving
    if torch.cuda.is_available():
        net.cpu()
    torch.save(net.embed, net_output_file)
    # Move back to GPU for eval & preprocessing
    if torch.cuda.is_available():
        net.cuda()


def split_train_eval(data, train_ids_file):
    train_data = dict(data)
    eval_data = dict(data)
    if train_ids_file:
        train_ids = json.loads(train_ids_file.read_text())
        assert isinstance(train_ids, List)
        assert all(isinstance(i, int) for i in train_ids)
        assert 0 < len(train_ids) <= len(data)
        for k in list(data.keys()):
            if k in train_ids:
                del eval_data[k]
            else:
                del train_data[k]
    return eval_data, train_data


def train_pca(embed_dim, latent_dim, train_data):
    print(f'Doing PCA with n_components = {embed_dim}')
    net = LatentEmbedAutoEncoder(latent_dim, embed_dim)
    from sklearn.decomposition import PCA
    pca = PCA(n_components=embed_dim)
    X = np.array([t.numpy() for t in train_data.values()])
    pca.fit(X)
    # PCA    : x' = C * (x - m)
    # Linear : x' = W * x + b
    # => W = C, b = - C*m
    net.embed.weight.data = torch.tensor(pca.components_)
    net.embed.bias.data = torch.tensor(pca.components_).matmul(torch.tensor(-pca.mean_))
    # PCA    : x = CT * x' + m
    # => W = CT, b = + m
    net.reverse.weight.data = net.embed.weight.t()
    net.reverse.bias.data = torch.tensor(pca.mean_)
    # Sanity checks
    sample = torch.rand(latent_dim).unsqueeze(0)
    pca_transformed = torch.tensor(pca.transform(sample.numpy())).squeeze()
    net_transformed = net.embed(sample).squeeze()
    assert sum(abs(pca_transformed - net_transformed)) < 0.001
    pca_back = torch.tensor(pca.inverse_transform(pca_transformed.unsqueeze(0).numpy())).squeeze()
    net_back = net.reverse(net_transformed.unsqueeze(0)).squeeze()
    assert sum(abs(pca_back - net_back)) < 0.001
    return net


def train(criterion, data, net, optimizer, num_epochs):
    batch_size = 64
    values = list(data.values())
    batches = [
        torch.cat([t.unsqueeze(0) for t in values[i:i + batch_size]], dim=0)
        for i in range(0, len(values), batch_size)
    ]
    for e in range(num_epochs):
        total_loss = 0
        latent_vector: torch.Tensor
        with progressbar(batches) as data_iterator:
            for batch in data_iterator:
                optimizer.zero_grad()
                loss = criterion(net(batch), batch)
                total_loss += loss
                loss.backward()
                optimizer.step()

        print(f'Epoch {e + 1}, loss: {total_loss / len(data)}')


def evaluate(data, latent_dim, net):
    if torch.cuda.is_available():
        data = {k: v.cuda() for k, v in data.items()}
        net = net.cuda()

    with progressbar(data.values()) as data_iterator, torch.no_grad():
        latent_values = torch.tensor(0.0)
        difference = torch.tensor(0.0)
        for latent_vector in data_iterator:
            latent_values += torch.sum(abs(latent_vector))
            batched = latent_vector.unsqueeze(0)
            difference += torch.sum(abs(net(batched) - batched))
        difference /= len(data) * latent_dim
        latent_values /= len(data) * latent_dim

    print('Average difference   : ', difference.item())
    print('Average latent value : ', latent_values.item())
    print('Relative average diff: ', (difference / latent_values).item())


def cli_main():
    parser = ArgumentParser()
    parser.add_argument('--latent-dim', type=int, metavar='N', default=1024,
                        help='dim of episode latent data')
    parser.add_argument('--embed-dim', type=int, metavar='N', required=True,
                        help='Embedding size for episode latent data')
    parser.add_argument('--num-epochs', type=int, metavar='N', default=10,
                        help='Number of epochs to train')
    parser.add_argument('--lr', type=float, metavar='LR', default=0.01,
                        help='Learning rate')
    parser.add_argument('--use-pca', action='store_true', default=False,
                        help='Use PCA instead of NN')
    parser.add_argument('--out', type=lambda s: Path(s), metavar='OUT', default=Path('latent_embed_layer.torch'),
                        help='File to save embedding layer to.')
    parser.add_argument('--preprocess-out', type=lambda s: Path(s), metavar='OUT',
                        default=Path('all_img_embeddings_reduced.torch'),
                        help='File to save preprocessed latent vectors to.')
    parser.add_argument('--train-ids-file', type=lambda s: Path(s), metavar='FILE', default=None,
                        help='File with ids of images belonging to train set.')
    parser.add_argument('input_file', type=lambda s: Path(s), metavar='FILE',
                        help='all_img_embeddings.torch: File with a "torch.save"d map of img ids to latent vectors')
    args = parser.parse_args()

    main(args.input_file, args.out, args.preprocess_out, args.train_ids_file,
         args.latent_dim, args.embed_dim, args.num_epochs,
         args.lr, args.use_pca)


if __name__ == '__main__':
    cli_main()
