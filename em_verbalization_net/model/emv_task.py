import os
import random
import shutil
import sys
import time
import traceback
from functools import reduce
from pathlib import Path
from typing import List, Tuple

import fairseq.data.iterators
import requests
import torch
from fairseq.data import Dictionary
from fairseq.tasks import FairseqTask, register_task
from requests import RequestException
from torch.utils.data import BatchSampler, SequentialSampler

from eval.semantic_eval import semantic_eval
from .combined_loss import CombinedLoss
from .em import EpisodeDictionary, TimeRefDictionary
from .em.auto_encoder import EpisodeReconstructionLoss
from .em.datetime_loss import DatetimeLoss
from .em.episode_dataset import EpisodeDataset
from .em.episode_regularizing_task import EpisodeRegularizingTaskMixin
from .emv_dataset import load_emv_dataset, EmvDataset


@register_task('emv_task')
class EmvTask(FairseqTask, EpisodeRegularizingTaskMixin):
    question_vocab: Dictionary
    answer_vocab: Dictionary
    episodes_vocab: EpisodeDictionary

    @staticmethod
    def is_episodes_files(filenames):
        episode_files = ['.episodes' in f for f in filenames]
        assert all(episode_files) or not any(episode_files), str(filenames)
        return all(episode_files)

    @staticmethod
    def is_time_refs_files(filenames):
        time_refs_files = ['.time_refs' in f for f in filenames]
        assert all(time_refs_files) or not any(time_refs_files), str(filenames)
        return all(time_refs_files)

    @classmethod
    def build_dictionary(cls, filenames, workers=1, threshold=-1, nwords=-1, padding_factor=8):
        if EmvTask.is_episodes_files(filenames):
            d = EpisodeDictionary(img_embedding_file=os.getenv('EM_IMG_EMBEDDING_FILE'))
            d.lazy_load_img_embeddings()  # No lazy loading during preprocessing!
            for filename in filenames:
                Dictionary.add_file_to_dictionary(
                    filename, d, EpisodeDictionary.tokenize_episode_json, workers)
            d.finalize(threshold=threshold, nwords=nwords, padding_factor=padding_factor)
            return d
        elif EmvTask.is_time_refs_files(filenames):
            return TimeRefDictionary()
        else:
            return super().build_dictionary(filenames, workers, threshold, nwords, padding_factor)

    @classmethod
    def load_dictionary(cls, filename):
        if 'episodes' in filename:
            return EpisodeDictionary.load(filename)
        return super().load_dictionary(filename)

    @staticmethod
    def add_args(parser):
        parser.add_argument('data', metavar='FILE',
                            help='file prefix for data')
        parser.add_argument('--max-source-positions', default=1024, type=int, metavar='N',
                            help='max number of tokens in the source sequence')
        parser.add_argument('--max-target-positions', default=1024, type=int, metavar='N',
                            help='max number of tokens in the target sequence')

        parser.add_argument('--unique-experiment-id', type=int,
                            default=int(time.time() * 1000),
                            help='Unique id for this experiment run. No need to set this by hand. '
                                 'This just serves as a hack to get the same id in all workers for DDP training, '
                                 'since the argument parser definitely only gets created once.')
        parser.add_argument('--external-data-generator-host', type=str, metavar='ADDR',
                            help=' of external data generator server')
        parser.add_argument('--external-data-generator-port', default=12345, type=int, metavar='N',
                            help='Port of external data generator server')
        parser.add_argument('--reload-dataset-interval', default=3, type=int, metavar='N',
                            help='How many epochs a dataset should be used until loading the next one')
        parser.add_argument('--subsample-dataset', default=1, type=float, metavar='N',
                            help='Train dataset subsample percentage')
        parser.add_argument('--no-episode-data', action='store_true', default=False,
                            help='Ignore all episode data. This will train & evaluate a QA-only model, which can be '
                                 'used to compare performance with / without episode data (i.e. estimate how good the '
                                 'performance can be based on prior knowledge only)')

        parser.add_argument('--test-print-full-episode-data', action='store_true', default=False,
                            help='Print complete episode data during inference. '
                                 'If not, print only episode id for lookup in episode file.')

        EpisodeRegularizingTaskMixin.add_args(parser)
        CombinedLoss.add_args_for(parser, ['answer', 'episode', 'time_ref'])
        DatetimeLoss.add_args(parser)

    @classmethod
    def setup_task(cls, args, **kwargs):
        data = args.data.split(':')[0] if ':' in args.data else args.data
        questions_vocab = Dictionary.load(os.path.join(data, 'dict.questions.txt'))
        answers_vocab = Dictionary.load(os.path.join(data, 'dict.answers.txt'))
        if args.no_episode_data:
            em_vocab = EpisodeDictionary()
        else:
            em_vocab = EpisodeDictionary.load(os.path.join(data, 'dict.episodes.txt'))
        print('| [questions] dictionary: {} types'.format(len(questions_vocab)))
        print('| [answers] dictionary: {} types'.format(len(answers_vocab)))
        print('| [EM] dictionary: {} types'.format(len(em_vocab)))
        return EmvTask(args, questions_vocab, answers_vocab, em_vocab)

    def __init__(self, args, question_vocab, answer_vocab, em_vocab):
        super().__init__(args)
        self.question_vocab = question_vocab
        self.answer_vocab = answer_vocab
        self.episodes_vocab = em_vocab
        self.tgt_time_ref_vocab = TimeRefDictionary()

        data_host = getattr(args, 'external_data_generator_host', None)
        if data_host:
            self.previous_received_dataset_path = None
            self.fetch_data_url = f'http://{data_host}:{args.external_data_generator_port}' \
                                  f'/{args.unique_experiment_id}'
        else:
            self.fetch_data_url = None

        if args.no_episode_data:
            self.episode_data_regularization = None
        else:
            self.init_episode_data_regularization()

    def load_dataset(self, split, combine=False, epoch=0, **kwargs):
        if split in self.datasets and epoch % self.args.reload_dataset_interval != 0:
            # Don't need to reload if the dataset already exists and epoch is not a multiple of reload_dataset_interval
            # Note that in case self.args.data contains no ':', fairseq doesn't even call load_dataset multiple times
            return

        data_path = self.args.data.split(':')[0] if ':' in self.args.data else self.args.data
        if self.fetch_data_url and epoch > 0:  # Use the provided dataset for the first reload_dataset_interval epochs
            if self.previous_received_dataset_path and self.previous_received_dataset_path.is_dir():
                shutil.rmtree(self.previous_received_dataset_path,
                              onerror=lambda *args: print('Error deleting previous dataset dir', *args))
            try:
                url = self.fetch_data_url + f'/{epoch}'
                print(f'| Fetching new dataset path from {url}...')
                start_waiting = time.time()
                r = requests.get(url)
                duration = time.time() - start_waiting
                if duration > 10:
                    print(f'| Waited for {round(duration)}s for new dataset to be generated!')
                data_path = r.text
                self.previous_received_dataset_path = Path(data_path)
            except RequestException:
                print(f'Could not fetch new dataset from external data generator at "{self.fetch_data_url}"')
                traceback.print_exc()

        if split in self.datasets:
            previous = self.datasets[split]
            if previous in self.dataset_to_epoch_iter:
                prev_iter = self.dataset_to_epoch_iter[previous]
                delattr(prev_iter, 'dataset')
                delattr(prev_iter, 'collate_fn')
                del self.dataset_to_epoch_iter[previous]
            del self.datasets[split]
        if torch.cuda.is_available():
            torch.cuda.empty_cache()

        dataset = load_emv_dataset(split, data_path, self.question_vocab, self.answer_vocab,
                                   self.episodes_vocab, self.tgt_time_ref_vocab, self.args.dataset_impl,
                                   self.args.max_source_positions, self.args.max_target_positions, combine,
                                   self.episode_data_regularization, self.args.no_episode_data)
        self.datasets[split] = dataset

    def get_batch_iterator(self, dataset, max_tokens=None, max_sentences=None, max_positions=None,
                           ignore_invalid_inputs=False, required_batch_size_multiple=1, seed=1, num_shards=1,
                           shard_id=0, num_workers=0, epoch=0):
        is_valid = dataset is self.datasets.get(getattr(self.args, 'valid_subset', 'valid'), None)
        if self.args.subsample_dataset < 1 and not is_valid \
                and not isinstance(dataset, EpisodeDataset):  # Don't want to subsample for regularization init
            used_dataset = fairseq.data.SubsampleDataset(dataset, size_ratio=self.args.subsample_dataset)
            used_dataset.shuffle = epoch < getattr(self.args, 'curriculum', 0)
        else:
            used_dataset = dataset
        if epoch < getattr(self.args, 'curriculum', 0) and not is_valid:
            print('| Using curriculum learning for epoch', epoch)
            return fairseq.data.iterators.EpochBatchIterator(
                dataset=used_dataset,
                collate_fn=dataset.collater,
                batch_sampler=BatchSampler(SequentialSampler(used_dataset),
                                           batch_size=self.args.max_sentences,
                                           drop_last=False),
                seed=seed,
                num_shards=num_shards,
                shard_id=shard_id,
                num_workers=num_workers,
                epoch=epoch,
            )
        print('| Creating batch iterator...', end='', flush=True)
        start = time.time()
        result = super().get_batch_iterator(used_dataset, max_tokens, max_sentences,
                                            max_positions, ignore_invalid_inputs,
                                            required_batch_size_multiple, seed,
                                            num_shards, shard_id, num_workers, epoch)
        print(' done, took', round(time.time() - start), 'seconds')
        return result

    def get_initializing_episode_dataset(self) -> EpisodeDataset:
        return EpisodeDataset(self.datasets['train'].episodes, self.episodes_vocab)

    def build_criterion(self, args):
        criteria = {
            'answer': super().build_criterion(args),
            'episode': EpisodeReconstructionLoss(args, self),
            'time_ref': DatetimeLoss(args, self),
        }
        return CombinedLoss(args, self, underlying_criteria=criteria, weights={
            k: getattr(args, 'loss_weight_' + k, 1 / len(criteria))
            for k in criteria.keys()
        })

    def aggregate_logging_outputs(self, logging_outputs, criterion):
        if isinstance(criterion, CombinedLoss):
            outputs = criterion.aggregate_logging_outputs(logging_outputs)
        else:
            outputs = super().aggregate_logging_outputs(logging_outputs, criterion)
        if not any('semantic_score' in o for o in logging_outputs):
            return outputs

        semantic_scores = [o['semantic_score']
                           for o in logging_outputs
                           if 'semantic_score' in o and o['semantic_score'] is not None]
        if len(semantic_scores) > 0:
            outputs['semantic_score'] = reduce(lambda x, y: x + y, semantic_scores, 0)
            outputs['semantic_score'] /= len(semantic_scores)
        else:
            outputs['semantic_score'] = 0
        return outputs

    def valid_step(self, sample, model, criterion):
        if random.random() < 0.4:
            buffer: List[Tuple[str, str, str]] = []
            model.set_append_to_results_buffer(lambda result: buffer.append(result))
            try:
                loss, sample_size, logging_output = super().valid_step(sample, model, criterion)
                buffer = [(self.question_vocab.string(s),
                           self.answer_vocab.string(t),
                           self.answer_vocab.string(h)) for s, t, h in buffer]
                lines = '\n'.join(f'S- {s}\nT- {t}\nH- 1 {h}' for s, t, h in buffer).splitlines()
                try:
                    score, total = semantic_eval(lines)
                except Exception as e:
                    print('Exception during semantic_eval:', sys.stderr)
                    traceback.print_exc(file=sys.stderr)
                    score, total = 0, 1
                if total == 0:
                    logging_output['semantic_score'] = None
                else:
                    logging_output['semantic_score'] = score / total
            finally:
                model.set_append_to_results_buffer(None)
            return loss, sample_size, logging_output
        else:
            loss, sample_size, logging_output = super().valid_step(sample, model, criterion)
            logging_output['semantic_score'] = None
            return loss, sample_size, logging_output

    def inference_step(self, generator, models, sample, prefix_tokens=None):
        if not self.args.quiet:
            for i, sample_id in enumerate(sample['id'].tolist()):
                if self.args.test_print_full_episode_data:
                    episode = sample['net_input']['episode_tokens'][i]
                    decoded = self.reverse_episode_regularization(episode)
                    episode_string = self.episodes_dictionary.string(decoded)
                    print('E-{}\t{}'.format(sample_id, episode_string))
                else:
                    dataset: EmvDataset = self.datasets[self.args.gen_subset]
                    episode_id = dataset.episode_refs[sample_id]
                    print('E-{}\t{}'.format(sample_id, episode_id))
        return super().inference_step(generator, models, sample, prefix_tokens)

    def max_positions(self):
        """Return the max input length allowed by the task."""
        # The source should be less than *args.max_positions* and the "target"
        # has max length 1.
        return self.args.max_source_positions, self.args.max_target_positions

    @property
    def source_dictionary(self):
        """Return the source :class:`~fairseq.data.Dictionary`."""
        return self.question_vocab

    @property
    def target_dictionary(self):
        """Return the target :class:`~fairseq.data.Dictionary`."""
        return self.answer_vocab

    @property
    def episodes_dictionary(self):
        """Return the episodes data :class:`~fairseq.data.Dictionary`."""
        return self.episodes_vocab

    @property
    def tgt_time_ref_dictionary(self):
        """Return the tgt time ref :class:`~fairseq.data.Dictionary`."""
        return self.tgt_time_ref_vocab
