import argparse
import os
from argparse import ArgumentParser, ArgumentError
from typing import Dict

import torch
from fairseq.data import Dictionary
from fairseq.tasks import register_task
from fairseq.tasks.translation import TranslationTask
from transformers import T5Tokenizer


class T5Dictionary(Dictionary):

    def __init__(self, t5_tokenizer: T5Tokenizer):
        super().__init__()
        self.t5_tokenizer = t5_tokenizer
        vocab: Dict[str, int] = t5_tokenizer.get_vocab()
        self.indices = vocab
        self.symbols = ['' for i in range(len(vocab))]
        for word, index in vocab.items():
            self.symbols[index] = word

        self.bos_word, self.pad_word, self.eos_word, self.unk_word = \
            t5_tokenizer.bos_token, t5_tokenizer.pad_token, \
            t5_tokenizer.eos_token, t5_tokenizer.unk_token
        self.bos_index, self.pad_index, self.eos_index, self.unk_index = \
            t5_tokenizer.bos_token_id, t5_tokenizer.pad_token_id, \
            t5_tokenizer.eos_token_id, t5_tokenizer.unk_token_id
        self.nspecial = len(t5_tokenizer.all_special_tokens)

    def encode_line(self, line, line_tokenizer=None,
                    add_if_not_exist=True,
                    consumer=None, append_eos=True,
                    reverse_order=False):
        tokens_list = self.t5_tokenizer.encode(text=line)
        if append_eos:
            tokens_list.append(self.eos_index)
        return torch.LongTensor(tokens_list)

    def string(self, tensor, bpe_symbol=None, escape_unk=False):
        if not torch.is_tensor(tensor) or tensor.dim() != 1:
            return super().string(tensor, bpe_symbol, escape_unk)
        else:
            return self.t5_tokenizer.decode(tensor)


@register_task('pretrained_t5_task')
class PretrainedT5TranslationTask(TranslationTask):
    """
    Example usage (add model configuration args, this shows only how to setup the task):

    export T5_PRETRAINED="t5-small"
    export T5_PRETRAINED_CACHE_DIR=~/model-cache
    fairseq-train \
        --task pretrained_t5_task \
        -a hf_t5 \
        --source-lang english --target-lang german \
        --pretrained $T5_PRETRAINED \
        --pretrained-cache-dir $T5_PRETRAINED_CACHE_DIR \
        --user-dir ~/path/to/code \
        --unfreeze-t5-enc-dec "encoder,decoder" \
        data-bin
    """

    @classmethod
    def build_dictionary(cls, filenames, workers=1, threshold=-1, nwords=-1, padding_factor=8):
        # Dirty hack because fairseq doesn't pass command line args to build_dictionary
        args = argparse.Namespace()
        args.pretrained = os.environ['T5_PRETRAINED']
        args.pretrained_cache_dir = os.environ['T5_PRETRAINED_CACHE_DIR']
        d = cls.create_t5_vocab(args)
        return d

    @staticmethod
    def add_args(parser: ArgumentParser):
        TranslationTask.add_args(parser)
        PretrainedT5TranslationTask.add_pretrained_args(parser)

    @staticmethod
    def add_pretrained_args(parser):
        try:
            parser.add_argument('--pretrained', type=str, metavar='path',
                                help='Use the pretrained tokenizer model with that name.')
            parser.add_argument('--pretrained-cache-dir', type=str, metavar='path',
                                help='Cache for pretrained models.')
        except ArgumentError:
            # The model already added this arguments...
            # Still need the code above if evaluating
            pass

    @classmethod
    def setup_task(cls, args, **kwargs):
        t5_vocab = cls.create_t5_vocab(args)

        print('| [text] dictionary: {} types'.format(len(t5_vocab)))

        return PretrainedT5TranslationTask(args=args, src_dict=t5_vocab, tgt_dict=t5_vocab)

    @classmethod
    def create_t5_vocab(cls, args):
        t5_tokenizer = T5Tokenizer.from_pretrained(
            args.pretrained,
            cache_dir=getattr(args, 'pretrained_cache_dir', None)
        )
        t5_vocab = T5Dictionary(t5_tokenizer)
        return t5_vocab
