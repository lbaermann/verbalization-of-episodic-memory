# Adapted from hf_gpt2.py

import logging
import os
import sys
from itertools import chain
from typing import Dict, List, Optional

import torch
from fairseq.data import Dictionary
from fairseq.models import (
    FairseqIncrementalDecoder,
    register_model,
    register_model_architecture,
    FairseqEncoderDecoderModel, FairseqEncoder)
from fairseq.models.transformer import EncoderOut, TransformerEncoder
from fairseq.tasks import FairseqTask
from torch import nn

logger = logging.getLogger(__name__)

try:
    # Prepend the transformers submodule to the path, so that
    # it's prioritized over other installations. This allows
    # making local changes in the submodule.
    sys.path.insert(
        0, os.path.join(os.path.dirname(__file__), 'transformers', 'src')
    )
    from transformers import T5Config, T5Model, T5ForConditionalGeneration
    from transformers.modeling_t5 import T5Stack
except ImportError:
    raise ImportError(
        '\n\nPlease install huggingface/transformers with:'
        '\n\n  pip install transformers'
        '\n\nOr to make local edits, install the submodule:'
        '\n\n  git submodule update --init '
        'fairseq/models/huggingface/transformers'
    )

DEFAULT_MAX_TARGET_POSITIONS = 1024


@register_model('hf_t5')
class HuggingFaceT5EncoderDecoderModel(FairseqEncoderDecoderModel):

    def __init__(self, encoder, decoder):
        super().__init__(encoder, decoder)

    @staticmethod
    def add_args(parser):
        """Add model-specific arguments to the parser."""
        # fmt: off
        parser.add_argument('--pretrained', type=str, metavar='path',
                            help='Use the pretrained model with that name.'
                                 'No model parameters changing model size should be present.')
        parser.add_argument('--pretrained-cache-dir', type=str, metavar='path',
                            help='Cache for pretrained models.')
        parser.add_argument('--unfreeze-t5-enc-dec', type=str, metavar='param1,param2,...',
                            help='comma-seperated list of model parameter name prefixes '
                                 'of T5 encoder and decoder to unfreeze. '
                                 'By default, all parameters are freezed. '
                                 'Run T5ForConditionalGeneration().named_parameters() to see parameter names. '
                                 '(Only applies if using pretrained model)')

        parser.add_argument('--hidden-size', type=int, metavar='N',
                            help='hidden size (d_model)')
        parser.add_argument('--attention-head-size', type=int, metavar='N',
                            help='attention head size (d_kv)')
        parser.add_argument('--feedforward-size', type=int, metavar='N',
                            help='feedforward size (d_ff)')
        parser.add_argument('--num-attention-heads', type=int, metavar='N',
                            help='num attention heads (num_heads)')
        parser.add_argument('--num-layers', type=int, metavar='N',
                            help='num layers (num_layers)')
        parser.add_argument('--num-relative-attention-buckets', type=int, metavar='N',
                            help='relative_attention_num_buckets')
        parser.add_argument('--dropout', type=float, metavar='D',
                            help='dropout probability for all fully connected layers '
                                 'in the embeddings, encoder, and decoder')
        # fmt: on

    @classmethod
    def build_model(cls, args, task: FairseqTask):
        """Build a new model instance."""
        dictionary: Dictionary = task.target_dictionary

        if getattr(args, 'pretrained', None):
            model = T5ForConditionalGeneration.from_pretrained(
                args.pretrained,
                pad_token_id=dictionary.pad(),
                eos_token_id=dictionary.eos(),
                cache_dir=getattr(args, 'pretrained_cache_dir', None)
            )

            unfreeze: List[str] = list(filter(lambda s: len(s.strip()) > 0,
                                              getattr(args, 'unfreeze_t5_enc_dec', '').split(',')))
            for name, p in chain(model.named_parameters()):
                p.requires_grad = any(name.startswith(u) for u in unfreeze)

            r = range(len(dictionary), model.config.vocab_size)
            if len(r) > 0:
                print(f'| Adding {len(r)} unused_filler_symbols to T5 dictionary')
            for i in r:
                dictionary.add_symbol('_unused_filler_symbol_' + str(i))
        else:
            default_architecture(args)
            config = T5Config(
                output_attentions=True,
                vocab_size=len(dictionary),
                n_positions=args.max_target_positions,
                d_model=args.hidden_size,
                d_kv=args.attention_head_size,
                d_ff=args.feedforward_size,
                num_layers=args.num_layers,
                num_heads=args.num_attention_heads,
                relative_attention_num_buckets=args.num_relative_attention_buckets,
                dropout_rate=args.dropout,
                layer_norm_epsilon=1e-6,
                initializer_factor=1.0,
                is_encoder_decoder=True,
                pad_token_id=dictionary.pad(),
                eos_token_id=dictionary.eos(),
            )
            model = T5ForConditionalGeneration(config)

        # set zero embedding for padding symbol
        pad_idx = dictionary.pad()
        model.shared.weight.data[pad_idx].zero_()

        return cls(cls.build_encoder(model, task, args),
                   cls.build_decoder(model, task, args))

    @classmethod
    def build_encoder(cls, model: T5ForConditionalGeneration, task: FairseqTask, args):
        return HuggingFaceT5Encoder(model.encoder, task)

    @classmethod
    def build_decoder(cls, model: T5ForConditionalGeneration, task: FairseqTask, args):
        return HuggingFaceT5Decoder(model.decoder, model.lm_head, task)


class HuggingFaceT5Encoder(FairseqEncoder):

    def __init__(self, t5_encoder: T5Stack, task: FairseqTask):
        super().__init__(task.source_dictionary)
        self.t5_encoder = t5_encoder
        self.padding_idx = task.source_dictionary.pad()

    def forward(self, src_tokens, src_lengths=None, return_all_hiddens=False, **kwargs):
        # print('Encoder.forward', src_tokens.shape)
        if 'symb_episode_tokens' in kwargs:
            del kwargs['symb_episode_tokens']
            del kwargs['symb_episode_lengths']

        encoder_outputs = self.t5_encoder(input_ids=src_tokens, **kwargs)
        # last_hidden has dim (batch_size, sequence_length, hidden_size)
        # fairseq works with T x B x C
        last_hidden: torch.Tensor = encoder_outputs[0]
        last_hidden.transpose_(0, 1)

        encoder_padding_mask = src_tokens.eq(self.padding_idx)
        if not encoder_padding_mask.any():
            encoder_padding_mask = None

        return EncoderOut(
            encoder_out=last_hidden,  # T x B x C
            encoder_padding_mask=encoder_padding_mask,  # B x T
            encoder_embedding=None,  # B x T x C
            encoder_states=[] if return_all_hiddens else None,  # List[T x B x C]
        )

    def reorder_encoder_out(self, encoder_out, new_order):
        # noinspection PyTypeChecker
        no_self: TransformerEncoder = None
        return TransformerEncoder.reorder_encoder_out(no_self, encoder_out, new_order)


class HuggingFaceT5Decoder(FairseqIncrementalDecoder):

    def __init__(self, t5_decoder: T5Stack, lm_head: nn.Module, task):
        super().__init__(task.target_dictionary)
        self.t5_decoder = t5_decoder
        self.lm_head = lm_head

    def forward(
            self,
            prev_output_tokens,
            encoder_out=None,
            incremental_state: Optional[Dict[str, List[torch.Tensor]]] = None,
            **kwargs
    ):
        # print('Decoder.forward', prev_output_tokens.shape, encoder_out.encoder_out.shape)
        features, attn = self.extract_features(prev_output_tokens, encoder_out, incremental_state, **kwargs)
        lm_logits = self.output_layer(features)
        return lm_logits, attn

    def extract_features(self, prev_output_tokens,
                         encoder_out: EncoderOut = None,
                         incremental_state=None, **kwargs):
        # print('Decoder.extract_features', incremental_state)
        # fairseq EncoderOut is T x B x C
        # Huggingface T5 expects B x T x C
        hidden_states = encoder_out.encoder_out.transpose(0, 1)
        pad_mask = encoder_out.encoder_padding_mask
        converted_padding_mask = None if pad_mask is None else ~pad_mask
        out = self.t5_decoder(input_ids=prev_output_tokens,
                              encoder_hidden_states=hidden_states,
                              encoder_attention_mask=converted_padding_mask)
        # We do not return attn weights till now
        #  (does not correctly work with fairseq generate)
        return out[0], {'attn': None}

    def output_layer(self, features, **kwargs):
        # print('Decoder.output_layer', features.shape)
        # Rescale output before projecting on vocab
        features = features * (self.t5_decoder.config.d_model ** -0.5)
        lm_logits = self.lm_head(features)
        return lm_logits

    def max_positions(self):
        return self.t5_decoder.config.n_positions - 1


@register_model_architecture('hf_t5', 'hf_t5')
def default_architecture(args):
    args.max_target_positions = getattr(args, 'max_target_positions', 512)
    args.hidden_size = getattr(args, 'hidden_size', 512)
    args.attention_head_size = getattr(args, 'attention_head_size', 64)
    args.feedforward_size = getattr(args, 'feedforward_size', 2048)
    args.num_layers = getattr(args, 'num_layers', 6)
    args.num_attention_heads = getattr(args, 'num_attention_heads', 8)
    args.num_relative_attention_buckets = getattr(args, 'num_relative_attention_buckets', 32)
    args.dropout = getattr(args, 'dropout', 0.1)
