import os
from argparse import ArgumentParser

from fairseq.tasks import register_task

from model import EmvTask
from model.em import EpisodeDictionary
from model.t5.t5_task import PretrainedT5TranslationTask


@register_task('pretrained_t5_emv_task')
class PretrainedT5EmvTask(EmvTask):

    @classmethod
    def build_dictionary(cls, filenames, workers=1, threshold=-1, nwords=-1, padding_factor=8):
        if EmvTask.is_episodes_files(filenames) or EmvTask.is_time_refs_files(filenames):
            ref = EmvTask
        else:
            ref = PretrainedT5TranslationTask
        return ref.build_dictionary(filenames, workers, threshold, nwords, padding_factor)

    @staticmethod
    def add_args(parser: ArgumentParser):
        EmvTask.add_args(parser)
        PretrainedT5TranslationTask.add_pretrained_args(parser)

    @classmethod
    def setup_task(cls, args, **kwargs):
        data = args.data.split(':')[0] if ':' in args.data else args.data
        t5_vocab = PretrainedT5TranslationTask.create_t5_vocab(args)
        if args.no_episode_data:
            em_vocab = EpisodeDictionary()
        else:
            em_vocab = EpisodeDictionary.load(os.path.join(data, 'dict.episodes.txt'))

        print('| [text] dictionary: {} types'.format(len(t5_vocab)))
        print('| [EM] dictionary: {} types'.format(len(em_vocab)))

        return PretrainedT5EmvTask(args, t5_vocab, t5_vocab, em_vocab)
