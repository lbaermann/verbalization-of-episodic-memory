from fairseq.models import register_model, register_model_architecture
from transformers import T5ForConditionalGeneration

from .hf_t5 import HuggingFaceT5EncoderDecoderModel, default_architecture
from .. import EmvTask
from ..emv_model import BaseEmvModel, prototype_emv_linear_architecture, prototype_emv_lstm_architecture
from ..em import LinearEmEncoder, LstmEmEncoder


def _cls_emv_t5_model(em_enc_class):
    class _EmvT5Model(BaseEmvModel, HuggingFaceT5EncoderDecoderModel):
        @staticmethod
        def add_args(parser):
            BaseEmvModel.add_model_args(em_enc_class, parser)
            HuggingFaceT5EncoderDecoderModel.add_args(parser)

        @classmethod
        def build_model(cls, args, task: EmvTask):
            model = super().build_model(args, task)
            model.print_additional_output = args.print_additional_output
            model.additional_decoders = super().build_additional_decoders(args, task)
            model.init_pretrained_episode_autoencoder(args)
            model.freeze_model_params(args)
            return model

        @classmethod
        def build_encoder(cls, model: T5ForConditionalGeneration, task: EmvTask, args):
            assert isinstance(task, EmvTask)
            question_encoder = super().build_encoder(model, task, args)
            args.encoder_embed_dim = question_encoder.t5_encoder.config.d_model
            return super().build_compound_encoder(
                em_enc_class,
                question_encoder, task.episodes_dictionary, args)

    return _EmvT5Model


EmvT5LinearModel = _cls_emv_t5_model(LinearEmEncoder)
EmvT5LstmModel = _cls_emv_t5_model(LstmEmEncoder)
register_model('baseline_emv_t5_linear')(EmvT5LinearModel)
register_model('baseline_emv_t5_lstm')(EmvT5LstmModel)


@register_model_architecture('baseline_emv_t5_linear', 'baseline_emv_linear_arch')
def baseline_emv_linear_arch(args):
    default_architecture(args)
    args.episode_enc_hidden_dim = getattr(args, 'episode_enc_hidden_dim', 512)
    prototype_emv_linear_architecture(args)


@register_model_architecture('baseline_emv_t5_lstm', 'baseline_emv_lstm_arch')
def baseline_emv_lstm_arch(args):
    default_architecture(args)
    args.episode_enc_hidden_dim = getattr(args, 'episode_enc_hidden_dim', 512)
    prototype_emv_lstm_architecture(args)
