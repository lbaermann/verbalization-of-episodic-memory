import os
from functools import lru_cache
from pathlib import Path
from typing import List

import numpy as np
import torch
from fairseq.data import Dictionary, LanguagePairDataset, data_utils, MMapIndexedDataset
from fairseq.tasks.translation import load_langpair_dataset
from torch.utils.data import Dataset

from .em import EpisodeDictionary
from .em.episode_dataset import EpisodeDataset
from .em.regularize import RegularizationLayer


# Hack
def hack_mmap_indexed_dataset():
    @lru_cache(maxsize=8)
    def __getitem__(self, i):
        ptr, size = self._index[i]
        np_array = np.frombuffer(self._bin_buffer, dtype=self._index.dtype, count=size, offset=ptr)
        # Avoid explicit conversion to int64 for all float64 tensors here.
        if self._index.dtype != np.float64 and self._index.dtype != np.int64:
            np_array = np_array.astype(np.int64)
        return torch.from_numpy(np_array)

    MMapIndexedDataset.__getitem__ = __getitem__


hack_mmap_indexed_dataset()


def load_emv_dataset(split, data,
                     question_vocab, answer_vocab, episodes_vocab, tgt_time_ref_vocab,
                     dataset_impl, max_source_positions, max_target_positions,
                     combine=False, episode_data_regularization: RegularizationLayer = None,
                     no_episodes_data_mode=False):
    """Load a given dataset split (e.g., train, valid, test)."""

    text_dataset = load_langpair_dataset(
        data, split,
        src='questions', src_dict=question_vocab,
        tgt='answers', tgt_dict=answer_vocab,
        combine=combine, dataset_impl=dataset_impl,
        upsample_primary=False,
        left_pad_source=False,
        left_pad_target=False,
        max_source_positions=max_source_positions,
        max_target_positions=max_target_positions, load_alignments=False,
        truncate_source=False
    )
    questions, question_lengths = text_dataset.src, text_dataset.src_sizes
    answers, answer_lengths = text_dataset.tgt, text_dataset.tgt_sizes

    if no_episodes_data_mode:
        empty_episode = '[{' \
                        '"started": "2020 01 01 0 00 00 00", "ended": "2020 01 01 0 00 00 00", ' \
                        '"keyframes": {"2020 01 01 0 00 00 00": {}}' \
                        '}]'
        episodes = [episodes_vocab.encode_line(empty_episode).long()]
        episode_ids = [0] * len(questions)
    else:
        episodes, episode_ids = _load_episode_dataset(split, data, episodes_vocab)
    tgt_time_refs = _load_time_refs(split, data, tgt_time_ref_vocab)

    assert len(questions) == len(answers), f'{len(questions)} != {len(answers)}'
    assert len(questions) == len(episode_ids), f'{len(questions)} != {len(episode_ids)}'
    print('| {} {} {} examples'.format(data, split, len(questions)))

    return EmvDataset(episodes=episodes, episodes_dict=episodes_vocab, episode_refs=episode_ids,
                      tgt_time_refs=tgt_time_refs, tgt_time_ref_dict=tgt_time_ref_vocab, src=questions,
                      src_sizes=question_lengths, src_dict=question_vocab, tgt=answers, tgt_sizes=answer_lengths,
                      tgt_dict=answer_vocab, left_pad_source=False, max_source_positions=max_source_positions,
                      max_target_positions=max_target_positions, input_feeding=True,
                      episode_data_regularization=None if no_episodes_data_mode else episode_data_regularization)


def _load_episode_dataset(split, data, episodes_vocab):
    prefix = os.path.join(data, split + '.')
    episodes = data_utils.load_indexed_dataset(
        prefix + 'episodes-None.episodes',
        episodes_vocab)

    episode_ids_file = Path(prefix + 'episode_ids')
    if episode_ids_file.exists():
        print('| Using episode indirection data for', data, split)
        episode_ids = list(int(l) for l in episode_ids_file.read_text().splitlines())
    else:
        episode_ids = list(range(len(episodes)))
    return episodes, episode_ids


def _load_time_refs(split, data, tgt_time_ref_vocab):
    time_refs_file = Path(data, split + '.time_refs-None.time_refs')
    bin_file = time_refs_file.with_name(time_refs_file.name + '.bin')
    if bin_file.exists():
        return data_utils.load_indexed_dataset(
            str(time_refs_file),
            tgt_time_ref_vocab)
    else:
        return None


class EmvDataset(LanguagePairDataset):

    def __init__(self, episodes: Dataset, episodes_dict: EpisodeDictionary, episode_refs: List[int],
                 tgt_time_refs: Dataset, tgt_time_ref_dict: Dictionary, src: Dataset, src_sizes: List[int],
                 src_dict: Dictionary, tgt: Dataset = None, tgt_sizes=None, tgt_dict=None, left_pad_source=True,
                 left_pad_target=False, max_source_positions=1024, max_target_positions=1024, shuffle=True,
                 input_feeding=True, remove_eos_from_source=False, append_eos_to_target=False, append_bos=False,
                 episode_data_regularization: RegularizationLayer = None):
        super().__init__(
            src, src_sizes, src_dict,
            tgt, tgt_sizes, tgt_dict,
            left_pad_source, left_pad_target,
            max_source_positions, max_target_positions,
            shuffle, input_feeding,
            remove_eos_from_source, append_eos_to_target,
            align_dataset=None, append_bos=append_bos
        )
        self.episodes = episodes
        self.episodes_dict = episodes_dict
        self.episode_refs = episode_refs
        self.tgt_time_refs = tgt_time_refs
        self.tgt_time_ref_dict = tgt_time_ref_dict
        self.episode_data_regularization = episode_data_regularization

    def __getitem__(self, index):
        example = super().__getitem__(index)
        example['episode'] = self.episodes[self.episode_refs[index]]
        if self.tgt_time_refs:
            example['tgt_time_ref'] = self.tgt_time_refs[index]
        return example

    def collater(self, samples):
        if len(samples) == 0:
            return {}

        batch = super().collater(samples)

        # noinspection PyCallByClass
        episode_tokens = EpisodeDataset.regularize_and_collate_episodes(
            self,
            [s['episode'] for s in samples],
            pad_idx=self.episodes_dict.pad(),
        )
        # noinspection PyArgumentList
        src_lengths = torch.LongTensor([s['source'].numel() for s in samples])
        src_lengths, sort_order = src_lengths.sort(descending=True)

        # noinspection PyArgumentList,PyCallByClass
        episode_lengths = EpisodeDataset.get_episode_lenghts(self, samples)
        episode_tokens = episode_tokens.index_select(0, sort_order)

        batch['net_input']['episode_tokens'] = episode_tokens
        batch['net_input']['episode_lengths'] = episode_lengths

        # multiple targets for multiple decoders
        # noinspection PyCallByClass
        batch['targets'] = {
            'answer': batch['target'],
            'episode': EpisodeDataset.get_autoencoder_targets(self, episode_tokens),
        }
        if self.tgt_time_refs:
            tgt_time_refs = torch.cat(tuple(s['tgt_time_ref'].unsqueeze(0) for s in samples), dim=0)
            tgt_time_refs = tgt_time_refs.index_select(0, sort_order)
            batch['targets']['time_ref'] = tgt_time_refs.type(torch.long)
        return batch

    @property
    def supports_prefetch(self):
        return False
        # return super().supports_prefetch() and getattr(self.episodes, 'supports_prefetch', False)

    @property
    def sizes(self):
        return self.src_sizes

    def prefetch(self, indices):
        super().prefetch(indices)
        if hasattr(self.episodes, 'prefetch'):
            self.episodes.prefetch(indices)
