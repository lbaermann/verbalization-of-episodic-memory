import json
import sys

import numpy as np
import torch

img_id_to_file = json.load(open(sys.argv[1]))
export_dict = {
    int(img_id): torch.tensor(np.load(img_file), dtype=torch.float)
    for img_id, img_file in img_id_to_file.items()
}
torch.save(export_dict, sys.argv[2])
