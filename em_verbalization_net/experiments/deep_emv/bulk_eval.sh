#!/bin/bash

./deep_emv_eval.sh best data-bin-1  ep1_best_test
./deep_emv_eval.sh best data-bin-2  ep2_best_test
./deep_emv_eval.sh best data-bin-3  ep3_best_test
./deep_emv_eval.sh best data-bin-5  ep5_best_test
./deep_emv_eval.sh best data-bin-10 ep10_best_test
./deep_emv_eval.sh last data-bin-5  ep5_last_test
./deep_emv_eval.sh last data-bin-10 ep10_last_test
