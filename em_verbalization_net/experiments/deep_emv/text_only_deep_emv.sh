#!/bin/bash

#rm checkpoints/checkpoint_best.pt
#rm checkpoints/checkpoint_last.pt
set -e

source ../experiment_setup.sh

common_args=(
  --no-episode-data
  --batch-size 128
  --num-workers 4
  --task pretrained_t5_emv_task
  --pretrained "$T5_PRETRAINED"
  --pretrained-cache-dir "$T5_PRETRAINED_CACHE_DIR"
  --user-dir ../../model
  -a baseline_emv_lstm_arch
  --episode-enc-embed-dim-dt 1
  --episode-enc-embed-dim-symb 1
  --episode-enc-embed-dim-subsymb 1
  --episode-enc-embed-dim-latent 1
  --episode-enc-hidden-dim 1
  --episode-enc-output-dim 1
  --episode-enc-dropout 0
  --save-interval 1
  --best-checkpoint-metric 'semantic_score'
  --maximize-best-checkpoint-metric
  --optimizer adam --lr 0.01 --lr-scheduler inverse_sqrt --warmup-init-lr 0.002 --warmup-updates 3000
  --unfreeze-t5-enc-dec "encoder,decoder.block.0"
  --loss-weight-episode 0.1 --loss-weight-time-ref 0.1 --loss-weight-answer 0.8
)

date

fairseq-train "${common_args[@]}" --me 20 --subsample-dataset 0.25 data-bin-5
