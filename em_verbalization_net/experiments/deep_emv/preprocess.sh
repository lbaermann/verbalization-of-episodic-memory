#!/bin/bash

rm data-bin/*
set -e

source ../experiment_setup.sh

export GENERATED_DATASET_BASE=../../data_generator
python preprocess_img_embeddings.py \
  "$GENERATED_DATASET_BASE/img_id_to_file.json" \
  "$PREPROCESS_DST_DIR/all_img_embeddings.torch"
source ../preprocess_emv_dataset.sh $GENERATED_DATASET_BASE $GENERATED_DATA_PREFIX
