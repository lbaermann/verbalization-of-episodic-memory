#!/bin/bash

CHECK=$1
DATA_BIN=$2
EVAL_NAME=$3
EVAL_ON=$4
if [[ -z $CHECK ]]; then
  CHECK='best'
fi
if [[ -z $CHECK ]]; then
  DATA_BIN='data-bin'
fi
if [[ -z $EVAL_ON ]]; then
  EVAL_ON='test'
fi
if [[ -z $EVAL_NAME ]]; then
  EVAL_NAME="$(basename $DATA_BIN)_${CHECK}_${EVAL_ON}"
fi
echo "Evaluating $CHECK on $DATA_BIN/$EVAL_ON set (as $EVAL_NAME)"

OUTPUT_FILE=results/output_$EVAL_NAME.txt

if [[ ! -d results ]]; then
  mkdir results
fi
if [[ -f $OUTPUT_FILE ]]; then
  echo "Output file $OUTPUT_FILE already exists!"
  exit 1
fi

source ../experiment_setup.sh

{
  git rev-parse HEAD
  date
  fairseq-generate \
    --num-workers 1 \
    --batch-size 100 \
    --max-len-a 0 --max-len-b 30 \
    --gen-subset "$EVAL_ON" \
    --task pretrained_t5_emv_task \
    --pretrained "$T5_PRETRAINED" \
    --pretrained-cache-dir "$T5_PRETRAINED_CACHE_DIR" \
    --user-dir ../../model \
    --no-progress-bar \
    --path "checkpoints/checkpoint_$CHECK.pt" \
    $DATA_BIN
} > "$OUTPUT_FILE"
if [[ 0 -ne $? ]]; then
  echo "fairseq-generate failed. Exiting."
  exit
fi

source _eval_generate_output.sh "$OUTPUT_FILE" >"results/stats_$EVAL_NAME.txt"
