#!/bin/bash

#rm checkpoints/checkpoint_best.pt
#rm checkpoints/checkpoint_last.pt
set -e

source ../experiment_setup.sh

common_args=(
  --batch-size 48
  --num-workers 4
  --task pretrained_t5_emv_task
  --pretrained "$T5_PRETRAINED"
  --pretrained-cache-dir "$T5_PRETRAINED_CACHE_DIR"
  --user-dir ../../model
  -a baseline_emv_lstm_arch
  --episode-enc-embed-dim-dt 32
  --episode-enc-embed-dim-symb 32
  --episode-enc-embed-dim-subsymb 32
  --episode-enc-embed-dim-latent 32
  --episode-enc-hidden-dim 64
  --episode-enc-output-dim 128
  --episode-enc-dropout 0.01
  --episode-enc-symb-embed-dropout 0.05
  --save-interval 1
  --best-checkpoint-metric 'semantic_score'
  --maximize-best-checkpoint-metric
  --optimizer adam --lr 0.01 --lr-scheduler inverse_sqrt --warmup-init-lr 0.002 --warmup-updates 3000
  --unfreeze-t5-enc-dec "encoder,decoder.block.0"
  --loss-weight-episode 0.1 --loss-weight-time-ref 0.1 --loss-weight-answer 0.8
)

printf "\n\n%s\n\n" "$(date)"
fairseq-train "${common_args[@]}" --me  8 --subsample-dataset 0.2  data-bin-1
fairseq-train "${common_args[@]}" --me 12 --subsample-dataset 0.15 data-bin-2
fairseq-train "${common_args[@]}" --me 16 --subsample-dataset 0.1  data-bin-3
fairseq-train "${common_args[@]}" --me 20 --subsample-dataset 0.05 data-bin-5
