#!/bin/bash
set -e

export CUDA_VISIBLE_DEVICES='' # Don't use GPU for one sample
source ../experiment_setup.sh >/dev/null

mkdir tmp-data
if [[ -z $1 ]]; then
  echo 'First argument must be input query'
  exit 1
fi
if [[ -z $2 ]]; then
  echo 'Second argument must be episode JSON data'
  exit 1
fi
if [[ -z $CHECK ]]; then
  CHECK='best'
fi
if [[ -z $EVAL_ON ]]; then
  EVAL_ON='test'
fi

echo "$1" >tmp-data/test.questions-answers.questions
echo "--" >tmp-data/test.questions-answers.answers
echo "$2" >tmp-data/test.episodes-None.episodes
cp data-bin/dict.episodes.txt tmp-data/dict.episodes.txt

output=$(
  fairseq-generate \
    --num-workers 1 \
    --batch-size 1 \
    --max-len-a 0 --max-len-b 30 \
    --gen-subset $EVAL_ON \
    --task pretrained_t5_emv_task \
    --pretrained $T5_PRETRAINED \
    --pretrained-cache-dir $T5_PRETRAINED_CACHE_DIR \
    --user-dir ../../model \
    --no-progress-bar \
    --path checkpoints/checkpoint_$CHECK.pt \
    --dataset-impl raw \
    tmp-data
)
if [[ $output == *'H-0'* ]]; then
  echo "$output" | grep 'H-0'
else
  echo 'Failed!'
  echo "$output"
fi

rm -r tmp-data
