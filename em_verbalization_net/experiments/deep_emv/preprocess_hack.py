from fairseq_cli.preprocess import cli_main
import fairseq.data.indexed_dataset as data
import numpy as np


def make_builder_float(out_file, impl, vocab_size=None):
    if impl == 'mmap':
        return data.MMapIndexedDatasetBuilder(out_file, dtype=np.double)
    else:
        return data.IndexedDatasetBuilder(out_file, dtype=np.double)


data.make_builder = make_builder_float

print('Starting fairseq-preprocess with hacked dataset impl...')
cli_main()
