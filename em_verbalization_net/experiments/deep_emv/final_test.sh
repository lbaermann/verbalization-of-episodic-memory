#!/bin/bash
set -e

source ../experiment_setup.sh >/dev/null

mkdir tmp-data
if [[ ! -d $1 ]]; then
  echo 'First argument must be dataset input directory'
  exit 1
fi
if [[ ! -f $2 ]]; then
  echo 'Second argument must be path to model checkpoint file to evaluate'
  exit 1
fi
if [[ -z $3 ]]; then
  name='human_test'
else
  name=$3
fi
if [[ ! -f $1/$name.episodes ]]; then
  echo "$1/$name.episodes does not exist"
  exit 1
fi
if [[ ! -f $1/$name.questions ]]; then
  echo "$1/$name.questions does not exist"
  exit 1
fi
if [[ ! -f $1/$name.answers ]]; then
  echo "$1/$name.answers does not exist"
  exit 1
fi

cp "$1/$name.questions" tmp-data/test.questions-answers.questions
cp "$1/$name.answers" tmp-data/test.questions-answers.answers
echo >> tmp-data/test.questions-answers.answers
cp "$1/$name.episodes" tmp-data/test.episodes-None.episodes
if [[ -f $1/$name.episode_ids ]]; then
  cp "$1/$name.episode_ids" tmp-data/test.episode_ids
fi
if [[ -d data-bin-1 ]]; then
  cp data-bin-1/dict.episodes.txt tmp-data/dict.episodes.txt
else
  cp data-bin/dict.episodes.txt tmp-data/dict.episodes.txt
fi

export BLOCK_PRETRAINED_EPISODE_AUTOENC_LOADING=true
fairseq-generate \
  --num-workers 1 \
  --batch-size 8 \
  --max-len-a 0 --max-len-b 30 \
  --gen-subset test \
  --task pretrained_t5_emv_task \
  --pretrained $T5_PRETRAINED \
  --pretrained-cache-dir $T5_PRETRAINED_CACHE_DIR \
  --user-dir ../../model \
  --no-progress-bar \
  --path "$2" \
  --dataset-impl raw \
  tmp-data

rm -r tmp-data
