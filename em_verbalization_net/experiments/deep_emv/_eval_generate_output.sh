if [[ -z $1 ]]; then
  echo 'first argument needs to be set to fairseq-generate output file!'
  exit 1
fi
if [[ -z $DATA_BIN ]]; then
  echo 'DATA_BIN needs to be set!'
  exit 1
fi
if [[ -z $EVAL_ON ]]; then
  echo 'EVAL_ON needs to be set!'
  exit 1
fi

export PYTHONPATH=$PYTHONPATH:../..
python -m eval.calc_gt_score "$1"
printf '\n\nSemantic eval\n'
python -m eval.semantic_eval "$1"
printf '\n\nCategorizing eval\n'
python -m eval.categorizing_eval \
  --episodes "$DATA_BIN/$EVAL_ON.episodes.raw" \
  --workers 8 \
  "$1"
