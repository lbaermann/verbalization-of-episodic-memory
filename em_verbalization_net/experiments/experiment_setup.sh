
source ../../venv/bin/activate

echo "Running on $(hostname -s)"
if [[ -n $CUDA_VISIBLE_DEVICES ]]; then
  echo "Using GPU $CUDA_VISIBLE_DEVICES"
else
  echo "CUDA_VISIBLE_DEVICES not set!"
fi

export NUM_WORKERS=2

export GENERATED_DATASET_BASE=../../../../emv_data_generator
export GENERATED_DATA_PREFIX="simulated."
export DATASET_BASE=../../../../../dataset/baseline_data_export
export DATA_PREFIX=""

export T5_PRETRAINED="t5-small"
export T5_PRETRAINED_CACHE_DIR=../../cache

export DATA_GENERATOR_HOST_CURRICULUM=localhost
export DATA_GENERATOR_HOST_SAMPLE_DIALOG=localhost
export PREPROCESS_DST_DIR=data-bin