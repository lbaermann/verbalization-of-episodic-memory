
if [[ -n $3 ]]; then
  TASK=$3
else
  TASK='pretrained_t5_emv_task'
fi

fairseq-preprocess \
    --workers $NUM_WORKERS \
    --user-dir ../../model \
    --task $TASK \
    -s questions \
    -t answers \
    --destdir "$PREPROCESS_DST_DIR" \
    --trainpref $1/${2}train \
    --validpref $1/${2}valid \
    --testpref $1/${2}test \
    --joined-dictionary

if [[ -f preprocess_hack.py ]]; then
  em_preprocess_cmd="python preprocess_hack.py --workers $NUM_WORKERS"
else
  em_preprocess_cmd='fairseq-preprocess --dataset-impl raw'
fi

EM_IMG_EMBEDDING_FILE="$PREPROCESS_DST_DIR/all_img_embeddings.torch"
if [[ -f $EM_IMG_EMBEDDING_FILE ]]; then
  export EM_IMG_EMBEDDING_FILE
fi

$em_preprocess_cmd \
    --user-dir ../../model \
    --task $TASK \
    -s episodes \
    --only-source \
    --destdir "$PREPROCESS_DST_DIR" \
    --trainpref $1/${2}train \
    --validpref $1/${2}valid \
    --testpref $1/${2}test

if [[ -f $1/${2}train.time_refs ]]; then
fairseq-preprocess \
    --workers $NUM_WORKERS \
    --user-dir ../../model \
    --task $TASK \
    -s time_refs \
    --only-source \
    --destdir "$PREPROCESS_DST_DIR" \
    --trainpref $1/${2}train \
    --validpref $1/${2}valid \
    --testpref $1/${2}test
fi

# Copy Episode indirection files if present
cp $1/${2}train.episode_ids "$PREPROCESS_DST_DIR/train.episode_ids" || :
cp $1/${2}valid.episode_ids "$PREPROCESS_DST_DIR/valid.episode_ids" || :
cp $1/${2}test.episode_ids "$PREPROCESS_DST_DIR/test.episode_ids" || :

# Copy raw Episodes file for categorizing_eval.py
cp $1/${2}test.episodes "$PREPROCESS_DST_DIR/test.episodes.raw" || :
