#!/bin/bash

source ../experiment_setup.sh
export PYTHONPATH=$PYTHONPATH:../..

python -c "from model.em.auto_encoder import eval_model; eval_model()" \
    --batch-size 128 \
    --num-generator-workers 0 \
    --valid-size-factor 5 \
    --path checkpoints/checkpoint_best.pt \
    data-bin:
