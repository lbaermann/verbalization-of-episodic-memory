#!/bin/bash

source ../experiment_setup.sh

python -c "from torch.multiprocessing import set_start_method; set_start_method('spawn'); from fairseq_cli.train import cli_main; cli_main()" \
    --num-generator-workers 2 \
    --num-workers 0 \
    --user-dir ../../model \
    --task episode_auto_encoder_task \
    --max-sentences 64 \
    --max-sentences-valid 16 \
    -a episode_auto_encoder_lstm_arch \
    --encoder-embed-dim 128 \
    --episode-enc-embed-dim-dt 32 \
    --episode-enc-embed-dim-symb 32 \
    --exclude-symbolic-data \
    --episode-enc-embed-dim-subsymb 32 \
    --episode-enc-embed-dim-latent 32 \
    --episode-enc-hidden-dim 64 \
    --episode-enc-dropout 0.01 \
    --episode-enc-symb-embed-dropout 0.05 \
    --latent-embed-init-weights checkpoints/latent_embed_layer.torch \
    --no-epoch-checkpoints \
    --valid-size-factor 2 \
    --epoch-size-factor 3 \
    --regenerate-interval 50 \
    --optimizer adam --lr 0.01 --lr-scheduler inverse_sqrt --warmup-init-lr 0.001 --warmup-updates 1000 \
    --me 600 \
    data-bin:
    # We actually use live generated data.
    # Data path is for dictionary and episode generation template files
    # : is for fairseq-train to reload dataset after each epoch.
