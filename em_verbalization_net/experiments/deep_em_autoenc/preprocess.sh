#!/bin/bash

rm data-bin/*

source ../experiment_setup.sh

export DATASET_BASE=../../../../dataset-collector-server/export_episodes
source ../preprocess_emv_dataset.sh $DATASET_BASE $DATA_PREFIX

rm data-bin/*answers*
rm data-bin/*questions*
