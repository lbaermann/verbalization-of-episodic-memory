import collections
import os
import random
import subprocess
import sys
import time
import traceback
from argparse import ArgumentParser
from collections import namedtuple
from multiprocessing import Pool, JoinableQueue, Manager
from pathlib import Path
from typing import List, Tuple, Iterable

from click import progressbar

from data_generator.episode_grammar import episode_grammar
from data_generator.episodes import *
from data_generator.episodes.generator import generate_histories_based_on
from data_generator.grammar import generate_all_words


def generate_dialog_for_episode(episode_list: EpisodeList, idx, answers, questions, episode_ids, time_refs,
                                sub_sample: bool, few_dialogs_per_episode: bool) -> Tuple[int, int]:
    question_time = max(episode.question_time for episode in episode_list)
    try:
        grammar = episode_grammar(episode_list, question_time, simple=False)
        if few_dialogs_per_episode:
            words = iter(grammar.draw_sample(random) for i in range(10))
        else:
            words = generate_all_words(grammar)
    except:
        print(f'Failed dialog generation for {episode_list}')
        traceback.print_exc()
        words = []
    error_dialogs, total = 0, 0
    logged_empty_dialog = False
    for w in words:
        if sub_sample and random.random() > 0.5:
            continue
        total += 1
        if len(w) == 0:
            if not logged_empty_dialog:
                print(f'Empty dialog for {[e.encode() for e in episode_list]}')
                logged_empty_dialog = True
            error_dialogs += 1
            continue
        split = w.split('\n')
        assert len(split) == 3, f'"{w}"'
        if 'ERROR' in split:
            error_dialogs += 1
            continue
        time_refs.append(split[0])
        questions.append(question_time.strftime('%Y %m %d %w %H %M %S') + ' ' + split[1])
        answers.append(split[2])
        episode_ids.append(idx)
    return error_dialogs, total


DatasetSplit = namedtuple('DatasetSplit', [
    'episode_ids', 'questions', 'answers', 'time_refs', 'episode_strings'
])


class _ListSubsetView(collections.Sequence):

    def __init__(self, original_list: List, index_list: List[int]):
        self.original_list = original_list
        self.index_list = index_list

    def __len__(self):
        return len(self.index_list)

    def __getitem__(self, i):
        return self.original_list[self.index_list[i]]


class _IntModifyingListSubsetView(_ListSubsetView):

    def __init__(self, original_list: List, index_list: List[int], subtrahend: int):
        super().__init__(original_list, index_list)
        self.subtrahend = subtrahend

    def __getitem__(self, i):
        return super().__getitem__(i) - self.subtrahend


def create_shards(dataset: DatasetSplit, num_shards: int):
    # A shard needs to be self contained, i.e. all episode references need to be inside itself
    # Therefore, start by sharding the episodes.
    # This assumes dialogs are identically distributed amongst the episodes (which is approximately true)
    num_episodes = len(dataset.episode_strings)
    shard_length = num_episodes // num_shards + (1 if num_episodes % num_shards > 0 else 0)
    shards = []
    print(f'Creating {num_shards} shards... Indexing...')
    index_lists: List[List[int]] = [[] for i in range(num_shards)]
    with progressbar(enumerate(dataset.episode_ids), length=len(dataset.episode_ids)) as iterator:
        for data_idx, ep_idx in iterator:
            shard_idx = ep_idx // shard_length
            index_lists[shard_idx].append(data_idx)
    print('Distributing...', end=' ', flush=True)
    for i in range(num_shards):
        my_slice = slice(i * shard_length, (i + 1) * shard_length)
        relevant_items = index_lists[i]
        shards.append(DatasetSplit(
            episode_ids=_IntModifyingListSubsetView(dataset.episode_ids, relevant_items, subtrahend=my_slice.start),
            questions=_ListSubsetView(dataset.questions, relevant_items),
            answers=_ListSubsetView(dataset.answers, relevant_items),
            time_refs=_ListSubsetView(dataset.time_refs, relevant_items),
            episode_strings=_ListSubsetView(dataset.episode_strings, list(range(my_slice.start, my_slice.stop)))
        ))
        print(i, end=' ', flush=True)
    print()
    return shards


def write_dataset(base, name, split: DatasetSplit, num_shards=1):
    shards = create_shards(split, num_shards) if num_shards > 1 else [split]
    for shard, local_split in enumerate(shards):
        shard_dir = Path(f'shard_{shard}') if num_shards > 1 else base
        if not shard_dir.is_dir():
            shard_dir.mkdir()
        episode_ids_file = (shard_dir / (name + '.episode_ids'))
        questions_file = (shard_dir / (name + '.questions'))
        answers_file = (shard_dir / (name + '.answers'))
        time_ref_file = (shard_dir / (name + '.time_refs'))
        episodes_file = (shard_dir / (name + '.episodes'))

        episode_ids_file.write_text('\n'.join(str(i) for i in local_split.episode_ids))
        questions_file.write_text('\n'.join(local_split.questions))
        answers_file.write_text('\n'.join(local_split.answers))
        time_ref_file.write_text('\n'.join(local_split.time_refs))
        episodes_file.write_text('\n'.join(local_split.episode_strings))


def _worker(queue: JoinableQueue, end_of_queue_token, sample_episodes, sample_dialogs) \
        -> Tuple[List, List, List, List, int, int]:
    local_episode_ids, local_questions, local_answers, local_time_refs = [], [], [], []
    local_errors, local_total = 0, 0
    start_time = time.time()
    working_time = 0
    while True:
        history, idx = queue.get(block=True)
        if history == end_of_queue_token:
            queue.task_done()
            break
        start_working_time = time.time()
        e, t = generate_dialog_for_episode(history, idx, local_answers, local_questions,
                                           local_episode_ids, local_time_refs, sub_sample=sample_episodes,
                                           few_dialogs_per_episode=sample_dialogs)
        local_errors += e
        local_total += t
        queue.task_done()
        working_time += time.time() - start_working_time
    total_time = time.time() - start_time
    print('Worked ', round(working_time), 'of', round(total_time),
          'seconds, i.e.', round(working_time / total_time, 2), '%')
    return local_episode_ids, local_questions, local_answers, local_time_refs, local_errors, local_total


def gen_episodes_and_dialogs(base_file, size_factor, max_history_length, sample_episodes, sample_dialogs,
                             episode_ids, questions, answers,
                             time_refs, episodes: List[EpisodeList], num_workers):
    additional_factor = 20 if sample_dialogs else 1
    history_generator = generate_histories_based_on(base_file, size_factor * additional_factor,
                                                    max_history_length, random.Random())
    return gen_episodes_and_dialogs_dyn(history_generator, size_factor,
                                        sample_episodes, sample_dialogs, episode_ids,
                                        questions, answers, time_refs, episodes, num_workers)


def gen_episodes_and_dialogs_dyn(history_generator: Iterable[EpisodeList],
                                 size_factor, sample_episodes, sample_dialogs,
                                 episode_ids, questions, answers,
                                 time_refs, episodes: List[EpisodeList], num_workers):
    end_of_queue_token = "END-OF-QUEUE"

    errors, total = 0, 0
    if num_workers == 0 or size_factor == 0:
        print('\tSingle-threaded mode')
        for h in history_generator:
            errors, total = generate_dialog_for_episode(h, len(episodes), answers, questions,
                                                        episode_ids, time_refs, sub_sample=sample_episodes,
                                                        few_dialogs_per_episode=sample_dialogs)
            episodes.append(h)
    else:
        print(f'\tUsing {num_workers} additional workers')
        with Pool(num_workers) as pool, Manager() as manager:
            queue = manager.Queue(maxsize=num_workers * 10)
            results = []
            for i in range(num_workers):
                results.append(pool.apply_async(_worker, (queue, end_of_queue_token, sample_episodes, sample_dialogs),
                                                error_callback=lambda e: print(e, file=sys.stderr)))
            # Main thread is responsible for putting items into the queue
            for h in history_generator:
                queue.put((h, len(episodes)), block=True)
                episodes.append(h)
            for i in range(num_workers):
                queue.put((end_of_queue_token, 0), block=True)
            queue.join()

            for r in results:
                l_episode_ids, l_questions, l_answers, l_time_refs, l_errors, l_total = r.get()
                episode_ids += l_episode_ids
                questions += l_questions
                answers += l_answers
                time_refs += l_time_refs
                errors += l_errors
                total += l_total

    if errors > 0:
        print(f'\tSkipped {errors} erroneous of {total} dialogs in total ({round(errors / total * 100)}%)')


def gen_dataset(base_file, size_factor, max_history_length, num_workers, sample_episodes=False, sample_dialogs=False):
    print(f'Generation with size factor {size_factor}, '
          f'{"not " if not sample_episodes else ""}sampling episodes, '
          f'{"not " if not sample_dialogs else ""}sampling dialogs')
    episode_ids, questions, answers, time_refs = [], [], [], []
    episodes: List[EpisodeList] = []
    params = (size_factor, max_history_length, sample_episodes, sample_dialogs, episode_ids,
              questions, answers, time_refs, episodes, num_workers)

    gen_episodes_and_dialogs(base_file, *params)

    episode_strings = ['[' + ', '.join(e.encode() for e in ep_list) + ']' for ep_list in episodes]
    return DatasetSplit(episode_ids, questions, answers, time_refs, episode_strings)


def main(write_to_base: Path, episode_templates_files: Tuple[Path, Path, Path],
         train_size=4, valid_size=2, test_size=1,
         train_max_history_length=4, valid_max_history_length=5, test_max_history_length=10,
         train_sample_dialogs=False, should_zip=False,
         num_workers=1, num_shards=1):
    train = gen_dataset(episode_templates_files[0], size_factor=train_size, max_history_length=train_max_history_length,
                        num_workers=num_workers, sample_dialogs=train_sample_dialogs)
    print('Train', len(train[0]))

    valid = gen_dataset(episode_templates_files[1], size_factor=valid_size, max_history_length=valid_max_history_length,
                        num_workers=num_workers, sample_dialogs=True)
    print('Valid', len(valid[0]))

    test = gen_dataset(episode_templates_files[2], size_factor=test_size, max_history_length=test_max_history_length,
                       num_workers=num_workers, sample_episodes=True)
    print('Test ', len(test[0]))

    if not train_sample_dialogs:
        train = train  # create_curriculum(train)
    else:
        print('No curriculum learning for this dataset')

    print('Writing to', write_to_base)
    write_dataset(write_to_base, 'simulated.train', train, num_shards=num_shards)
    write_dataset(write_to_base, 'simulated.valid', valid)
    write_dataset(write_to_base, 'simulated.test', test)

    if should_zip:
        print('Compressing...')
        subprocess.run(['zip', 'data.zip', 'simulated.*'], check=True)


def cli_main():
    parser = ArgumentParser()
    parser.add_argument('--episode-template-dir', metavar='DIR', type=lambda s: Path(s), required=True,
                        help='Directory with episode template files to use for generation (one file per dataset split)')
    parser.add_argument('--num-workers', default=max(1, os.cpu_count() - 1), type=int, metavar='N')
    parser.add_argument('--num-shards', default=1, type=int, metavar='N')
    parser.add_argument('--max-history-lengths', nargs=3, default=[4, 5, 10], type=int)
    parser.add_argument('--size-factors', nargs=3, default=[4, 2, 1], type=int)
    parser.add_argument('output', metavar='OUTPUT-DIR', nargs='?', type=lambda s: Path(s), default=Path(),
                        help='Directory where to output the dataset')
    args = parser.parse_args()

    assert args.episode_template_dir.is_dir()
    templates = tuple(args.episode_template_dir / (x + '.episodes') for x in ['train', 'valid', 'test'])
    assert all(t.is_file() for t in templates)

    # noinspection PyTypeChecker
    main(write_to_base=args.output, episode_templates_files=templates,
         train_size=args.size_factors[0], valid_size=args.size_factors[1], test_size=args.size_factors[2],
         train_max_history_length=args.max_history_lengths[0],
         valid_max_history_length=args.max_history_lengths[1],
         test_max_history_length=args.max_history_lengths[2],
         num_workers=args.num_workers, num_shards=args.num_shards)


if __name__ == '__main__':
    cli_main()
