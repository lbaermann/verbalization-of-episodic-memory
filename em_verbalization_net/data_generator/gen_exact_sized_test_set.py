import os
import random
from argparse import ArgumentParser
from pathlib import Path
from typing import List

from data_generator.episodes import EpisodeList
from data_generator.episodes.generator import generate_histories_exact_length
from data_generator.main import gen_episodes_and_dialogs_dyn, DatasetSplit, write_dataset


def gen_exact_sized_test_dataset(base_file, num_histories, history_length, num_workers, keep_dates):
    print(f'Generating samples for {num_histories} histories of length {history_length}')
    episode_ids, questions, answers, time_refs = [], [], [], []
    episodes: List[EpisodeList] = []

    histories = generate_histories_exact_length(base_file, num_histories, history_length, random.Random(),
                                                keep_dates=keep_dates)
    gen_episodes_and_dialogs_dyn(histories, size_factor=1, sample_episodes=True, sample_dialogs=False,
                                 episode_ids=episode_ids, questions=questions, answers=answers, time_refs=time_refs,
                                 episodes=episodes, num_workers=num_workers)

    episode_strings = ['[' + ', '.join(e.encode() for e in ep_list) + ']' for ep_list in episodes]
    return DatasetSplit(episode_ids, questions, answers, time_refs, episode_strings)


def main():
    parser = ArgumentParser()
    parser.add_argument('--episode-template-file', metavar='FILE', type=lambda s: Path(s), required=True,
                        help='File with episode templates to use for generation')
    parser.add_argument('--num-workers', default=max(1, os.cpu_count() - 1), type=int, metavar='N')
    parser.add_argument('--output-size', required=True, type=int, metavar='N',
                        help='Number of histories to generate')
    parser.add_argument('--history-length', required=True, type=int, metavar='N',
                        help='History length to generate')
    parser.add_argument('--keep-dates', action='store_true', default=False,
                        help='Do not choose random dates')
    parser.add_argument('output', metavar='OUTPUT-DIR', nargs='?', type=lambda s: Path(s), default=Path(),
                        help='Directory where to output the dataset')
    args = parser.parse_args()

    assert args.episode_template_file.is_file()
    assert args.output_size > 0
    assert args.history_length > 0
    assert args.num_workers >= 0

    test = gen_exact_sized_test_dataset(args.episode_template_file, args.output_size,
                                        args.history_length, args.num_workers, args.keep_dates)
    print('Test ', len(test[0]))
    print(f'Writing to {args.output}')
    write_dataset(args.output, 'simulated.test', test)


if __name__ == '__main__':
    main()
