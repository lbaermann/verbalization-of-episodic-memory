set -e

source ../../experiments/experiment_setup.sh

export NUM_WORKERS=$2
export PREPROCESS_DST_DIR=$1/data-bin

source ../../experiments/preprocess_emv_dataset.sh "$1/generated" "$GENERATED_DATA_PREFIX"
