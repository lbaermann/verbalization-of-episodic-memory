import os
import shutil
import subprocess
from argparse import ArgumentParser
from datetime import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer
from multiprocessing import Queue, Process, set_start_method
from pathlib import Path
from typing import List, Dict, Tuple, Any, Union

from data_generator.main import main as data_generator_main


class DataGeneratorServer:
    template_files: Tuple[Path, Path, Path]
    buffer_filler_workers: List[Process]
    queue: Queue
    experiment_epoch_to_dataset_cache: Dict[Tuple[int, int], Path]

    def __init__(self, args) -> None:
        super().__init__()
        self.sample_dialogs = args.sample_dialogs
        self.num_workers = args.num_workers
        # noinspection PyTypeChecker
        self.template_files = tuple(Path(args.episode_template_dir) / (x + '.episodes')
                                    for x in ['train', 'valid', 'test'])
        self.base_path = Path(args.base)
        assert not self.base_path.exists(), str(self.base_path)
        assert all(f.is_file() for f in self.template_files), str(self.template_files)
        self.host = ''
        self.port = args.port
        self.queue = Queue(maxsize=args.queue_size)
        self.buffer_filler_workers = []
        self.experiment_epoch_to_dataset_cache = {}

    def get_request_handler(self):
        outer_self = self

        class _RequestHandler(BaseHTTPRequestHandler):
            def do_GET(self):
                parts = self.path.split('/')
                if len(parts) == 3:
                    key = int(parts[1]), int(parts[2])
                    response_path: Path
                    # There are no race conditions for this if because
                    # the HTTP server is only single-threaded
                    if key in outer_self.experiment_epoch_to_dataset_cache:
                        response_path = outer_self.experiment_epoch_to_dataset_cache[key]
                    else:
                        response_path = outer_self.queue.get()
                        outer_self.experiment_epoch_to_dataset_cache[key] = response_path
                else:
                    response_path = outer_self.queue.get()
                    print('WARN: Compatibility mode (no path given), receives a new dataset!')
                self.send_response(200)
                self.send_header("Content-type", "text/html")
                self.end_headers()
                self.wfile.write(str(response_path.absolute()).encode())

        return _RequestHandler

    def _run_server(self):
        httpd = HTTPServer((self.host, self.port), self.get_request_handler())
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass
        httpd.server_close()

    def _start_generator(self):
        self.base_path.mkdir(exist_ok=False)
        self.buffer_filler_workers = [Process(target=self._generate_dataset_loop, daemon=False)
                                      for i in range(1)]
        for p in self.buffer_filler_workers:
            p.start()

    def start(self):
        try:
            self._start_generator()
            self._run_server()
        finally:
            self.queue.close()
            for p in self.buffer_filler_workers:
                p.terminate()
                p.join(timeout=1)

    def _generate_dataset_loop(self):
        while True:
            current_second = int(datetime.now().timestamp())
            base = self.base_path / f'dataset_{current_second}'
            base.mkdir()
            generator_base = base / 'generated'
            generator_base.mkdir()
            data_generator_main(generator_base, self.template_files, should_zip=False,
                                train_size=4, valid_size=0, test_size=0,
                                train_sample_dialogs=self.sample_dialogs,
                                num_workers=self.num_workers)
            subprocess.run(['bash', 'preprocess.sh', str(base.absolute()), str(self.num_workers)],
                           cwd=(Path(__file__).parent / 'script').absolute(),
                           check=True)
            shutil.rmtree(generator_base,
                          onerror=lambda *args: print('Failed to remove "generated" dir', *args))
            self.queue.put(base / 'data-bin')


def main():
    parser = ArgumentParser()
    parser.add_argument('--num-workers', default=max(0, os.cpu_count() - 2), type=int, metavar='N')
    parser.add_argument('--queue-size', type=int, default=10, metavar='N')
    parser.add_argument('--port', type=int, default=12345, metavar='P')
    parser.add_argument('--sample-dialogs', action='store_true', default=False,
                        help='Use sample_dialogs option for data generator.')
    parser.add_argument('--episode-template-dir', metavar='DIR', required=True,
                        help='Directory with episode template files to use for generation (one file per dataset split)')
    parser.add_argument('base', type=str, metavar='PATH',
                        help='base path where to generate datasets')
    args = parser.parse_args()

    print('Starting server, Press CTRL+C to stop...', flush=True)
    server = DataGeneratorServer(args)
    server.start()
    print('Stopped server, cleaning up generated datasets...')
    shutil.rmtree(args.base)


if __name__ == '__main__':
    set_start_method('spawn')  # Otherwise performance when generating is super low
    main()
