import json
import random
from calendar import monthrange
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

from click import progressbar

from data_generator.episodes import Episode, EpisodeList


def load_episodes_from_file(file: Path):
    text = file.read_text()
    result = []
    for line in text.splitlines():
        data = json.loads(line)
        episode_list = [Episode.parse(x) for x in data]
        result.append(episode_list)
    return result


def _random_base_date(r: random.Random, max_month=12):
    month = r.randint(1, max_month)
    year = 2020
    month_length = monthrange(year, month)[1]
    result = datetime(
        year=year, month=month, day=r.randint(1, month_length),
        hour=r.randint(6, 20), minute=r.randint(0, 59), second=r.randint(0, 59)
    )
    # Generating dates at 2020 01 01 may cause bugs when dialog generator refers to yesterday,
    #  which will be in 2019. This causes time_ref target to become out of bounds (-1)
    #  Simply avoiding these days is the easiest fix for now.
    if month == 1 and result.day < 3:
        result += timedelta(days=3)
    return result


def _random_date(base: datetime, r: random.Random,
                 max_d_days: int = 0,
                 max_d_hours: int = 0, allow_negative=False,
                 avoid_nights=True):
    max_delta = timedelta(days=max_d_days, hours=max_d_hours)
    if allow_negative:
        r = r.random() * 2 - 1
    else:
        r = r.random()
        # allow_negative = False => want it to be definitely after event _starting_ at base
        #                        => add some small amount of time to base (3 minutes)
        base += timedelta(minutes=3)
    result = base + r * max_delta
    if avoid_nights and (result.hour < 8 or result.hour > 20):
        result += timedelta(hours=12)
    return result


def _iterate_with_random_datetimes(episode_lists: List[EpisodeList], multiplier: int, r: random.Random, silent=False):
    if multiplier == 0:
        # Producing 0 samples may provoke errors due to empty datasets somewhere else.
        # Therefore, emit one sample.
        yield _sample_based_on(episode_lists[0], r)
        return

    def do_work(it):
        for history in it:
            yield _sample_based_on(history, r)

    generator = (h for i in range(multiplier) for h in episode_lists)
    if silent:
        yield from do_work(generator)
    else:
        with progressbar(generator, length=len(episode_lists) * multiplier) as iterator:
            yield from do_work(iterator)


def _sample_based_on(history, r):
    # Avoid moving parts of the history into 2021 (for dialog generator/eval).
    month_range = history[-1].last_date.month - history[0].first_date.month
    max_start_month = 12 - month_range

    new_history = []
    first_episode_start_date = _random_base_date(r, max_month=max_start_month)
    first: Episode = history[0].copy()
    first.move_start_to(first_episode_start_date)
    new_history.append(first)

    # move following episodes respecting distance with random variations
    for i, episode in enumerate(history[1:]):
        old_prev = history[i]
        new_prev = new_history[-1]
        simple_distance = episode.start - old_prev.end
        actual_distance = episode.first_date - old_prev.last_date
        potential_overlap = max(simple_distance - actual_distance, timedelta(0))
        copy = episode.copy()
        new_distance = min((0.75 + r.random() * 0.5) * simple_distance, (datetime(2021, 1, 1) - new_prev.end) * 0.9)
        copy.move_start_to(_random_date(new_prev.end + new_distance + potential_overlap, r, max_d_hours=1))
        new_history.append(copy)

    for e in new_history:
        e.question_time = _random_date(e.last_date, r, max_d_days=3)

    return new_history


def generate_episodes_based_on(base_file: Path, multiplier: int, r: random.Random, silent=False):
    episode_lists = load_episodes_from_file(base_file)
    yield from _iterate_with_random_datetimes(episode_lists, multiplier, r, silent)


def generate_histories_based_on(base_file: Path, multiplier: int, max_history_length: int,
                                r: random.Random, silent=False):
    episode_lists = load_episodes_from_file(base_file)
    assert all(len(h) == 1 for h in episode_lists), 'When generating histories, input should be single-episode lists'
    episodes: List[Episode] = [h[0] for h in episode_lists]
    histories: List[EpisodeList] = []
    for length in range(1, max_history_length + 1):
        sample_length = len(episodes) // max_history_length * length
        sample = r.sample(episodes, k=sample_length)
        slice_len = len(sample) // length
        sample_partitions = [sample[i * slice_len:(i + 1) * slice_len] for i in range(length)]
        while all(len(partition) > 0 for partition in sample_partitions):
            next_history = []
            for i in range(length):
                pop = sample_partitions[i].pop(0).copy()
                pop.move_start_to(_random_base_date(r))
                next_history.append(pop)
            next_history.sort(key=lambda e: e.last_date)
            if length > 1 and \
                    any(next_history[i + 1].first_date < next_history[i].last_date for i in range(length - 1)):
                # Just skip this bad sample with overlapping episodes
                continue
            histories.append(next_history)

    yield from _iterate_with_random_datetimes(histories, multiplier, r, silent)


def generate_histories_exact_length(base_file: Path, num_histories: int, history_length: int,
                                    r: random.Random, silent=False, keep_dates=False):
    episode_lists = load_episodes_from_file(base_file)
    assert all(len(h) == 1 for h in episode_lists), 'When generating histories, input should be single-episode lists'
    episodes: List[Episode] = [h[0] for h in episode_lists]
    episodes_copy = []
    histories: List[EpisodeList] = []

    while len(histories) < num_histories:
        if len(episodes_copy) < history_length:
            episodes_copy = list(episodes)
            r.shuffle(episodes_copy)

        next_history = []
        for i in range(history_length):
            pop = episodes_copy.pop(0).copy()
            if not keep_dates:
                pop.move_start_to(_random_base_date(r))
            next_history.append(pop)
        next_history.sort(key=lambda e: e.last_date)
        if history_length > 1 and \
                any(next_history[i + 1].first_date < next_history[i].last_date for i in range(history_length - 1)):
            # Just skip this bad sample with overlapping episodes
            continue
        histories.append(next_history)

    if keep_dates:
        for h in histories:
            for e in h:
                e.question_time = _random_date(e.last_date, r, max_d_days=3)
        yield from histories
    else:
        yield from _iterate_with_random_datetimes(histories, 1, r, silent)
