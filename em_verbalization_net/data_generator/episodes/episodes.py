import json
import re
from datetime import datetime
from typing import List, Tuple, Dict, Callable, Set

Vec3 = Tuple[float, float, float]
Vec6 = Tuple[float, float, float, float, float, float]
Vec7 = Tuple[float, float, float, float, float, float, float]


class ActionEntry:

    def __init__(self, name: str, status: str):
        assert status in {'started', 'success', 'failure'}
        self.name = name
        self.status = status

    def encode(self) -> dict:
        return self.__dict__

    @classmethod
    def parse(cls, data):
        return cls(name=data['name'], status=data['status'])

    def nl_action(self, past: bool) -> Set[str]:
        brought = 'brought' if past else 'bring'
        ed = 'ed' if past else ''
        d = 'd' if past else ''
        if 'BringObjectAndPutAway' in self.name:
            return {f'help{ed} with', f'hand{ed} over and {brought} back', f'assist{ed} with'}
        elif 'PutObjectAway' in self.name:
            return {'put away', f'{brought} back'}
        elif 'WhatCanYouSeeNow' in self.name:
            return set()  # This is handled separately by the grammar
        elif 'BringObject' in self.name:
            return {f'pass{ed} over', f'hand{ed} over', brought}
        elif 'move' in self.name:
            return {f'move{d} to'}
        elif 'grasp' in self.name:
            return {f'grasp{ed}'}
        elif 'putdown' in self.name:
            return {f'put down', f'release{d}'}

    @property
    def nl_object(self) -> Set[str]:
        if 'torch' in self.name:
            return {'torch', 'flashlight'}
        elif 'cloth' in self.name:
            return {'cloth', 'rag'}
        elif 'redcup' in self.name:
            return {'red cup'}
        elif 'greencup' in self.name:
            return {'green cup'}
        elif 'multivitaminjuice' in self.name:
            return {'multivitamin juice', 'vitamin juice'}
        elif 'move' in self.name:
            destination = self.name.split()[-1]
            if destination.startswith('roundtable'):
                return {'round table'}
            elif destination.startswith('sideboard'):
                return {'sideboard', 'big table'}
            elif destination.startswith('sink'):
                return {'sink'}
            elif destination.startswith('countertop'):
                return {'countertop'}
            elif destination.startswith('placesetting'):
                return {'setting place', 'small table'}
            elif destination.startswith('besidedishwasher'):
                return {'counter next to the dishwasher'}
            else:
                raise NotImplementedError(destination)
        return set()  # Cannot determine object from action, should use objects frames


class ObjectEntry:

    def __init__(self, name: str, frame: str, position: Vec3, newly_detected: bool):
        self.name = name
        self.frame = frame
        self.position = position
        self.newly_detected = newly_detected

    def encode(self) -> dict:
        return {
            'name': self.name,
            'position': self.position,
            'frame': self.frame,
            'type': 'newly detected' if self.newly_detected else 'updated'
        }

    @classmethod
    def parse(cls, data):
        return cls(
            name=data['name'],
            frame=data.get('frame', ''),
            position=tuple(data['position']),
            newly_detected=(data['type'] == 'newly detected')
        )


class PoseEntry:

    def __init__(self, confidence: float, local_coords: Vec3, global_coords: Vec3):
        self.confidence = confidence
        self.local_coords = local_coords
        self.global_coords = global_coords

    def encode(self) -> dict:
        return {
            'confidence': self.confidence,
            'local': self.local_coords,
            'global': self.global_coords
        }

    @classmethod
    def parse(cls, data):
        return cls(
            confidence=data['confidence'],
            local_coords=tuple(data.get('local', [])),
            global_coords=tuple(data.get('global', []))
        )


class EpisodeKeyframe:
    actions: List[ActionEntry]
    objects: List[ObjectEntry]
    speech: List[str]
    pose: Dict[str, PoseEntry]
    joints: Dict[str, Vec7]
    platform: Vec6
    img_id: int

    def __init__(self, actions=None, objects=None, speech=None, pose=None, joints=None, platform=None, img_id=None):
        if platform is None:
            platform = []
        if joints is None:
            joints = []
        if pose is None:
            pose = []
        if speech is None:
            speech = []
        if objects is None:
            objects = []
        if actions is None:
            actions = []
        self.platform = platform
        self.joints = joints
        self.pose = pose
        self.speech = speech
        self.objects = objects
        self.actions = actions
        self.img_id = img_id

    def __len__(self) -> int:
        return len(self.actions) + len(self.objects) + len(self.speech) \
               + (0 if len(self.pose) == 0 else 1) \
               + (0 if len(self.joints) == 0 else 1) \
               + (0 if len(self.platform) == 0 else 1)

    def encode(self) -> dict:
        result = {}
        if len(self.actions) > 0:
            result['actions'] = [x.encode() for x in self.actions]
        if len(self.objects) > 0:
            result['objects'] = [x.encode() for x in self.objects]
        if len(self.speech) > 0:
            result['speech'] = self.speech
        if len(self.pose) > 0:
            result['poses'] = {k: v.encode() for k, v in self.pose.items()}
        if len(self.joints) > 0:
            result['joints'] = self.joints
        if len(self.platform) > 0:
            result['platform'] = self.platform
        if self.img_id is not None:
            result['img_id'] = self.img_id
        return result

    @classmethod
    def parse(cls, data: Dict):
        return cls(
            actions=[ActionEntry.parse(entry) for entry in data.get('actions', [])],
            objects=[ObjectEntry.parse(entry) for entry in data.get('objects', [])],
            speech=data.get('speech', []),
            pose={name: PoseEntry.parse(value) for name, value in data.get('poses', {}).items()},
            joints={key: tuple(value) for key, value in data.get('joints', {}).items()},
            platform=tuple(data.get('platform', [])),
            img_id=data.get('img_id')
        )

    def nl_action(self, past: bool) -> Set[str]:
        return set(x for action in self.actions for x in action.nl_action(past))

    @property
    def nl_object(self) -> Set[str]:
        action_args = set(x for action in self.actions for x in action.nl_object)
        if len(action_args) == 0:
            for obj in self.objects:
                if 'hand' not in obj.name:
                    action_args.add(obj.name)
        return action_args


class Episode:
    question_time: datetime

    start: datetime
    end: datetime
    status: str
    goal: str
    keyframes: Dict[datetime, EpisodeKeyframe]

    def __init__(self, start: datetime, end: datetime, status: str,
                 goal: str, keyframes: Dict[datetime, EpisodeKeyframe]):
        super().__init__()
        assert start < end
        assert status in {'success', 'failure', 'abort'}
        self.start = start
        self.end = end
        self.status = status
        self.goal = goal
        self.keyframes = keyframes

    @property
    def first_date(self):
        return min({self.start} | self.keyframes.keys())

    @property
    def last_date(self):
        return max({self.end} | self.keyframes.keys())

    @property
    def action_keys(self) -> List[str]:
        return [a.name.split()[0]
                for k in self.keyframes.values()
                for a in k.actions
                if a.status == 'started']

    def parse_goal_and_args(self) -> Tuple[int, List[str]]:
        if self.goal == 'NO_EPISODE':
            return 0, []

        # r'\(existsK\(\?\w+\s*:\s*loc_(.+),\s*\?\w+\s*:\s*obj_(.+)\)\s*K\(objectAt\(\?\w+,\s*\?\w+\)\)\)'

        def match_arg_lists(cmd1, cmd2, list1, list2):
            if len(list1) == 1:
                return 1, [cmd1] + list1 + list2
            else:
                assert len(list2) == 1 or len(list1) == len(list2), f'{cmd2}, {list1}, {list2}'
                if len(list2) == 1:
                    list2 = list2 * len(list1)
                return len(list1), [cmd2] + list1 + list2

        objects = re.findall(r'obj_([a-z_A-Z]+)', self.goal)
        objects = [o for o in objects if all(x not in o for x in ['hand', 'agent'])]
        if 'objectAt' in self.goal:
            locations = re.findall(r'loc_([a-z_A-Z]+)', self.goal)
            return match_arg_lists('move', 'move2', objects, locations)
        elif 'grasp' in self.goal:
            hands = re.findall(r'obj_(right|left)hand', self.goal)
            return match_arg_lists('grasp', 'grasp2', objects, hands)
        else:
            print('warning: cannot parse', self.goal)
            return 0, []

    def nl_actions(self, past: bool) -> List[Set[str]]:
        return [a.nl_action(past)
                for k in self.keyframes.values()
                for a in k.actions
                if a.status == 'started']

    @property
    def nl_objects(self) -> List[Set[str]]:
        """ Objects in this episode, aligned to nl_actions array """
        nl_objects = []
        searching_next_object = False
        for k in self.keyframes.values():
            if any(a.status == 'started' for a in k.actions):
                if searching_next_object:
                    # Found no object for last action, add empty set
                    nl_objects.append(set())
                searching_next_object = True
            if searching_next_object and len(k.nl_object) > 0:
                searching_next_object = False
                nl_objects.append(k.nl_object)
        if searching_next_object:
            # End of loop and no obj for last action found
            nl_objects.append(set())
        return nl_objects

    @property
    def action_success_states(self) -> List[bool]:
        """ Success states of the actions, aligned with nl_actions """
        success_states = {}
        started_actions: Dict[str, int] = {}
        action_counter = 0
        for k in self.keyframes.values():
            for a in k.actions:
                if a.status == 'started':
                    if a.name in started_actions:
                        number = started_actions[a.name]
                        success_states[number] = False
                    started_actions[a.name] = action_counter
                    action_counter += 1
                else:
                    if a.name in started_actions:
                        number = started_actions[a.name]
                        success = a.status == 'success'
                        if success:
                            success_states[number] = True
                            del started_actions[a.name]
                        else:
                            success_states[number] = False
                            # Keep the entry in started_actions in case the action was retried and finally succeeded
                    else:
                        # print('Warning: Reached end of never started action!', a.encode(), self.encode())
                        pass

        # Assume incomplete actions were not successful
        for number in started_actions.values():
            success_states[number] = False

        assert len(success_states) == len(self.nl_actions(False)), f"{success_states}, {self.nl_actions(False)}," \
                                                                   f" {self.encode()}"
        return [state for i, state in sorted(success_states.items())]

    def encode(self) -> str:
        return json.dumps({
            'started': self.start.strftime('%Y %m %d %w %H %M %S'),
            'ended': self.end.strftime('%Y %m %d %w %H %M %S'),
            'status': self.status,
            'goal': self.goal,
            'keyframes': {
                t.strftime('%Y %m %d %w %H %M %S'): e.encode() for t, e in self.keyframes.items()
                if len(e) > 0
            }
        })

    @classmethod
    def parse(cls, data: Dict):
        return cls(
            start=datetime.strptime(data['started'], '%Y %m %d %w %H %M %S'),
            end=datetime.strptime(data['ended'], '%Y %m %d %w %H %M %S'),
            status=data['status'],
            goal=data['goal'],
            keyframes={
                datetime.strptime(dt, '%Y %m %d %w %H %M %S'): EpisodeKeyframe.parse(frame)
                for dt, frame in data['keyframes'].items()
            }
        )

    def copy(self):
        return Episode.parse(json.loads(self.encode()))

    def move_start_to(self, new_start_date: datetime):
        old_start_date = self.start
        self.start = new_start_date
        self.end = self.end - old_start_date + new_start_date
        self.keyframes = {
            k - old_start_date + new_start_date: v for k, v in self.keyframes.items()
        }

    def __repr__(self) -> str:
        return f'Episode({self.status:7} @ {self.start} - {self.end})'


EpisodeList = List[Episode]
