from abc import ABC
from typing import Callable, Set, Union, Tuple


class Grammar:

    def __or__(self, other):
        return Or(self, convert_to_grammar(other))

    def __ror__(self, other):
        return Or(convert_to_grammar(other), self)

    def __add__(self, other):
        return Concat(self, convert_to_grammar(other))

    def __radd__(self, other):
        return convert_to_grammar(other) + self

    def generate_all(self):
        raise NotImplementedError

    def draw_sample(self, random):
        raise NotImplementedError

    def list_subtypes(self) -> set:
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError


def convert_to_grammar(obj):
    if isinstance(obj, str):
        obj = Terminal(obj)
    return obj


def optional(expr):
    expr = convert_to_grammar(expr)
    return expr | ""


class Terminal(Grammar):

    def __init__(self, content: str):
        super().__init__()
        assert isinstance(content, str)
        self.content = content

    def __str__(self):
        return self.content

    def generate_all(self):
        yield self.content

    def draw_sample(self, random):
        return self.content

    def list_subtypes(self) -> set:
        return {Terminal}

    def __len__(self):
        return 1


class Concat(Grammar):

    def __init__(self, first: Grammar, second: Grammar):
        super().__init__()
        assert isinstance(first, Grammar)
        assert isinstance(second, Grammar)
        self.first = first
        self.second = second

    def __str__(self):
        return str(self.first) + str(self.second)

    def generate_all(self):
        for a in self.first.generate_all():
            for b in self.second.generate_all():
                yield a + b

    def draw_sample(self, random):
        a = self.first.draw_sample(random)
        b = self.second.draw_sample(random)
        return a + b

    def list_subtypes(self) -> set:
        return self.first.list_subtypes().union(self.second.list_subtypes()).union({Concat})

    def __len__(self):
        return len(self.first) * len(self.second)


class Or(Grammar):

    def __init__(self, first: Grammar, second: Grammar):
        super().__init__()
        assert isinstance(first, Grammar)
        assert isinstance(second, Grammar)
        self.first = first
        self.second = second

    def __str__(self):
        return "(" + str(self.first) + "|" + str(self.second) + ")"

    def generate_all(self):
        yield from self.first.generate_all()
        yield from self.second.generate_all()

    def draw_sample(self, random):
        # Need to normalize a large Or of Ors so that each option has equal probability!
        options = self._get_non_or_children()
        choice = random.choices(options, weights=[len(o) for o in options], k=1)[0]
        return choice.draw_sample(random)

    def _get_non_or_children(self):
        non_or_children = []
        if isinstance(self.first, Or):
            non_or_children += self.first._get_non_or_children()
        else:
            non_or_children.append(self.first)
        if isinstance(self.second, Or):
            non_or_children += self.second._get_non_or_children()
        else:
            non_or_children.append(self.second)
        return non_or_children

    def list_subtypes(self) -> set:
        return self.first.list_subtypes().union(self.second.list_subtypes()).union({Or})

    def __len__(self):
        return len(self.first) + len(self.second)


class Variable(Grammar, ABC):

    def list_subtypes(self) -> set:
        raise NotImplementedError

    def __init__(self, name: str) -> None:
        super().__init__()
        self.name = name
        self.actual_options = []
        self.index = -1
        self.chosen_option = None

    def generate_all(self):
        yield self.chosen_option

    def choose_next_option(self) -> bool:
        self.index += 1
        if self.index >= len(self.actual_options):
            return False
        self.chosen_option = self.actual_options[self.index]
        return True

    def reset(self):
        self.index = -1
        self.chosen_option = None

    def __len__(self):
        return 1


class IndependentVariable(Variable):

    def __init__(self, name: str, options: Grammar):
        super().__init__(name)
        assert Variable not in options.list_subtypes()
        self.options = options
        self.actual_options = list(options.generate_all())

    def __str__(self):
        return "[Var " + self.name + "]"

    def list_subtypes(self) -> set:
        return self.options.list_subtypes().union({Variable})

    def draw_sample(self, random):
        if self.chosen_option is None:
            self.chosen_option = random.choice(self.actual_options)
        return self.chosen_option


class BoundVariable(Variable):

    def __init__(self, name: str, ref: Union[Variable, Set[Variable]],
                 generator: Callable[[Union[Variable, Tuple[Variable, ...]]], Set[str]]) -> None:
        super().__init__(name)
        self.ref = ref if isinstance(ref, set) else {ref}
        self.generator = generator

    def reset(self):
        super().reset()
        self.actual_options = list(self.generator(*self.ref))

    def list_subtypes(self) -> set:
        return {BoundVariable}

    def __str__(self) -> str:
        return "[" + self.name + " bound to " + str(set(map(lambda x: x.name, self.ref))) + "]"

    def draw_sample(self, random):
        raise ValueError('Grammars involving bound variables cannot draw sample!')
