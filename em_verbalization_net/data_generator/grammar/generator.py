from typing import List

from .grammar import *


def find_vars(grammar: Grammar) -> set:
    if isinstance(grammar, Or) or isinstance(grammar, Concat):
        return find_vars(grammar.first).union(find_vars(grammar.second))
    elif isinstance(grammar, Variable):
        return {grammar}
    else:
        return set()


def topological_sorting(variables: Set[Variable]) -> List[Variable]:
    # Collect all transitive dependencies
    variables = transitive_closure(variables)
    bound: Set[BoundVariable] = set(filter(lambda v: isinstance(v, BoundVariable), variables))
    independent = list(variables.difference(bound))
    len_before = len(bound)
    while len(bound) > 0:
        for b in bound:
            if len(b.ref - set(independent)) == 0:
                independent.append(b)
                bound.remove(b)
                break
        if len_before == len(bound):
            raise RecursionError("Loop in bound variables " + str(set(map(str, bound))))
        len_before = len(bound)
    return independent


def transitive_closure(vars: Set[Variable]):
    dependencies = set(vars)
    for var in vars:
        if isinstance(var, BoundVariable):
            dependencies |= transitive_closure(var.ref)
    return dependencies


def generate_all_words(grammar: Grammar) -> Set[str]:
    def _split_ors(g: Grammar):
        if isinstance(g, Or):
            yield from _split_ors(g.first)
            yield from _split_ors(g.second)
        else:
            yield from _gen_words(g, topological_sorting(find_vars(g)))

    def _gen_words(g: Grammar, vs: List[Variable]):
        if len(vs) == 0:
            yield from g.generate_all()
        else:
            v: Variable = vs[0]
            v.reset()
            while v.choose_next_option():
                yield from _gen_words(g, vs[1:])

    all_vars = find_vars(grammar)
    if len(all_vars) == 0:
        yield from grammar.generate_all()
    else:
        yield from _split_ors(grammar)


def calc_grammar_size(grammar: Grammar) -> int:
    def _split_ors(g: Grammar) -> int:
        if isinstance(g, Or):
            return _split_ors(g.first) + _split_ors(g.second)
        else:
            return _calc_size(g, topological_sorting(find_vars(g)))

    def _calc_size(g: Grammar, vs: List[Variable]) -> int:
        if len(vs) == 0:
            return len(g)
        else:
            v: Variable = vs[0]
            v.reset()
            total = 0
            while v.choose_next_option():
                total += _calc_size(g, vs[1:])
            return total

    all_vars = find_vars(grammar)
    if len(all_vars) == 0:
        return len(grammar)
    else:
        return _split_ors(grammar)
