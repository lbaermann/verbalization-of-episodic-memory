import random
from datetime import datetime, timedelta, time
from functools import reduce
from typing import List

from .episodes import Episode, EpisodeList
from .episodes.episodes import ActionEntry
from .grammar import generator, calc_grammar_size
from .grammar.grammar import *

FAKE = "FAKE"

lf = "\n"
T = Terminal
V = IndependentVariable
B = BoundVariable
o = optional

int_to_string_dict = {
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six',
    7: 'seven',
    8: 'eight',
    9: 'nine',
    10: 'ten',
    11: 'eleven',
    12: 'twelve',
}

please = o("please ")
_please = o(" please")
tell_me = T('tell me ') | 'talk to me about ' | (T('explain ') + o('to me '))
your_task = T('your ') + (T('business') | 'task' | 'exercise')
your_tasks = T('your ') + (T('businesses') | 'tasks' | 'exercises')


def int_to_string(i: int):
    if i in int_to_string_dict:
        return int_to_string_dict[i]
    else:
        return str(i)


def hour_to_daytime(hour: int):
    if hour < 10:
        return 'in the morning'
    elif hour < 12:
        return 'before noon'
    elif hour < 14:
        return 'at noon'
    elif hour < 18:
        return 'in the afternoon'
    else:
        return 'in the evening'


def select_random(options: Grammar, k=8):
    options = list(generator.generate_all_words(options))
    if len(options) < k:
        return any_of(*options)
    else:
        return any_of(*random.choices(options, k=min(len(options), k)))


def any_of(*items: Union[str, Grammar]) -> Grammar:
    if len(items) == 0:
        return T("")
    if isinstance(items[0], str):
        g = T(items[0])
    elif isinstance(items[0], Grammar):
        g = items[0]
    else:
        assert False, 'items[0] neither str nor Grammar: ' + str(items[0])
    for s in items[1:]:
        g |= s
    return g


def get_what_did_you_do_question(time, simple):
    basic = T('what did you do') + time
    if simple:
        return basic
    return basic \
           | ((tell_me | 'what was ') + your_task + time) \
           | ((tell_me | 'what were ') + your_tasks + time) \
           | (please + tell_me + 'what you did' + time) \
           | (tell_me + 'what you did' + time + _please)


def _make_move_objects_to_places_phrase(arg_count, goal_and_args):
    # layout of goal_an_args : move2 object1 object2 location1 location2
    answer = ''
    if len(set(goal_and_args[1 + arg_count:])) == 1:
        # All locations are equal
        for i in range(arg_count):
            answer += f'the {goal_and_args[1 + i]}'
            if i == arg_count - 2:
                answer += ' and '
            elif i < arg_count - 2:
                answer += ', '
        answer += f' to the {goal_and_args[1 + arg_count]}'
    else:
        # Different locations
        for i in range(arg_count):
            answer += f'the {goal_and_args[1 + i]} to the {goal_and_args[1 + arg_count + i]}'
            if i == arg_count - 2:
                answer += ' and '
            elif i < arg_count - 2:
                answer += ', '
    return answer


def get_what_did_you_do_answer(e: Episode):
    arg_count, goal_and_args = e.parse_goal_and_args()
    # Armar 3
    if len(goal_and_args) > 0:
        failure = e.status != 'success'
        goal = goal_and_args[0]
        grasp_verb = 'i tried to but failed to grab the ' if failure else 'i grabbed the '
        move_verb = 'i tried to but failed to move ' if failure else 'i moved '
        if goal == 'grasp':
            return grasp_verb + goal_and_args[1]
        elif goal == 'grasp2':
            return f'{grasp_verb}{goal_and_args[1]} and the {goal_and_args[2]}'
        elif goal == 'move':
            return f'{move_verb}the {goal_and_args[1]} to the {goal_and_args[2]}'
        elif goal == 'move2':
            return move_verb + _make_move_objects_to_places_phrase(arg_count, goal_and_args)

    # Armar 6
    def answer(action_name: str, i=0):
        if action_name == 'WhatCanYouSeeNow':
            objects = [T('the ') + obj for objs in e.nl_objects for obj in objs]
            if len(objects) == 1:
                objs_term = objects[0]
            elif len(objects) == 2:
                objs_term = objects[0] + ' and ' + objects[1]
            else:
                objs_term = reduce(lambda a, b: a + b, (x + ', ' for x in objects[:-2])) \
                            + objects[-2] + ' and ' + objects[-1]
            return T('i looked around and saw ') + objs_term
        else:
            action = any_of(*e.nl_actions(past=True)[i])
            obj = any_of(*e.nl_objects[i])
            return T('i ') + action + ' the ' + obj

    if len(e.action_keys) == 1:
        return answer(e.action_keys[0])
    elif len(e.action_keys) == 2:
        return T('first, ') + answer(e.action_keys[0]) + ', then, ' + answer(e.action_keys[1], i=1)
    else:
        return 'ERROR'


def get_time_ref(ref: datetime, now: datetime, prefer_exact=False):
    abs_day = (T(ref.strftime('on %b %d'))
               | ref.strftime('on %B %d'))
    diff_to_full_hour = -ref.minute if ref.minute < 30 else 60 - ref.minute
    ref_rounded = ref + timedelta(minutes=diff_to_full_hour)
    abs_hour = (T(ref.strftime('%H:%M'))
                # | ref.strftime('%I:%M %p')
                )
    if prefer_exact:
        abs_hour |= T(ref.strftime('%H:%M:%S'))
    else:
        abs_hour |= o('about ') + (T(ref_rounded.strftime('%H o clock'))
                                   | ref_rounded.strftime('%I %p'))
        if ref_rounded.hour < 10:
            # Without trailing 0
            abs_hour |= o('about ') + str(ref_rounded.hour) + ' o clock'
    absolute = abs_day + ' at ' + abs_hour

    delta = now - ref
    delta_minutes = round(delta.seconds / 60)
    delta_hours = round(delta_minutes / 60)
    timespec = (T('at ') + abs_hour)
    if not prefer_exact:
        timespec |= hour_to_daytime(ref.hour)
    if delta.days == 0 and delta_hours <= now.hour:
        rel = o('today ') + timespec
        if delta_hours > 1:
            # rel |= o('about ') + int_to_string(delta_hours) + ' hours ago'
            if not prefer_exact:
                rel |= 'in the last ' + int_to_string(delta_minutes // 60 + 1) + ' hours'
        elif delta_hours == 1:
            # rel |= o('about ') + 'one hour ago'
            if not prefer_exact:
                rel |= 'in the last hour'
        else:
            rel |= int_to_string(delta_minutes) + ' minutes ago'
            if not prefer_exact:
                rel |= 'about ' + int_to_string(round(delta_minutes / 5) * 5) + ' minutes ago'
            # rel |= (T('in') | 'during') + ' the last hour'
    elif (delta.days == 1 and delta_hours <= now.hour) or (delta.days == 0 and delta_hours > now.hour):
        rel = T('yesterday ') + timespec
    else:
        rel = T(int_to_string(delta.days + (delta_hours > now.hour))) + ' days ago ' + timespec

    return rel | absolute


def get_whats_the_time_dialog(question_time):
    time_ref = question_time.strftime('%Y %m %d %w %H %M %S')
    time_question = T('what is the time')
    time_answer = question_time.strftime('It is %H:%M')
    date_question = 'what day is today'
    date_answer = question_time.strftime('It is %B %d')
    return T(time_ref) + lf + ((time_question + lf + time_answer)
                               | (date_question + lf + date_answer))


def get_what_did_you_do_dialog(episode, question_time, simple, only=False):
    if only:
        time = T('')
    else:
        time = T(' ') + get_time_ref(ref=episode.start, now=question_time)
    question = get_what_did_you_do_question(time, simple)
    answer = get_what_did_you_do_answer(episode)
    time_ref = episode.start.strftime('%Y %m %d %w %H %M %S')
    question_answer = time_ref + lf + question + lf + answer
    return question_answer


def get_when_dialogs(episode, question_time, simple):
    when_did_you = T('when did you ')
    # if not simple:
    # this would need tense adjustment to all the sentences below
    #     when_did_you |= tell_me + (T('when') | 'at what time') + ' you '

    questions_and_answers = []

    def add_possible_when_question(correct_response: datetime, question: Grammar, answer_start, prefer_exact=False):
        questions_and_answers.append(
            correct_response.strftime('%Y %m %d %w %H %M %S') + lf +
            question + lf +
            answer_start + select_random(get_time_ref(correct_response, question_time, prefer_exact))
        )

    arg_count, goal_and_args = episode.parse_goal_and_args()
    if len(goal_and_args) > 0:
        question = when_did_you
        question += _make_action_phrase_based_on_goal_and_args(episode)
        add_possible_when_question(episode.start, question, 'i did it ')

    for i, action_key in enumerate(episode.action_keys):
        action, timestamp = [(a, t) for t, k in episode.keyframes.items()
                             for a in k.actions if a.status == 'started'][i]
        if action_key == 'WhatCanYouSeeNow':
            add_possible_when_question(timestamp, when_did_you + 'get the question to tell what you can see',
                                       'this was ')
        else:
            nl_action = episode.nl_actions(past=False)[i]
            nl_object = episode.nl_objects[i]
            add_possible_when_question(timestamp, when_did_you + any_of(*nl_action) + ' the ' + any_of(*nl_object),
                                       'i did it ')

    see = T('see ') | 'detect ' | 'spot '
    i_saw_it = T('it was ') | 'i saw it '

    when_question_object_blacklist = [
        'uppersideboard', 'uppersink', 'wallsink', 'controltable', 'windows', 'dishwasher',
        'fridge', 'lowersideboard', 'sideboard', 'roundtable', 'lowersink', 'fridge', 'oven',
    ]
    object_times = [(o.name, t) for t, k in episode.keyframes.items() for o in k.objects
                    if 'hand' not in o.name and o.name not in when_question_object_blacklist]
    seen_objects = set(o for o, t in object_times)
    for obj in seen_objects:
        first_time = [t for o, t in object_times if o == obj][0]
        last_time = [t for o, t in object_times if o == obj][-1]
        add_possible_when_question(first_time, when_did_you + (T('first ') | 'initially ') + see + 'the ' + obj,
                                   i_saw_it, prefer_exact=True)
        add_possible_when_question(last_time, when_did_you + (T('last ')) + see + 'the ' + obj, i_saw_it, True)

    person_times = sorted([t for t, k in episode.keyframes.items() if any(p.confidence > 0.5 for p in k.pose.values())])
    if len(person_times) > 0:
        add_possible_when_question(person_times[0], when_did_you + (T('first ')) + see + 'a person', 'this was ')
        add_possible_when_question(person_times[-1], when_did_you + (T('last ')) + see + 'a person', 'this was ')

    return any_of(*questions_and_answers)


def _make_action_phrase_based_on_goal_and_args(episode):
    arg_count, goal_and_args = episode.parse_goal_and_args()
    assert len(goal_and_args) > 0, episode.encode()
    goal = goal_and_args[0]
    if goal == 'grasp':
        return 'grasp the ' + goal_and_args[1]
    elif goal == 'grasp2':
        return 'grasp the ' + goal_and_args[1] + ' and the ' + goal_and_args[2]
    elif goal == 'move' or goal == 'move2':
        return 'move ' + _make_move_objects_to_places_phrase(arg_count, goal_and_args)
    else:
        raise NotImplementedError(goal)


def get_action_stats_dialog(episode: Episode, question_time: datetime):
    time_ref = episode.start.strftime('%Y %m %d %w %H %M %S')
    dialogs = []
    for i, nl_action in enumerate(episode.nl_actions(past=False)):
        ref = get_time_ref(episode.start, question_time)
        task_desc = any_of(*nl_action) + ' the ' + any_of(*episode.nl_objects[i])
        question = (tell_me + 'how your task to ' + task_desc + ' worked ' + ref
                    ) | ('did you have any problems to ' + task_desc + ' ' + ref)
        success = episode.action_success_states[i]
        answer = any_of('everything went fine', 'i had no problem to ' + task_desc) \
            if success else \
            'i could not successfully finish my task to ' + task_desc
        question_answer = time_ref + lf + question + lf + answer
        dialogs.append(select_random(question_answer, k=calc_grammar_size(question_answer) // 10))

    intervals = [
        (question_time.date(), timedelta(days=1), 'today'),
        (question_time.date() - timedelta(days=1), timedelta(days=1), 'yesterday'),
        (question_time.date(), timedelta(days=3), 'in the last few days'),
    ]
    failed_actions = [(t, a) for t, k in episode.keyframes.items() for a in k.actions if a.status == 'failure']
    for end_date, date_range, name in intervals:
        range_end = datetime.combine(end_date, time(hour=23, minute=59, second=59, microsecond=999))
        range_start = range_end - date_range
        failed_in_range = [(t, a) for t, a in failed_actions if range_start < t < range_end]
        time_ref = datetime.combine(end_date, time(hour=12)).strftime('%Y %m %d %w %H %M %S')
        question = 'did you have any problem ' + name
        if len(failed_in_range) > 0:
            fail_time, failed_action = failed_in_range[-1]
            nl_object = failed_action.nl_object
            if len(nl_object) == 0:
                nl_object = [k.nl_object for t, k in episode.keyframes.items()
                             if len(k.nl_object) > 0 and t < fail_time][-1]
            task_desc = any_of(*failed_action.nl_action(past=False)) + ' the ' + any_of(*nl_object)
            answer = 'yes, i failed to ' + task_desc + ' ' + select_random(
                get_time_ref(fail_time, question_time, prefer_exact=True), k=1)
        elif episode.status != 'success' \
                and range_start < episode.start < range_end \
                and episode.parse_goal_and_args()[0] > 0:
            answer = 'yes, i failed to ' + _make_action_phrase_based_on_goal_and_args(episode) + ' ' + select_random(
                get_time_ref(episode.end, question_time, prefer_exact=True), k=1)
        else:
            answer = 'no'
        question_answer = time_ref + lf + question + lf + answer
        dialogs.append(question_answer)

    return any_of(*dialogs)


def get_which_object_did_you_do_move_dialog(episode: Episode, question_time):
    time_ref = episode.start.strftime('%Y %m %d %w %H %M %S')
    result = []
    # Armar3 data
    arg_count, goal_and_args = episode.parse_goal_and_args()
    if len(goal_and_args) > 0 and goal_and_args[0] == 'move':
        destination_to_objects = {}
        for i in range(arg_count):
            obj = goal_and_args[1 + i]
            location = goal_and_args[1 + arg_count + i]
            destination_to_objects.setdefault(location, [])
            destination_to_objects[location].append(obj)
        for dest, objects in destination_to_objects.items():
            question = tell_me + f'which object you moved to the {dest} ' + get_time_ref(episode.start, question_time)
            answer = any_of('i moved ', 'i think you are talking about ')
            for i in range(len(objects)):
                answer += f'the {objects[i]}'
                if i == arg_count - 2:
                    answer += ' and '
                elif i < arg_count - 2:
                    answer += ', '
            question_answer = time_ref + lf + question + lf + answer
            result.append(question_answer)

    # Armar6 data:
    for i, action in enumerate(episode.action_keys):
        if action in ['BringObject', 'PutObjectAway', 'BringObjectAndPutAway']:
            nl_object = any_of(*episode.nl_objects[i])
            nl_action = any_of(*episode.nl_actions(past=True)[i])
            question = tell_me + 'which object you ' + nl_action + ' ' + get_time_ref(episode.start, question_time)
            answer = T('i think you are talking about the ') + nl_object
            question_answer = time_ref + lf + question + lf + answer
            result.append(question_answer)
    if len(result) == 0:
        action = any_of('put away', 'moved', 'brought')
        question = tell_me + 'which object you ' + action + ' ' + get_time_ref(episode.start, question_time,
                                                                               prefer_exact=True)
        answer = 'i did not move any object at that time'
        question_answer = time_ref + lf + question + lf + answer
        result.append(question_answer)
    return any_of(*result)


def get_how_long_did_it_take_dialog(episode: Episode, question_time):
    how_long_did_it_take_to = (tell_me + 'how long it took to ') | 'how long did it take to '
    action_starts: List[Tuple[datetime, ActionEntry, int]] = [
        (t, a, i) for i, (t, a)
        in enumerate([(t, a) for t, k in episode.keyframes.items() for a in k.actions
                      if a.status == 'started'])
        if episode.action_success_states[i]
    ]
    action_ends = [(t, a) for t, k in episode.keyframes.items() for a in k.actions if a.status == 'success']

    if len(action_starts) != len(action_ends):
        # print(f"starts: {len(action_starts)}, ends: {len(action_ends)}, episode: {episode.encode()}")
        trim = min(len(action_starts), len(action_ends))
        action_starts = action_starts[:trim]
        action_ends = action_ends[:trim]

    result = []
    for i in range(len(action_starts)):
        duration = action_ends[i][0] - action_starts[i][0]
        nl_action = any_of(*action_starts[i][1].nl_action(past=False))
        nl_object = any_of(*episode.nl_objects[action_starts[i][2]])

        time_ref = action_starts[i][0].strftime('%Y %m %d %w %H %M %S')
        question = how_long_did_it_take_to + nl_action + ' the ' + nl_object \
                   + ' ' + get_time_ref(action_starts[i][0], question_time)
        exact_question = 'how long exactly did it take to ' + nl_action + ' the ' + nl_object \
                         + ' ' + get_time_ref(action_starts[i][0], question_time)
        answer = 'it took ' + str(round(duration.total_seconds())) + ' seconds'
        result.append(time_ref + lf + exact_question + lf + answer)

        duration_minutes = round(duration.total_seconds() / 60)
        if duration_minutes > 0:
            broad_answer = 'it took about ' + (int_to_string(duration_minutes) + ' minutes'
                                               if duration_minutes > 1 else 'one minute')
            result.append(time_ref + lf + question + lf + broad_answer)

    if len(result) == 0:
        time_ref = episode.start.strftime('%Y %m %d %w %H %M %S')
        possible_action = IndependentVariable('action', any_of('move to', 'grasp', 'grab'))
        possible_object = IndependentVariable('object', any_of('vitamin juice', 'red cup', 'cloth', 'green cup'))
        possible_time = IndependentVariable('time', any_of('today', 'yesterday', 'yesterday in the afternoon'))
        question = T('how long did it take to ') + possible_action + ' the ' + possible_object + ' ' + possible_time
        answer = T('i did not do anything with the ') + possible_object + ' ' + possible_time
        result.append(time_ref + lf + question + lf + answer)

    return any_of(*result)


def get_where_have_you_been_dialog(e, question_time):
    result = []
    for action_time, action in [(t, a) for t, k in e.keyframes.items() for a in k.actions if a.status == 'success']:
        if action.name.split()[0] == 'move':
            time_ref = e.start.strftime('%Y %m %d %w %H %M %S')
            question = 'where have you been ' + get_time_ref(action_time, question_time, prefer_exact=True)
            answer = 'i just arrived at the ' + any_of(*action.nl_object)
            result.append(time_ref + lf + question + lf + answer)
    return any_of(*result)


def episode_grammar(episodes: EpisodeList, question_time: datetime, simple=False) -> Grammar:
    global o
    if simple:
        o = convert_to_grammar
    else:
        o = optional

    # Optional fillers
    global please, _please, tell_me
    please = o("please ")
    _please = o(" please")
    tell_me = T('tell me ') | 'talk to me about ' | (T('explain ') + o('to me '))

    grammar = get_whats_the_time_dialog(question_time)

    if len(episodes) == 1:
        grammar |= get_what_did_you_do_dialog(episodes[0], question_time, simple, only=True)

    for e in episodes:
        grammar |= get_what_did_you_do_dialog(e, question_time, simple)
        grammar |= get_when_dialogs(e, question_time, simple)
        grammar |= get_action_stats_dialog(e, question_time)
        grammar |= get_which_object_did_you_do_move_dialog(e, question_time)
        grammar |= get_how_long_did_it_take_dialog(e, question_time)
        grammar |= get_where_have_you_been_dialog(e, question_time)

    return grammar
