import re
import sys
import traceback
from datetime import datetime
from pprint import pprint
from typing import Dict

from eval.semantic_eval import is_semantically_correct, contains, objects, locations, estimate_time_ref


def find_index_in_episode_list(episode_strings, key, question_time):
    try:
        tgt_time_ref, tgt_estimation_interval = estimate_time_ref(key, question_time)
        for i, episode in enumerate(episode_strings):
            key_frame: str
            for key_frame in episode.splitlines():
                if len(key_frame.strip()) == 0 or '<pad_frame>' in key_frame:
                    continue
                episode_time = datetime.strptime(' '.join(key_frame.split()[:6]), '%Y %m %d %H %M %S')
                if tgt_time_ref - tgt_estimation_interval \
                        <= episode_time \
                        <= tgt_time_ref + tgt_estimation_interval:
                    if contains(key, *objects):
                        for obj in objects:
                            if obj in key and obj in episode_strings[i]:
                                return i
                    else:
                        return i
    except ValueError:
        traceback.print_exc()
        pass  # key doesn't seem to contain a time spec

    if contains(key, *objects) and contains(key, *locations):
        for obj in objects:
            for loc in locations:
                if obj in key and loc in key:
                    correct_episode = min((i for i, episode in enumerate(episode_strings)
                                           if obj in episode and loc in episode),
                                          default=None)
                    if correct_episode is not None:
                        return correct_episode
    elif contains(key, *locations):
        for loc in locations:
            if loc in key:
                correct_episode = min((i for i, episode in enumerate(episode_strings)
                                       if loc in episode),
                                      default=None)
                if correct_episode is not None:
                    return correct_episode
    return None


def find_episode_ref_offset(episode_list, hypo, query, target):
    date_string = ' '.join(query.split()[:7])
    query_time = datetime.strptime(date_string, '%Y %m %d %w %H %M %S')

    episode_strings = episode_list.replace(',', '').split('<eos_frame>')
    correct_episode = find_index_in_episode_list(episode_strings, target, query_time)
    hypo_episode = find_index_in_episode_list(episode_strings, hypo, query_time)

    if correct_episode is None:
        if 'what is the time' not in query and 'what day' not in query and 'arm' not in query and 'hand' not in query:
            # print('Could not handle', query, '###', target)
            pass
        return None, correct_episode, hypo_episode
    if hypo_episode is None:
        return None, correct_episode, hypo_episode
    if is_semantically_correct(query, target, hypo):
        return 0, correct_episode, hypo_episode

    return hypo_episode - correct_episode, correct_episode, hypo_episode


def reference_eval(lines):
    episode_ref_offsets: Dict[int, int] = {
        # keys: Offset from correct episode, value: counter
        0: 0,
        None: 0  # Key None is used if there is no episode fitting to the answer (i.e. the answer is completely wrong)
    }
    offsets_by_episode_count = {1: {}, 2: {}, 3: {}}
    correct_index_by_episode_count = {1: {None: 0, 0: 0}, 2: {None: 0, 0: 0, 1: 0}, 3: {None: 0, 0: 0, 1: 0, 2: 0}}
    hypo_index_by_episode_count = {1: {None: 0, 0: 0}, 2: {None: 0, 0: 0, 1: 0}, 3: {None: 0, 0: 0, 1: 0, 2: 0}}
    total_episode_counter = {1: 0, 2: 0, 3: 0}
    hypo_episode_counter = {}
    target_episode_counter = {}
    episode_buffer: Dict[int, str] = {}
    target = None
    query = None
    read_episode_id, read_episode_buf = None, ''
    for line in lines:
        if read_episode_id:
            if ']' in line:
                episode_buffer[read_episode_id - 1] = read_episode_buf
                read_episode_id = None
            else:
                read_episode_buf += line
            continue

        split = line.split()
        if line.startswith('S-'):
            query = ' '.join(split[1:])
        elif line.startswith('T-'):
            target = ' '.join(split[1:])
        elif line.startswith('E-'):
            read_episode_id = int(re.match(r'E-(\d+)', line).group(1)) + 1
            read_episode_buf = ''
        elif line.startswith('H-'):
            sample_id = int(re.match(r'H-(\d+)', line).group(1))
            hypo = ' '.join(split[2:])
            episode_list = episode_buffer.pop(sample_id)
            offset, correct_episode, hypo_episode = find_episode_ref_offset(
                episode_list, hypo.lower(), query.lower(), target.lower())

            length = episode_list.count('<eos_frame>')
            total_episode_counter[length] += 1
            hypo_episode_counter.setdefault(hypo_episode, 0)
            hypo_episode_counter[hypo_episode] += 1
            target_episode_counter.setdefault(correct_episode, 0)
            target_episode_counter[correct_episode] += 1
            episode_ref_offsets.setdefault(offset, 0)
            episode_ref_offsets[offset] += 1
            correct_index_by_episode_count[length].setdefault(correct_episode, 0)
            correct_index_by_episode_count[length][correct_episode] += 1
            hypo_index_by_episode_count[length].setdefault(hypo_episode, 0)
            hypo_index_by_episode_count[length][hypo_episode] += 1
            offsets_by_episode_count[length].setdefault(offset, 0)
            offsets_by_episode_count[length][offset] += 1
            if offset == 0 and not is_semantically_correct(query, target, hypo):
                print('Something wrong?', target, '##', hypo, episode_list)

    return (total_episode_counter, episode_ref_offsets,
            hypo_episode_counter, target_episode_counter,
            correct_index_by_episode_count, hypo_index_by_episode_count, offsets_by_episode_count)


if __name__ == '__main__':
    names = ['total episodes',
             'episode ref offsets',
             'hypo episode counter',
             'target episode counter',
             'correct index by episode count',
             'hypo index by episode count',
             'offsets by episode count']
    for name, result in zip(names, reference_eval(open(sys.argv[1], 'r'))):
        print()
        print(name)
        pprint(result)
