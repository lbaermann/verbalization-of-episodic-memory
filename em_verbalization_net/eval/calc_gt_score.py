import sys

ignore_locations = False if len(sys.argv) < 3 else sys.argv[2] == 'ignore_locations'


def _ignore_locations(s):
    if ignore_locations:
        locations = ['sink', 'countertop', 'stove', 'fridge',
                     'cupboard', 'oven', 'table']
        for loc in locations:
            s = s.replace(loc, 'LOC')
        return s
    else:
        return s


score = 0
total = 0
target = None
answers = {}
refs = set()
for line in open(sys.argv[1], 'r'):
    split = line.split()
    if line.startswith('T-'):
        target = ' '.join(split[1:])
        refs.add(target)
        total += 1
    elif line.startswith('H-'):
        hypo = ' '.join(split[2:])
        answers[hypo] = answers[hypo] + 1 if hypo in answers else 1
        if _ignore_locations(hypo) == _ignore_locations(target):
            score += 1
        else:
            # print('Wrong:', hypo, '!=', target)
            pass

print('Score:', score, 'out of', total)
print(round(score / total * 100, ndigits=1), '%')
print()
print(len(answers), 'different answers out of', len(refs), 'in total',
      '\nTop 5:\n' +
      '\n'.join(str(round(freq / total * 100, ndigits=1)) + '% ' + ans
                for ans, freq in sorted(answers.items(), key=lambda item: item[1], reverse=True)[:5]))
