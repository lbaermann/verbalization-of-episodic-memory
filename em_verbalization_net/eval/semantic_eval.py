import re
import sys
import time
from datetime import datetime, timedelta
from functools import reduce
from multiprocessing import Pool, cpu_count
from typing import Tuple

from data_generator.episode_grammar import int_to_string_dict

_silent = True
objects = {'cerealsbox', 'cereals', 'box', 'milk',
           'multivitaminjuice', 'multivitamin', 'vitamin', 'juice',
           'cloth', 'torch'}
colors = {'red', 'green', 'blue', 'white', 'yellow', 'orange'}
for color in colors:
    objects.add(color + ' cup')
    objects.add(color + ' mug')
    objects.add(color + ' plate')
    objects.add(color + 'cup')
    objects.add(color + 'mug')
    objects.add(color + 'plate')
locations = {'sink', 'countertop', 'space', 'stove', 'fridge',
             'cupboard', 'oven', 'table', 'placesetting', 'roundtable', 'round table',
             'setting place', 'big table', 'sideboard', 'small table', 'dishwasher'}
move_words = {'move', 'moved', 'bring', 'brought'}
grasp_words = {'grasp', 'grasped', 'grab', 'grabbed'}
string_to_int_dict = {v: k for k, v in int_to_string_dict.items()}

time_word_to_time_range = {
    # word : (time range start, time range end)
    # only estimates, may overlap! (fuzzy language terms)
    'morning': (0, 10),
    'before noon': (10, 12),
    ' noon': (11, 14),  # Noon gets a trailing space to avoid seeing it before 'afternoon'. No sentence starts with noon
    'afternoon': (14, 18),
    'evening': (17, 24)
}

_months = [datetime.now().replace(day=1, month=n) for n in range(1, 13)]
_month_names = [m.strftime('%B').lower() for m in _months]
_month_short_names = [m.strftime('%b').lower() for m in _months]


class _res:
    hour_am_pm = re.compile(r'(\d|1\d|2[0123])\s*(pm|am|o.+clock)')
    hour_min_second = re.compile(r'(\d|1\d|2[0123])\s*:\s*([0-5]?\d)\s*(?::\s*([0-5]?\d))?')

    _any_num_re = '(' + '|'.join(string_to_int_dict.keys()) + r'|\d+)'
    any_num_days = re.compile(rf'{_any_num_re}\s*days?|day')
    any_num_hours = re.compile(rf'{_any_num_re}\s*hours?|hour')
    any_num_mins = re.compile(rf'{_any_num_re}\s*minutes?|minute')

    _any_month_name = '(?P<month>' + '|'.join(_month_names) + '|' + '|'.join(_month_short_names) + ')'
    _day_re = r'(?P<day>\d+)'
    month_day = re.compile(rf'{_any_month_name}\s*{_day_re}')
    day_month = re.compile(rf'{_day_re}(\w|\s)+{_any_month_name}')

    _any_object_re = '(' + '|'.join(objects) + ')'
    _any_location_re = '(' + '|'.join(locations) + ')'
    object_to_location = re.compile(rf'{_any_object_re}\s+to(?:\s|\w)+?{_any_location_re}')

    duration_expr = re.compile(rf'{_any_num_re} (s|seconds|min|m|minutes)')


def string_to_int(string):
    if string in string_to_int_dict:
        return string_to_int_dict[string]
    else:
        return int(string)


def contains(string, *options):
    """
    Returns true iff the given string contains one of the options as a whole word.
    """
    split = string.split()
    word_pairs = [split[i - 1] + ' ' + split[i] for i in range(1, len(split))]
    word_triples = [split[i - 2] + ' ' + split[i - 1] + ' ' + split[i] for i in range(2, len(split))]
    x: str
    for x in options:
        num_words = len(x.split())
        if num_words == 3:
            if x in word_triples:
                return True
        elif num_words == 2:
            if x in word_pairs:
                return True
        elif num_words == 1:
            if x in split:
                return True
        else:
            raise NotImplementedError(num_words)
    return False


def count(string, *options):
    """
    Returns the number of items from options the given string contains as a whole word.
    """
    return sum(1 for x in options if x in string.split())


def check_hypo_for_items(items, target, hypo):
    for i in items:
        if i in target and i not in hypo:
            return False
    return True


def estimate_time(utterance, base):
    match = re.search(_res.hour_am_pm, utterance)
    if match:
        hour = int(match.group(1))
        pm_offset = 12 if match.group(2) == 'pm' and hour != 12 else 0
        return base.replace(hour=hour + pm_offset, minute=30, second=0), \
               timedelta(minutes=59, seconds=59)
    roughly = contains(utterance, 'about', 'around', 'roughly', 'circa', 'broadly', 'rounded')
    match = re.search(_res.hour_min_second, utterance)
    if match:
        hour = int(match.group(1))
        minutes = int(match.group(2))
        second = match.group(3)
        if second:
            return base.replace(hour=hour, minute=minutes, second=int(second)), \
                   timedelta(seconds=10 if roughly else 1)
        else:
            return base.replace(hour=hour, minute=minutes, second=30), \
                   timedelta(minutes=10 if roughly else 1)
    for time_word, (start, end) in time_word_to_time_range.items():
        if time_word in utterance:
            delta = timedelta(hours=end - start) / 2
            middle = base.replace(hour=start, minute=0, second=0) + delta
            return middle, delta

    if not _silent:
        print('Warn: Cannot estimate time for', utterance, file=sys.stderr)
    return base, timedelta(0)


def estimate_time_ref(utterance: str, question_time: datetime) -> Tuple[datetime, timedelta]:
    if 'yesterday' in utterance:
        yesterday = question_time - timedelta(days=1)
        return estimate_time(utterance, yesterday)
    if contains(utterance, 'today', 'this day', 'current day'):
        return estimate_time(utterance, question_time)
    if contains(utterance, 'in the last', 'ago', 'previous'):
        def singular_save_string_to_int(group, unit):
            if group:
                return string_to_int(group)
            elif any(x in utterance for x in
                     [f'in the last {unit}', f'a {unit} ago', f'an {unit} ago', f'previous {unit}']):
                return 1
            else:
                raise ValueError(unit, group, utterance)

        match = re.search(_res.any_num_days, utterance)
        if match:
            days = singular_save_string_to_int(match.group(1), 'day')
            return estimate_time(utterance, question_time - timedelta(days=days))
        match = re.search(_res.any_num_hours, utterance)
        if match:
            hours = singular_save_string_to_int(match.group(1), 'hour')
            return question_time - timedelta(hours=hours), timedelta(minutes=70)
        match = re.search(_res.any_num_mins, utterance)
        if match:
            minutes = singular_save_string_to_int(match.group(1), 'minute')
            return question_time - timedelta(minutes=minutes), timedelta(minutes=15)
    return estimate_month_and_day(question_time, utterance)


def estimate_month_and_day(question_time, utterance):
    def handle_date_match(match):
        month = match.group('month')
        if month in _month_names:
            month = _month_names.index(month)
        else:
            month = _month_short_names.index(month)
        day = int(match.group('day'))
        mentioned_date = question_time.replace(month=month + 1, day=day)
        if question_time < mentioned_date:
            # The mentioned date should never be in the future.
            # This means we accidentally passed the year boundary.
            mentioned_date = mentioned_date.replace(year=mentioned_date.year - 1)
        return estimate_time(utterance, mentioned_date)

    match1 = re.search(_res.month_day, utterance)
    if match1:
        return handle_date_match(match1)
    match2 = re.search(_res.day_month, utterance)
    if match2:
        return handle_date_match(match2)
    else:
        # There is no date ref and nothing else matched. Seems to refer to today
        return estimate_time(utterance, question_time)


def is_semantically_correct(query, target, hypo):
    query = query.lower().strip()
    target = target.lower().strip()
    hypo = hypo.lower().strip()
    if hypo == target:
        return True

    # For semantic of Yes / No questions, simply refer to yes or no keywords
    if contains(target, 'yes', 'true', 'indeed'):
        return contains(hypo, 'yes', 'true', 'indeed')
    if contains(target, 'no', 'false', 'never'):
        return contains(hypo, 'no', 'false', 'never')

    if contains(query, 'hand', 'arm'):
        return contains(target, 'left') == contains(hypo, 'left') and \
               contains(target, 'right') == contains(hypo, 'right')

    if contains(query, 'when', 'time', 'date', 'day'):
        date_string = ' '.join(query.split()[:7])
        question_time = datetime.strptime(date_string, '%Y %m %d %w %H %M %S')
        tgt_time_ref, tgt_estimation_interval = estimate_time_ref(target, question_time)
        hypo_time_ref, hypo_estimation_interval = estimate_time_ref(hypo, question_time)

        def is_in_interval(date):
            return tgt_time_ref - tgt_estimation_interval \
                   <= date \
                   <= tgt_time_ref + tgt_estimation_interval

        hypo_encloses_target = hypo_time_ref - hypo_estimation_interval \
                               <= tgt_time_ref \
                               <= hypo_time_ref + hypo_estimation_interval
        return is_in_interval(hypo_time_ref - hypo_estimation_interval) \
               or is_in_interval(hypo_time_ref + hypo_estimation_interval) \
               or hypo_encloses_target

    # Check if action matches
    if contains(target, *move_words) and not contains(hypo, *move_words):
        return False
    if contains(target, *grasp_words) and not contains(hypo, *grasp_words):
        return False

    target_object_count = count(target, *objects)
    target_location_count = count(target, *locations)
    hypo_object_count = count(hypo, *objects)
    hypo_location_count = count(hypo, *locations)

    if target_object_count != hypo_object_count or target_location_count != hypo_location_count:
        return False

    if target_object_count and target_location_count > 1 or hypo_object_count > 1 and hypo_location_count > 1:
        target_object_to_locations = _res.object_to_location.findall(target)
        hypo_object_to_locations = _res.object_to_location.findall(hypo)
        # Order is not important, only correct object-location pairs
        return set(target_object_to_locations) == set(hypo_object_to_locations)

    if target_object_count > 0 or target_location_count > 0:
        result = True
        if target_object_count > 0:
            result &= check_hypo_for_items(objects, target, hypo)
        if target_location_count > 0:
            result &= check_hypo_for_items(locations, target, hypo)
        return result

    raise ValueError(f'Did not match any eval criteria: {query}, {target}, {hypo}')


def semantic_eval(lines):
    score = 0
    total = 0
    target = None
    query = None
    for line in lines:
        split = line.split()
        if line.startswith('S-'):
            query = ' '.join(split[1:])
        elif line.startswith('T-'):
            target = ' '.join(split[1:])
            total += 1
        elif line.startswith('H-'):
            hypo = ' '.join(split[2:])
            try:
                correct = is_semantically_correct(query, target, hypo)
                score += correct
                if correct and hypo != target and not _silent:
                    print('?', query)
                    print('  H >', hypo)
                    print('  T >', target)
            except Exception:
                if _silent:
                    pass
                else:
                    raise
    return score, total


def parallel_semantic_eval(line_iterators):
    num_procs = len(line_iterators)
    with Pool(num_procs) as pool:
        results = pool.map(semantic_eval, line_iterators)
        return reduce(lambda r1, r2: (r1[0] + r2[0], r1[1] + r2[1]), results, (0, 0))


def main():
    num_workers = cpu_count()
    start = time.time()
    with open(sys.argv[1], 'r', encoding='utf-8') as f:
        lines = f.readlines()
        n = len(lines) // num_workers
        lines_grouped = [lines[i:i + n] for i in range(0, len(lines), n)]
        lines_grouped[-1] += lines[num_workers * n:]

        score, total = parallel_semantic_eval(lines_grouped)

        print('Score:', score, 'out of', total)
        print(round(score / total * 100, ndigits=1), '%')
        print()
    end = time.time()
    print('Duration:', end - start)


if __name__ == '__main__':
    _silent = True
    main()
