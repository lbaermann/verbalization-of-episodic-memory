import queue
import traceback
from argparse import ArgumentParser
from collections import namedtuple
from enum import Enum
from math import ceil
from multiprocessing import Manager
from pathlib import Path
from typing import Dict, List

from click import progressbar

from data_generator.episode_grammar import get_what_did_you_do_question
from data_generator.episodes import EpisodeList
from data_generator.episodes.generator import load_episodes_from_file
from data_generator.grammar import generate_all_words
from eval.semantic_eval import *
from eval.semantic_eval import _res


class EvaluationCategory(Enum):
    CORRECT = 'correct'
    TMI_BUT_CORRECT = 'too much, but correct information'
    TMI_WRONG = 'correct answer, but additional incorrect information'
    PARTIALLY_CORRECT = 'some part of the answer is correct, some wrong'
    PARTIALLY_CORRECT_ONLY_ACTION = 'only action part of the answer is correct'
    WRONG = 'wrong answer, but correct answer intent'
    INAPPROPRIATE = 'answer is not related to the question'


EvaluationResult = namedtuple('EvaluationResult', ['correct_facts', 'incorrect_facts',
                                                   'expected_facts', 'category'])


class CategorizationStats:

    def __init__(self) -> None:
        self.categories_stats = {
            category: 0 for category in EvaluationCategory
        }
        # These two refer to partially correct answers
        #  TMI does not affect this.
        self.correct_info_counter = 0
        self.incorrect_info_counter = 0
        self.expected_info_counter = 0

    def __iadd__(self, other):
        if isinstance(other, CategorizationStats):
            self.correct_info_counter += other.correct_info_counter
            self.incorrect_info_counter += other.incorrect_info_counter
            self.expected_info_counter += other.expected_info_counter
            for c in EvaluationCategory:
                self.categories_stats[c] += other.categories_stats[c]
        elif isinstance(other, Tuple):
            if not isinstance(other, EvaluationResult):
                other = EvaluationResult(*other)
            assert other.incorrect_facts >= 0, other.incorrect_facts
            self.correct_info_counter += other.correct_facts
            self.incorrect_info_counter += other.incorrect_facts
            self.expected_info_counter += other.expected_facts
            self.categories_stats[other.category] += 1
        else:
            raise ValueError(other)
        return self

    @property
    def information_precision(self):
        return self.correct_info_counter / (self.correct_info_counter + self.incorrect_info_counter)

    @property
    def information_recall(self):
        return self.correct_info_counter / self.expected_info_counter

    def __str__(self) -> str:
        all_lines = sum(self.categories_stats.values())
        return f'{sum(self.categories_stats.values()):6} Total\n' \
               + ('\n'.join(f'{v:6} ({v / all_lines:>5.1%}): {k.name}' for k, v in self.categories_stats.items())) \
               + '\n\n' \
                 f'info. precision: {self.information_precision:.1%}\n' \
                 f'info. recall   : {self.information_recall:.1%}\n'


_simple_wrong = 0, 1, 1, EvaluationCategory.WRONG
_simple_correct = 1, 0, 1, EvaluationCategory.CORRECT
_simple_inappropriate = 0, 1, 1, EvaluationCategory.INAPPROPRIATE

_what_did_you_do_phrases = list(generate_all_words(get_what_did_you_do_question(time='', simple=False)))


def evaluate_sample(history, query, target, hypo) -> EvaluationResult:
    date_string = ' '.join(query.split()[:7])
    question_time = datetime.strptime(date_string, '%Y %m %d %w %H %M %S')
    query_without_question_time = ' '.join(query.split()[7:])

    if 'what is the time' in query:
        return _handle_whats_the_time(hypo, question_time)

    if 'what day is today' in query:
        return _handle_what_day_is_today(hypo, question_time)

    if 'how your task' in query or 'any problem' in query:
        return _handle_did_you_have_any_problems(query, hypo, target, question_time)

    if any(x in query for x in _what_did_you_do_phrases):
        return _handle_what_did_you_do(history, query, question_time, query_without_question_time, target, hypo)

    if 'when did you' in query:
        return _handle_when_did_you(hypo, question_time, target)

    if 'where have you been' in query:
        return _handle_where_have_you_been(history, query, question_time, target, hypo)

    if 'which object you' in query:
        return _handle_which_object_did_you_move(history, query, question_time, target, hypo)

    if 'how long' in query:
        return _handle_how_long_did_it_take(history, query, query_without_question_time, question_time, target, hypo)

    if target == hypo:
        return _simple_correct

    return _simple_wrong


def _handle_when_did_you(hypo, question_time, target) -> EvaluationResult:
    tgt_time_ref, tgt_estimation_interval = estimate_time_ref(target, question_time)
    hypo_time_ref, hypo_estimation_interval = estimate_time_ref(hypo, question_time)
    if _datetimes_match(tgt_time_ref, tgt_estimation_interval, hypo_time_ref, hypo_estimation_interval):
        return _simple_correct
    else:
        return _simple_wrong


def _handle_did_you_have_any_problems(query, hypo, target, question_time) -> EvaluationResult:
    target_success = 'no' in target.split() or 'everything went fine' in target
    tgt_time_ref, tgt_time_ref_interval = estimate_time_ref(target, question_time)
    hypo_time_ref, hypo_time_ref_interval = estimate_time_ref(hypo, question_time)
    if 'no' in hypo.split() or 'fine' in hypo:
        if target_success:
            hypo_items = _find_items(hypo, objects | locations)
            if len(hypo_items) > 0:
                asked_items = _find_items(query, objects | locations)
                if any(h in asked_items for h in hypo_items) or hypo == target:
                    return 2, 0, 2, EvaluationCategory.CORRECT
                else:
                    return 1, len(hypo_items), 2, EvaluationCategory.PARTIALLY_CORRECT
            else:
                return _simple_correct
        else:
            return _simple_wrong
    if 'yes' in hypo or 'could not successfully' in hypo:
        if target_success:
            return _simple_wrong
        else:
            if _datetimes_match(tgt_time_ref, tgt_time_ref_interval, hypo_time_ref, hypo_time_ref_interval):
                return 2, 0, 2, EvaluationCategory.CORRECT
            else:
                return 1, 1, 2, EvaluationCategory.PARTIALLY_CORRECT
    else:
        return _simple_inappropriate


def _handle_how_long_did_it_take(history, query, query_without_question_time,
                                 question_time, target, hypo) -> EvaluationResult:
    tgt_match = _res.duration_expr.search(target)
    if tgt_match:
        tgt_duration_number = string_to_int(tgt_match.group(1))
        tgt_duration_unit_minutes = tgt_match.group(2).startswith('m')
        hypo_match = _res.duration_expr.search(hypo)
        if not hypo_match:
            if 'did not do anything with' in hypo:
                return _simple_wrong
            else:
                return _simple_inappropriate

        hypo_duration_number = string_to_int(hypo_match.group(1))
        hypo_duration_unit_minutes = hypo_match.group(2).startswith('m')
        if tgt_duration_unit_minutes and hypo_duration_unit_minutes:
            if hypo_duration_number == tgt_duration_number:
                return _simple_correct
            else:
                return _simple_wrong
        else:
            tgt_duration = timedelta(minutes=tgt_duration_number) \
                if tgt_duration_unit_minutes else timedelta(seconds=tgt_duration_number)
            hypo_duration = timedelta(minutes=hypo_duration_number) \
                if hypo_duration_unit_minutes else timedelta(seconds=hypo_duration_number)
            tolerance = timedelta(seconds=10)
            if tgt_duration - tolerance <= hypo_duration <= tgt_duration + tolerance:
                return _simple_correct
            else:
                return _simple_wrong
    else:
        if 'did not do' in hypo:
            total_info = 1
            correct_info = 1

            hypo_time, hypo_time_tolerance = estimate_time_ref(hypo, question_time)
            if 'today' in hypo or hypo_time != question_time:
                # Hypo contains a time ref. Check if it's correct
                ref, ref_tolerance = estimate_time_ref(query_without_question_time, question_time)
                if ref_tolerance.total_seconds() == 0 and 'today' in query:
                    ref = ref.replace(hour=12, minute=0)
                    ref_tolerance = timedelta(hours=12, seconds=1)
                correct_info += int(_datetimes_match(ref, ref_tolerance, hypo_time, hypo_time_tolerance))
                total_info += 1

            asked_objects = _find_items(query, objects)
            if len(asked_objects) > 0:
                hypo_objects = _find_items(hypo, objects)
                total_info += 1
                correct_info += int(len(hypo_objects) == 0 or any(h in asked_objects for h in hypo_objects))

            if correct_info == total_info:
                return correct_info, 0, total_info, EvaluationCategory.CORRECT
            else:
                return correct_info, total_info - correct_info, total_info, EvaluationCategory.PARTIALLY_CORRECT
        elif _res.duration_expr.search(hypo):
            return _simple_wrong
        else:
            return _simple_inappropriate


def _handle_which_object_did_you_move(history, query, question_time, target, hypo) -> EvaluationResult:
    target_objects = _find_items(target, objects)
    hypo_objects = _find_items(hypo, objects)

    if len(target_objects) == 0:
        if len(hypo_objects) == 0:
            if 'no' in hypo.split() or 'not' in hypo.split():
                return _simple_correct
            else:
                return _simple_inappropriate
        else:
            return _simple_wrong

    correct_facts = 0
    number_of_facts = len(target_objects)
    for obj in target_objects:
        if obj in hypo_objects:
            correct_facts += 1

    incorrect_facts = len(hypo_objects) - correct_facts
    if correct_facts == 0:
        return 0, incorrect_facts, number_of_facts, EvaluationCategory.WRONG
    elif correct_facts < number_of_facts:
        return correct_facts, incorrect_facts, number_of_facts, EvaluationCategory.PARTIALLY_CORRECT
    else:
        if incorrect_facts == 0:
            return correct_facts, 0, number_of_facts, EvaluationCategory.CORRECT
        else:
            return correct_facts, incorrect_facts, number_of_facts, EvaluationCategory.TMI_WRONG


def _handle_where_have_you_been(history, query, question_time, target, hypo) -> EvaluationResult:
    # if hypo == target:
    #    return _simple_correct

    tgt_time_ref, tgt_estimation_interval = estimate_time_ref(query, question_time)
    if tgt_estimation_interval.seconds >= 60:
        # no second precision was given. "Where have you been at 06:50"
        # don't want to have tolerance of one ±1 min here!
        tgt_estimation_interval = timedelta(seconds=31)
    ref_episode = _get_referred_episode(history, query, question_time)
    correct_answers = []
    for timestamp, keyframe in ref_episode.keyframes.items():
        if _datetimes_match(tgt_time_ref, tgt_estimation_interval, timestamp, timedelta(seconds=10)):
            for a in keyframe.actions:
                if 'move' in a.name and a.status == 'success':
                    move_destinations = a.nl_object
                    correct_answers += move_destinations
    hypo_locations = _find_items(hypo, locations)
    if len(hypo_locations) == 0:
        return _simple_inappropriate
    elif len(hypo_locations) == 1:
        if hypo_locations[0] in correct_answers:
            return _simple_correct
        else:
            return _simple_wrong
    else:
        # Where have you been should only have one answer. TMI
        if all(l in correct_answers for l in hypo_locations):
            return 1, 0, 1, EvaluationCategory.TMI_BUT_CORRECT
        else:
            return 1, len(hypo_locations), 1, EvaluationCategory.TMI_WRONG


def _datetimes_match(tgt, tgt_tolerance, hypo, hypo_tolerance):
    def is_in_interval(dt):
        return tgt - tgt_tolerance \
               <= dt \
               <= tgt + tgt_tolerance

    hypo_encloses_target = hypo - hypo_tolerance \
                           <= tgt \
                           <= hypo + hypo_tolerance
    return is_in_interval(hypo - hypo_tolerance) \
           or is_in_interval(hypo + hypo_tolerance) \
           or hypo_encloses_target


def _handle_what_did_you_do(history: EpisodeList, query: str, question_time: datetime,
                            query_without_question_time: str, target: str, hypo: str) -> EvaluationResult:
    ref_episode = _get_referred_episode(history, query_without_question_time, question_time)
    arg_count, goal_and_args = ref_episode.parse_goal_and_args()
    action = goal_and_args[0]
    number_of_facts = 1  # Action is definitely a fact
    correct_facts = 0
    action_correct = False

    if 'move' in action:
        if contains(hypo, *move_words):
            correct_facts += 1
            action_correct = True
    elif 'grasp' in action:
        if contains(hypo, *grasp_words):
            correct_facts += 1
            action_correct = True
    else:
        raise NotImplementedError(action)

    target_objects = _find_items(target, objects)
    target_locations = _find_items(target, locations)
    number_of_facts += len(target_objects) + len(target_locations)
    hypo_objects = _find_items(hypo, objects)
    hypo_locations = _find_items(hypo, locations)

    for obj in target_objects:
        if obj in hypo_objects:
            correct_facts += 1
    for loc in target_locations:
        if loc in hypo_locations:
            correct_facts += 1

    incorrect_facts = 1 + len(hypo_objects) + len(hypo_locations) - correct_facts

    if 'tried to but failed' in target:
        number_of_facts += 1
        if 'tried to' in hypo:
            correct_facts += 1
    if 'tried to' in hypo and 'tried to but failed' not in target:
        incorrect_facts += 1

    if correct_facts == 0 and (not any(x in hypo for x in move_words | grasp_words | objects | locations) or
                               any(x in hypo for x in ['no problem', 'could not successfully'])):
        return 0, incorrect_facts, number_of_facts, EvaluationCategory.INAPPROPRIATE

    eval_result = (EvaluationCategory.CORRECT if correct_facts == number_of_facts
                   else (EvaluationCategory.WRONG if correct_facts == 0
                         else (EvaluationCategory.PARTIALLY_CORRECT_ONLY_ACTION if action_correct and correct_facts == 1
                               else EvaluationCategory.PARTIALLY_CORRECT)))

    if len(target_objects) > 1 and len(target_locations) > 1:
        target_object_to_locations = _res.object_to_location.findall(target)
        hypo_object_to_locations = _res.object_to_location.findall(hypo)
        if set(target_object_to_locations) != set(hypo_object_to_locations):
            # Argument Order is wrong!
            return correct_facts, incorrect_facts, number_of_facts, EvaluationCategory.PARTIALLY_CORRECT

    # If not correct till now, do not care about TMI but return PARTIALLY_CORRECT or WRONG
    if correct_facts == number_of_facts and \
            (len(hypo_objects) > len(target_objects) or len(hypo_locations) > len(target_locations)):
        tmi_objects = set(hypo_objects) - set(target_objects)
        tmi_locations = set(hypo_locations) - set(target_locations)
        tmi_items = tmi_objects | tmi_locations
        tmi_wrong = False
        for tmi_item in tmi_items:
            if tmi_item not in goal_and_args:
                tmi_wrong = True
                break
        if tmi_wrong:
            return correct_facts, incorrect_facts, number_of_facts, EvaluationCategory.TMI_WRONG
        else:
            return correct_facts, incorrect_facts, number_of_facts, EvaluationCategory.TMI_BUT_CORRECT

    if hypo == target and eval_result != EvaluationCategory.CORRECT:
        print(query, hypo, eval_result)

    return correct_facts, incorrect_facts, number_of_facts, eval_result


def _get_referred_episode(history, utterance, question_time):
    ref_time, ref_time_tolerance = estimate_time_ref(utterance, question_time)
    if ref_time_tolerance.total_seconds() == 0 or len(history) == 1:
        assert len(history) == 1
        ref_episode = history[0]
    else:
        possible_ref_episodes = [e for e in history
                                 if _datetimes_match(e.start, e.end - e.start, ref_time, ref_time_tolerance)]
        assert len(possible_ref_episodes) == 1, f'{possible_ref_episodes}, {question_time}, {utterance}, {ref_time}'
        ref_episode = possible_ref_episodes[0]
    return ref_episode


def _find_items(utterance, items):
    result = []
    for i in items:
        if contains(utterance, i):
            result.append(i)
    for i, x in enumerate(list(result)):
        for j, y in enumerate(list(result)):
            if i == j:
                continue
            if x in y and x != y and x in result:
                result.remove(x)
    return result


def _handle_what_day_is_today(hypo, question_time) -> EvaluationResult:
    fake_reference_date = question_time.replace(month=(question_time.month + 6) % 12 + 1, day=1)
    hypo_time_ref, hypo_estimation_interval = estimate_time_ref(hypo, fake_reference_date)
    if hypo_time_ref.month == fake_reference_date.month:
        # There was no date information present in the hypo, estimate just returned the fake reference value
        return _simple_inappropriate
    if hypo_time_ref.date() == question_time.date():
        return _simple_correct
    else:
        return _simple_wrong


def _handle_whats_the_time(hypo, question_time) -> EvaluationResult:
    fake_reference_date = question_time.replace(month=(question_time.month + 6) % 12 + 1, day=1)
    hypo_time_ref, hypo_estimation_interval = estimate_time_ref(hypo, fake_reference_date)
    if hypo_estimation_interval.total_seconds() == 0:  # This indicates no time could be parsed
        return _simple_inappropriate

    if hypo_time_ref.date() != fake_reference_date.date():
        # Additional date information was present!
        if hypo_time_ref.date() == question_time.date():
            tmi_state = EvaluationCategory.TMI_BUT_CORRECT
        else:
            tmi_state = EvaluationCategory.TMI_WRONG
    else:
        tmi_state = None
    # Correct the fake reference date
    hypo_time_ref = datetime.combine(question_time.date(), hypo_time_ref.time())
    if _datetimes_match(tgt=question_time, tgt_tolerance=timedelta(minutes=1),
                        hypo=hypo_time_ref, hypo_tolerance=hypo_estimation_interval):
        if tmi_state:
            return 1, 1 if tmi_state == EvaluationCategory.TMI_WRONG else 0, 1, tmi_state
        else:
            return _simple_correct
    else:
        return _simple_wrong


def _local_do_eval(progress_report, given_episode_ids: Dict[int, int], histories,
                   eval_only_histories_of_length: int, lines):
    episode_id_buffer: Dict[int, int] = given_episode_ids or {}
    target = None
    query = None
    stats = CategorizationStats()
    local_progress = 0
    for line in lines:
        local_progress += len(line.encode())
        if progress_report and local_progress > 4e5:
            progress_report(local_progress)
            local_progress = 0
        split = line.split()
        if line.startswith('S-'):
            query = ' '.join(split[1:])
        elif line.startswith('T-'):
            target = ' '.join(split[1:])
        elif line.startswith('E-') and not given_episode_ids:
            match = re.match(r'E-(\d+)\s+(\d+)', line)
            assert match
            episode_id_buffer[int(match.group(1))] = int(match.group(2))
        elif line.startswith('H-'):
            sample_id = int(re.match(r'H-(\d+)', line).group(1))
            hypo = ' '.join(split[2:])
            assert sample_id in episode_id_buffer, sample_id
            history: EpisodeList = histories[episode_id_buffer.pop(sample_id)]
            if eval_only_histories_of_length is not None and len(history) != eval_only_histories_of_length:
                continue

            try:
                stats += evaluate_sample(history, query.lower().strip(), target.lower().strip(), hypo.lower().strip())
            except Exception as e:
                print('Failed to evaluate', query, target, hypo, sep=' ## ', file=sys.stderr)
                traceback.print_exc()
                stats += _simple_inappropriate
    return stats


def categorizing_eval(input_file: Path, episodes_file: Path, given_episode_ids: List[int] = None,
                      eval_only_histories_of_length: int = None, workers=1):
    assert input_file.is_file(), input_file
    assert episodes_file.is_file(), episodes_file
    assert eval_only_histories_of_length is None or eval_only_histories_of_length > 0
    start_time = time.time()

    histories = load_episodes_from_file(episodes_file)
    episode_ids: Dict[int, int] = {} if given_episode_ids else None
    if given_episode_ids:
        for i, entry in enumerate(given_episode_ids):
            episode_ids[i] = entry
    with input_file.open() as in_file, progressbar(length=input_file.stat().st_size) as bar:
        if workers > 1:
            all_lines = in_file.readlines()
            # Predetermine batch size, remove intro and extro lines
            batch_start = -1
            for i, l in enumerate(all_lines):
                if batch_start == -1 and l.startswith('E-'):
                    batch_start = i
                if batch_start >= 0 and not l.startswith('E-'):
                    batch_size = 5 * (i - batch_start)  # 5: E-, S-, T-, H-, P- lines
                    all_lines = all_lines[batch_start:-2]
                    break
            with Pool(workers) as pool, Manager() as manager:
                progress_queue = manager.Queue()
                worker_length = ceil(len(all_lines) // workers)
                worker_length += batch_size - worker_length % batch_size
                assert worker_length % batch_size == 0
                inputs = [(progress_queue.put_nowait,
                           episode_ids, histories, eval_only_histories_of_length,
                           all_lines[i * worker_length:(i + 1) * worker_length]) for i in range(workers)]
                all_stats = pool.starmap_async(_local_do_eval, inputs)

                while not all_stats.ready():
                    try:
                        update = progress_queue.get(block=True, timeout=0.2)
                        bar.update(update)
                    except queue.Empty:
                        pass
                bar.finish()

                stats = CategorizationStats()
                for s in all_stats.get():
                    stats += s
        else:
            stats = _local_do_eval(lambda p: bar.update(p), episode_ids, histories,
                                   eval_only_histories_of_length, in_file)

    if eval_only_histories_of_length:
        print(f'Evaluated only histories of length {eval_only_histories_of_length} '
              f'({sum(1 for h in histories if len(h) == eval_only_histories_of_length)} histories used)')
    print(stats)
    print(f'Took {time.time() - start_time} seconds')


def main():
    def _path(s):
        return Path(s)

    parser = ArgumentParser()
    parser.add_argument('--workers', metavar='N', type=int, required=False, default=1,
                        help='Number of parallel workers.')
    parser.add_argument('--episodes', metavar='FILE', type=_path, required=True,
                        help='Episodes file')
    parser.add_argument('--episode-ids', metavar='FILE', type=_path, required=False, default=None,
                        help='Episode ID indirection file. Only needs to be present if fairseq-generate output does not'
                             ' contain useful "E-{SampleID} {EpisodeID}" lines, as is the case when using EMV Task with'
                             ' --no-episode-data')
    parser.add_argument('--eval-only-history-length', type=int, default=None, required=False,
                        help='Eval only histories of the given length. All other histories will be ignored.')
    parser.add_argument('input', metavar='INPUT', type=_path,
                        help='Input file (expected to be fairseq-generate output)')
    args = parser.parse_args()

    if args.episode_ids:
        with args.episode_ids.open() as content:
            episode_ids = [int(line) for line in content]
    else:
        episode_ids = None

    categorizing_eval(args.input, args.episodes, given_episode_ids=episode_ids,
                      eval_only_histories_of_length=args.eval_only_history_length, workers=args.workers)


if __name__ == '__main__':
    main()
