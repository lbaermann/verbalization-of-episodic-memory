import re
from pathlib import Path
import csv

from eval.categorizing_eval import EvaluationCategory


def floatstr(f: float):
    return str(round(f, ndigits=3))


working_dir = Path('.')
with open('results.csv', mode='w+') as outfile:
    writer = csv.writer(outfile)
    writer.writerow(['experiment', 'checkpoint', 'history_length']
                    + [category.name for category in EvaluationCategory]
                    + ['info. precision', 'info. recall'])

    for result_dir in working_dir.glob('results-*'):
        experiment_id = result_dir.name.replace('results-', '')
        for stats_file in result_dir.glob('stats_*_test.txt'):
            match = re.fullmatch(r'stats_ep(\d+)_(best|last)_test.txt', stats_file.name)
            assert match, f'{stats_file.name}'

            episode_length = int(match.group(1))
            checkpoint = match.group(2)
            text = stats_file.read_text()

            results_dict = {}
            for percentage, category in re.findall(r'\(\s*(\d+\.\d+)\s*%\):\s*([A-Z_]+)', text):
                results_dict[category] = float(percentage) / 100
            for category, percentage in re.findall(r'info. (precision|recall)\s*:\s*(\s*\d+\.\d+)%', text):
                results_dict[category] = float(percentage) / 100

            results_list = [experiment_id, checkpoint, str(episode_length)]
            results_list += [floatstr(results_dict[category.name]) for category in EvaluationCategory]
            results_list += [floatstr(results_dict[category]) for category in ['precision', 'recall']]
            writer.writerow(results_list)
