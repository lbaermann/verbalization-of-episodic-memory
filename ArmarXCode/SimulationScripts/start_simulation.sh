
scenarios=~/armarx.projects/verbalization-of-episodic-memory/ArmarXCode/VerbalizationOfEpisodicMemory/scenarios

armarx scenario start $scenarios/Armar3PlanningSimulation
sleep 4
armarx scenario start $scenarios/Armar3PlanningSkills
sleep 3
armarx scenario start $scenarios/Armar3PlanningTest
sleep 9
armarx scenario start $scenarios/Armar3EpisodeRecorder/
sleep 1
armarx scenario start $scenarios/TaskProvider

