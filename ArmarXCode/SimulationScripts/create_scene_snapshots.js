
function randomRng(min, max) {
  return Math.random() * (max - min) + min;
}

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 *  * Shuffles array in place.
 *   * @param {Array} a items An array containing the items.
 *    */
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}

var itos = {
  1: 'one',
  2: 'two',
  3: 'three',
  4: 'four',
  5: 'five',
  6: 'six',
  7: 'seven',
  8: 'eight',
  9: 'nine',
  10: 'ten'
}

const cupRotation = {
    "qw" : 0.612372398376465,
    "qx" : -0.61237245798111,
    "qy" : 0.353553414344788,
    "qz" : -0.353553384542465
}
const cupRotation_countertop = {
    "qw" : 0.6830126643180847,
    "qx" : -0.6830127239227295,
    "qy" : -0.1830127090215683,
    "qz" : 0.1830126941204071
}
const cupRotation_roundtable = [[-90, 0, -210], [-90, 0, -200]] 
const cupRotation_lowersideboard = {
    "qw" : -0.3535534,
    "qx" : 0.3535534,
    "qy" : 0.6123724,
    "qz" : -0.6123724
}
const juiceRotation = [[-90, 0, -80], [-90, 0, -65]]
const juiceRotation_countertop = [[-90, 0, -30], [-90, 0, -10]]
const juiceRotation_roundtable = [[-90, 0, -230], [-90, 0, -220]]
const juiceRotation_lowersideboard = [[-90, 0, 60], [-90, 0, 80]]
const plateRotation = {
    "qw" : 0.61237245798111,
    "qx" : 0.612372398376465,
    "qy" : -0.353553384542465,
    "qz" : -0.353553384542465
}
countertopRotations = {
        'greencup': cupRotation_countertop,
        'redcup': cupRotation_countertop,
        'bluecup': cupRotation_countertop,
	'corn': [[-90, 0, -70], [-90, 0, -50]],
        'appletea': [[-90, 0, -5], [-90, 0, 5]],
	'coffeefilters': [[-90, 0, 170], [-90, 0, 190]],
        'cornycerealbars': [[-90, 0, -10], [-90, 0, 10]],
	'popcorn': [[0, 0, -100], [0, 0, -80]],
        'redplate': plateRotation,
        'blueplate': plateRotation,
        'multivitaminjuice': juiceRotation_countertop,
        'orangejuice': [[0, 0, 110], [0, 0, 130]],
        'vanillacookies': juiceRotation_countertop,
        'vitaliscereal': juiceRotation_countertop,
	'milk': juiceRotation_countertop,
	'nesquik': juiceRotation_countertop,
	'oil': [[-90, 0, 20], [-90, 0, 40]]
}
roundtableRotations = {
        'greencup': cupRotation_roundtable,
        'redcup': cupRotation_roundtable,
        'bluecup': cupRotation_roundtable,
	'corn': [[-90, 0, 75], [-90, 0, 95]], // countertop +- 145
        'appletea': [[-90, 0, -150], [-90, 0, -140]], 
	'coffeefilters': [[-90, 0, 315], [-90, 0, 335]], 
        'cornycerealbars': [[-90, 0, 135], [-90, 0, 155]],
	'popcorn': [[0, 0, 45], [0, 0, 65]],
        'redplate': plateRotation,
        'blueplate': plateRotation,
        'multivitaminjuice': juiceRotation_roundtable,
        'orangejuice': [[0, 0, 255], [0, 0, 275]],
        'vanillacookies': juiceRotation_roundtable,
        'vitaliscereal': juiceRotation_roundtable,
	'milk': juiceRotation_roundtable,
	'nesquik': juiceRotation_roundtable,
	'oil': [[-90, 0, 165], [-90, 0, 185]]
}
const objToRotation = {
    'sideboard': {
	'greencup': cupRotation,
	'redcup': cupRotation,
	'bluecup': cupRotation,
	'corn': [[-90, 0, -140], [-90, 0, -160]],
	'appletea': {
            "qw" : 0.5,
            "qx" : -0.5,
	    "qy" : 0.5,
            "qz" : -0.5
	},
	'redplate': plateRotation,
	'blueplate': plateRotation,
	'multivitaminjuice': juiceRotation,
	'orangejuice': [[0, 0, 20], [0, 0, 40]],
	'vanillacookies': juiceRotation,
	'vitaliscereal': juiceRotation,
	'nesquik': juiceRotation,
	'coffeefilters': [[-90, 0, 80], [-90, 0, 100]],
	'cornycerealbars': [[-90, 0, 260], [-90, 0, 280]],
	'popcorn': [[0, 0, 170], [0, 0, 190]],
	'milk': { // TODO adjust milk for other locations (after fixing object recognition and grasps)
	    "qw" : 0.423,
	    "qx" : 0.0,
	    "qy" : 0.0,
	    "qz" : -0.906
	},
	'oil': [[-90, 0, -70], [-90, 0, -50]],
	'sponge': cupRotation,
	'orangebowl': cupRotation
    },
    'countertop': countertopRotations,
    'besidessink': countertopRotations,
    'roundtable': roundtableRotations,
    'lowersideboard': {
	'greencup': cupRotation_lowersideboard,
	'redcup': cupRotation_lowersideboard,
	'bluecup': cupRotation_lowersideboard,
        'corn': [[-90, 0, 110], [-90, 0, 130]],
	'appletea': { // TODO adjust
            "qw" : 0.5,
            "qx" : -0.5,
            "qy" : -0.5,
            "qz" : 0.5
	},
	'popcorn': [[0, 0, -10], [0, 0, 10]],
	'cornycerealbars': [[-90, 0, 80], [-90, 0, 100]],
	'coffeefilters': [[-90, 0, -100], [-90, 0, -80]],
	'redplate': plateRotation,
	'blueplate': plateRotation,
	'multivitaminjuice': juiceRotation_lowersideboard,
	'orangejuice': [[0, 0, 200], [0, 0, 220]],
	'vanillacookies': juiceRotation_lowersideboard,
	'vitaliscereal': juiceRotation_lowersideboard,
	'milk': juiceRotation_lowersideboard,
	'nesquik': juiceRotation_lowersideboard,
	'oil': [[-90, 0, 110], [-90, 0, 130]]
    }
}
const objZValues_sideboard = {
    'greencup': 987.0048828125,
    'redcup': 987.0048828125,
    'bluecup': 987.0048828125,
    'corn': 955,
    'appletea': 1035,
    'multivitaminjuice': 1055.57250976562,
    'orangejuice': 1005,
    'nesquik': 1100,
    'popcorn': 1065,
    'coffeefilters': 1100,
    'cornycerealbars': 1080,
    'milk': 940,
    'pringles': 1100,
    'oil': 1050, 
    'blueplate': 935,
    'vanillacookies': 1162,
    'vitaliscereal': 1115,
    'sponge': 980,
    'orangebowl': 975
}
var objZValues_nearsink = {}
var objZValues_countertop = {}
var objZValues_roundtable = {}
Object.keys(objZValues_sideboard).forEach((key) => {
  const value = objZValues_sideboard[key]
  objZValues_nearsink[key] = value + 25.0
  objZValues_countertop[key] = value + 29.0
  objZValues_roundtable[key] = value - 41.0
})
objZValues_countertop['corn'] = 1000.0
objZValues_nearsink['corn'] = 1000.0
objZValues_countertop['popcorn'] = 1120.0
objZValues_nearsink['popcorn'] = 1120.0
objZValues_nearsink['vanillacookies'] = 1200.0
objZValues_countertop['vanillacookies'] = 1210.0
objZValues_countertop['coffeefilters'] = 1150.0
objZValues_nearsink['coffeefilters'] = 1150.0
objZValues_countertop['cornycerealbars'] = 1143.0
objZValues_nearsink['cornycerealbars'] = 1143.0
objZValues_countertop['nesquik'] = 1165.0
objZValues_nearsink['nesquik'] = 1160.0
objZValues_countertop['orangejuice'] = 1047.0
objZValues_nearsink['orangejuice'] = 1042.0
objZValues_roundtable['appletea'] += 15
objZValues_roundtable['corn'] += 15
objZValues_roundtable['coffeefilters'] += 15
objZValues_roundtable['cornycerealbars'] += 20
objZValues_roundtable['popcorn'] += 20
const objZValues = {
    'sideboard': objZValues_sideboard,
    'lowersideboard': objZValues_nearsink,
    'countertop': objZValues_countertop,
    'besidessink': objZValues_nearsink,
    'roundtable': objZValues_roundtable
} 

load('quaternion_slerp.js');

function isTooClose(x, y, used) {
  const margin = 150
  for (var p of used) {
    let px = p[0], py = p[1],
	dx = px - x, dy = py - y
    if (Math.sqrt(dx*dx, dy*dy) < margin) {
      //print('Too close! ('+ px + ', '+py + ') ('+x+', '+ y +')')
      return true
    }
  }
  return false
}

function createObject(name, loc, x, y, id) {
	if (id >= 20) 
	  id++ // This is because id 20 is somehow already occupied by dishwasher in template snapshot
	var object = db.getCollection('Prior_KitchenKKObjects').find({name: name}).next()
	object._id = "" + id
	object.type = '::memoryx::ObjectInstanceBase'
	var rot = objToRotation[loc][name];
	if (Array.isArray(rot)) {
	  if (rot.length == 2 && Array.isArray(rot[0]) && Array.isArray(rot[1])) {
	    let opt1 = rot[0], opt2 = rot[1]
            rot = qfromeuler(randomRng(opt1[0], opt2[0]), randomRng(opt1[1], opt2[1]), randomRng(opt1[2], opt2[2]))
	  } else {
            rot = rot[randomInt(0, rot.length)]
	  }
	}
	rot['agent'] = ''
	rot['frame'] = 'Global'
	object["attrs"]["classes"] = [ 
            {
                "uncertainty" : {
                    "typeName" : "::memoryx::DiscreteProbabilityBase",
                    "value" : {
                        "prob" : 1.0
                    }
                },
                "value" : {
                    "typeName" : "::armarx::StringVariantData",
                    "value" : name
                }
            }
        ]
	object["attrs"]["existenceCertainty"] = [ 
            {
                "value" : {
                    "typeName" : "::armarx::FloatVariantData",
                    "value" : 1.0
                }
            }
        ]
	object["attrs"]["orientation"] = [ 
            {
                "value" : {
                    "typeName" : "::armarx::FramedOrientationBase",
                    "value" : rot
                }
            }
        ]
        object["attrs"]["position"] = [ 
            {
                "uncertainty" : {
                    "typeName" : "::memoryx::MultivariateNormalDistributionBase",
                    "value" : {
                        "cov" : [ 
                            10000.0, 
                            0.0, 
                            0.0, 
                            0.0, 
                            10000.0, 
                            0.0, 
                            0.0, 
                            0.0, 
                            10000.0
                        ],
                        "mean" : [ 
                            0.0, 
                            0.0, 
                            0.0
                        ]
                    }
                },
                "value" : {
                    "typeName" : "::armarx::FramedPositionBase",
                    "value" : {
                        "agent" : "",
                        "frame" : "Global",
                        "x" : x,
                        "y" : y,
                        "z" : objZValues[loc][name]
                    }
                }
            }
        ]
        object["attrs"]["priority"] = [ 
            {
                "value" : {
                    "typeName" : "::armarx::IntVariantData",
                    "value" : 50.0
                }
            }
        ]
	return object
}

// load('xperience_filtered.objects.js')
const objects = ['appletea', 'cornycerealbars', 'coffeefilters', 'corn', 'multivitaminjuice', 'nesquik', 'oil', 'orangejuice', 'popcorn', 'vanillacookies', 'vitaliscereal', 'bluecup', 'greencup', 'redcup']

// object test:
// appletea		: sideboard (sometimes) 
// cornycerealbars	: sideboard, countertop
// coffeefilters	: sideboard, countertop, lowersideboard
// corn			: sideboard, countertop
// multivitaminjuice	: sideboard, countertop, lowersideboard
// nesquik		: sideboard, countertop, lowersideboard
// oil			: sideboard, countertop, lowersideboard
// orangejuice		: sideboard, countertop
// popcorn		: sideboard, countertop, lowersideboard
// vanillacookies	: sideboard, countertop, lowersideboard
// vitaliscereal	: sideboard, countertop, lowersideboard

// milk, pringles: recognition missing
// appletea: grasps need adjustments
// corneycerealbars, corn, orangejuice: doesn't work on lowersideboard

const locationRanges = {
  'sideboard': [4100, 4500, 6875, 7500],
  'countertop': [3700, 3800, 10300, 10400],
  'besidessink': [2800, 2900,  10250, 10350],
  'lowersideboard': [1950, 2050, 9750, 9850],
  'roundtable': [2750, 2920, 5700, 5950]
}
const locations = ['sideboard', 'countertop', 'besidessink', 'lowersideboard', 'roundtable']

for (var count of [1, 2, 3, 4, 5, 6, 8, 10]) {
  for (var i = 0; i < 1; i++) {
    for(var object of objects) {
      for (var loc of locations) {
        name = 'Snapshot_' + itos[count] + '_' + object + '_at_' + loc + '_' + i + '_x'
	print('creating ' + name)
	db.snapshots.insertOne({
	    "name" : name,
	    "segments" : {
		"activeOac" : name + "_activeOac",
		"affordances" : name + "_affordances",
		"agentInstances" : name + "_agentInstances",
		"environmentalPrimitives" : name + "_environmentalPrimitives",
		"objectClasses" : name + "_objectClasses",
		"objectInstances" : name + "_objectInstances",
		"objectRelations" : name + "_objectRelations",
		"worldState" : name + "_worldState"
	    }
	})
	name += '_objectInstances'
	db.Snapshot_planning_template_objectInstances.copyTo(name)
	var currentObj = object
	var currentLoc = loc
	var j = 0;
        var usedLocations = []
	var remainingObjects = Array.from(objects)
	shuffle(remainingObjects)
	objectLoop:
	while (j < count) {
	  remainingObjects.slice(remainingObjects.indexOf(currentObj), 1)
          print('Next object ' + j)
	  var x, y, locationTrials = 1
	  do {
            const locs = locationRanges[currentLoc]
	    const xmin = locs[0], xmax = locs[1], ymin = locs[2], ymax = locs[3]

            if (locationTrials % 10 == 0) {
              currentLoc = locations[(locations.indexOf(currentLoc) + 1) % locations.length]
	    }
            if (locationTrials == locations.length * 10 + 1) {
              currentLoc = 'sideboard'
	      print('Fallback to sideboard' + locationTrials)
	    }
            if (locationTrials >= (locations.length + 4) * 10) {
	      print('Too many trials, continue objectLoop' + locationTrials)
              j++
	      continue objectLoop
	    }
            locationTrials++
	    x = Math.random() * (xmax - xmin) + xmin
	    y = Math.random() * (ymax - ymin) + ymin
	  } while (isTooClose(x, y, usedLocations));
	  print('Using (' + x + ', ' + y + ') ' + currentLoc)
	  usedLocations.push([x, y])
	  db.getCollection(name).insertOne(createObject(currentObj, currentLoc, x, y, 14 + j))

          j++
	  currentObj = remainingObjects[(remainingObjects.indexOf(currentObj) + 1) % remainingObjects.length]
	  if (currentLoc != 'sideboard' || Math.random() > 0.6) {
	    currentLoc = locations[(locations.indexOf(currentLoc) + 1) % locations.length]
	  }
        }
      }
    }
  }
}







