
import sys
import time
import subprocess
from pathlib import Path

out_file = Path(sys.argv[1])
# Timeout seconds to wait when there is no progress. 
# Progress is monitored using the "New step" output of SpoacExecutive
timeout_seconds = float(sys.argv[2])

timed_out = False

def timeout(loud=True):
    global timed_out
    if loud:
        print('Timeout!')
    try:
        subprocess.run(['python', 'force_export.py'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=False)
    except Exception as e:
        print('Failed to force export episode', e)
        pass
    timed_out = True
    

max_end_time = time.time() + timeout_seconds

step_history = set()

while not timed_out:
    if not out_file.is_file():
        # Try again, log file will probably be created soon
        time.sleep(1)
        continue

    with out_file.open('r') as observed_file:
        try:
            lines = observed_file.readlines()
        except BaseException as e:
            print('Execution failed! Failure to read ArmarX Log File.', e)
            timeout(loud=False)
            break
        if any('SpoacExecutive]: all goals reached, waiting for new ones' in line for line in lines):
            print('Execution successful!')
            break
        for line in lines:
            if 'SpoacExecutiveApp][void spoac::Executive::run()]: New step' in line:
                step = line[line.find('New step: ') + len('New step: '):]
                # Only grant new timeout if the step is new.
                # This is to avoid being stuck in action retry cycles
                if step not in step_history:
                    print(f'  New step {step[1:step.find(";")]}, extending timeout', flush=True)
                    max_end_time = time.time() + timeout_seconds
                    step_history.add(step)
        if any('HumanAssistedLocationReplacement]: Waiting for answer' in line 
                or 'could not find plan for current task, aborting...' in line
                or 'Failed to open snapshot' in line
                for line in lines):
            timeout(loud=False)
            print('Execution failed!')
            break

    time.sleep(1)

    if time.time() > max_end_time:
        timeout()

