#!/bin/bash

export SIM_DEST=~/Documents/simulation-dataset
export SIM_NAME="$(date +%d_%m_%y)_$(hostname -s)"

./simulation_run.sh scenes_$SIM_NAME.txt >> simulation_log_$SIM_NAME.log

