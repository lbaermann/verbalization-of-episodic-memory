#!/bin/bash

export SIM_DEST=~/Documents/simulation-dataset

for s in $1
do
	armarx memory start
	armarx start
	name=${s##*.}
	export SIM_NAME="$(date +%d_%m_%y)_$(hostname -s)_$name"
	echo $SIM_NAME
	./simulation_run.sh $s >> logs/simulation_log_$SIM_NAME.log
	armarx reset
	armarx memory stop
done

