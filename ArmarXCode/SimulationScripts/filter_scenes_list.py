
from pathlib import Path
from collections import defaultdict
import random
import sys

max_len_per_snapshot = 10
infile = Path(sys.argv[1])
lines = infile.read_text().splitlines()

group_by_snapshot = defaultdict(list)
for l in lines:
    s = l[:l.find(' ')]
    group_by_snapshot[s].append(l)
print(f'{len(group_by_snapshot)} Snapshots with {len(lines)} simulations in total', file=sys.stderr)

for s, v in group_by_snapshot.items():
    if len(v) > max_len_per_snapshot:
        group_by_snapshot[s] = random.sample(v, k=max_len_per_snapshot)

result = [l for v in group_by_snapshot.values() for l in v]
result.sort()
for l in result:
    print(l)
