#!/bin/bash

if [[ -z "$1" ]] ; then
  export name="$(date +%d_%m_%y)_$(hostname -s)"
  export log=logs/simulation_log_$name.log
else
  export name=$1
  export log=logs/simulation_log_$name.log
fi
state='echo Successful $(grep -c successful $log) / Executed $(egrep -c "Execution|Timeout" $log )' # / Total $(cat scenes_$name.txt | wc -l)'
last='x=$(tput lines); let n=x-9; tail -n $n $log'
started_time='echo -e "Started: \t$(sed "2q;d" $log)"'
current_time='echo -e "Now: \t\t$(date)"'
update_time='echo -e "Last modified: \t$(date -r $log)"'

watch -t -n1 -x bash -c "echo && $last && echo && echo && echo $name && $state && $started_time && $current_time && $update_time"

