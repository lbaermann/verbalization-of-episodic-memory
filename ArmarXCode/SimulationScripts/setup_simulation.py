import sys
import subprocess
import os.path


def parse_goal_from_args(args):
    cmd = args[0]
    if cmd == 'move':
        obj = args[1]
        loc = args[2]
        return f'(existsK(?loc : loc_{loc}, ?obj : obj_{obj}) K(objectAt(?obj, ?loc)))'
    elif cmd == 'move2':
        obj1 = args[1]
        obj2 = args[2]
        loc = args[3]
        if len(args) > 4:
            loc2 = args[4]
            return f'(existsK(?l1 : loc_{loc}, ?l2 : loc_{loc2}, ?o1 : obj_{obj1}, ?o2 : obj_{obj2}) K(objectAt(?o1, ?l1)) & K(objectAt(?o2, ?l2)))'
        else:
            return f'(existsK(?loc : loc_{loc}, ?o1 : obj_{obj1}, ?o2 : obj_{obj2}) K(objectAt(?o1, ?loc)) & K(objectAt(?o2, ?loc)))'
    elif cmd == 'move3':
        obj1 = args[1]
        obj2 = args[2]
        obj3 = args[3]
        loc1 = args[4]
        loc2 = args[5]
        loc3 = args[6]
        return f'(existsK(?l1 : loc_{loc1}, ?l2 : loc_{loc2}, ?l3 : loc_{loc3}, ?o1 : obj_{obj1}, ?o2 : obj_{obj2}, ?o3 : obj_{obj3}) K(objectAt(?o1, ?l1)) & K(objectAt(?o2, ?l2)) & K(objectAt(?o3, ?l3)))'
    elif cmd == 'move2grasp':
        obj1 = args[1]
        obj2 = args[2]
        loc1 = args[3]
        loc2 = args[4]
        obj3 = args[5]
        hand = args[6] # left or right
        return f'(existsK(?a1 : obj_agent_robot, ?l1 : loc_{loc1}, ?l2 : loc_{loc2}, ?o1 : obj_{obj1}, ?o2 : obj_{obj2}, ?o3 : obj_{obj3}, ?h1 : obj_{hand}hand) K(objectAt(?o1, ?l1)) & K(objectAt(?o2, ?l2)) & K(grasped(?a1, ?h1, ?o3)))'
    elif cmd == 'grasp':
        obj = args[1]
        hand = args[2]  # left or right
        return f'(existsK(?a1 : obj_agent_robot, ?h1: obj_{hand}hand, ?o1 : obj_{obj}) K(grasped(?a1, ?h1, ?o1)))'
    elif cmd == 'grasp2':
        obj1 = args[1]
        obj2 = args[2]
        return f'(existsK(?a1 : obj_agent_robot, ?hl: obj_lefthand, ?hr: obj_righthand, ?o1 : obj_{obj1}, ?o2 : obj_{obj2}) K(grasped(?a1, ?hl, ?o1)) & K(grasped(?a1, ?hr, ?o2))'
    else:
        assert False, f'Unknown command "{cmd}"'


def change_config(cfg_file, prop, value):
    with open(cfg_file, 'r') as f:
        lines = f.readlines()
    for i, line in enumerate(list(lines)):
        if line.startswith(prop):
            lines[i] = f'{prop} = {value}\n'
            break
    with open(cfg_file, 'w') as f:
        f.writelines(lines)


scene = sys.argv[1]
if scene.endswith('_objectInstances'):
    print('Common error: Snapshot name should not end with _objectInstances !', file=sys.stderr)
    exit(1)

snapshot = f'Snapshot_{scene}'
goal = parse_goal_from_args(sys.argv[2:])
scenarios = os.path.expanduser('~/armarx.projects/verbalization-of-episodic-memory/ArmarXCode/VerbalizationOfEpisodicMemory/scenarios')
change_config(f'{scenarios}/TaskProvider/config/ExampleTaskProviderApp.cfg', 'ArmarX.ExampleTaskProvider.Goal', goal)
change_config(f'{scenarios}/Armar3PlanningSimulation/config/SimulatorApp.cfg', 'ArmarX.Simulator.LongtermMemory.SnapshotName', snapshot) 
#change_config(f'{scenarios}/Armar3PlanningSimulation/config/WorkingMemory.cfg', 'MemoryX.WorkingMemory.SnapshotToLoad', snapshot)

if scene.endswith('_x') or scene.startswith('Xperience'):
    executive_mem_snapshot = snapshot
else:
    executive_mem_snapshot = 'Snapshot_XperienceDemoRM'
change_config(f'{scenarios}/Armar3PlanningTest/config/ExecutiveMemoryInitializerApp.cfg', 'ArmarX.ExecutiveMemoryInitializer.InitialSnapshot', executive_mem_snapshot) 


print(scene)

