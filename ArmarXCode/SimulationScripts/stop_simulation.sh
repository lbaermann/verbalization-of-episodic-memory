
scenarios=~/armarx.projects/verbalization-of-episodic-memory/ArmarXCode/VerbalizationOfEpisodicMemory/scenarios

armarx scenario stop $scenarios/Armar3EpisodeRecorder
armarx scenario stop $scenarios/Armar3PlanningSimulation
armarx scenario stop $scenarios/Armar3PlanningTest
armarx scenario stop $scenarios/Armar3PlanningSkills
armarx scenario stop $scenarios/TaskProvider

sleep 3

