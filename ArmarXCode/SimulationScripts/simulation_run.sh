#!/bin/bash
armarx start
mkdir /tmp/EMExport
echo "Leonard Episode Recording" > /tmp/norebootreason
set -e

if [[ -z "$DISPLAY" ]] ; then
  echo Variable DISPLAY is not set, simulation will not work as expected!
  exit 1
fi
if [[ -z "$SIM_DEST" ]] ; then
  echo Variable SIM_DEST is not set! Specify directory where to place simulation results.
  exit 1
fi
if [[ -z "$SIM_NAME" ]] ; then
  echo Variable SIM_NAME is not set! Specify name of this run.
  exit 1
fi

date

input=$1
total=$( grep -c -v '#' $input )
let i=1
while IFS= read -r line
do

  if [[ $line == \#* ]]; then
    continue
  fi

  echo "Simulating ($i / $total): $line"
  echo "Leonard Episode Recording (Progress: $i / $total)" > /tmp/norebootreason

  scene=$( python3 setup_simulation.py $line )
  out="logs/$scene.log"
  ./start_simulation.sh &> "$out" &

  source ~/py-venvs/armarx/bin/activate
  python3 wait_for_episode_success_or_timeout.py "$out" 120
  deactivate

  ./stop_simulation.sh > /dev/null
  armarx killAll > /dev/null

  let i++
done < "$input"

date

cd /tmp/EMExport
tar czf $SIM_DEST/episodes_$SIM_NAME.tar.gz episode*
rm /tmp/norebootreason

armarx stop

