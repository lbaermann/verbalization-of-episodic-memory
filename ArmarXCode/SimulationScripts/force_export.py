
from armarx.slice_loader import load_armarx_slice
from armarx.ice_manager import get_topic, ice_communicator

load_armarx_slice('RobotAPI', 'speech/SpeechInterface.ice')

from armarx import TextListenerInterfacePrx


def force_episode_export():
    dialogInputPublisher = get_topic(TextListenerInterfacePrx, 'Speech_Commands')
    dialogInputPublisher.reportText('export now')


if __name__ == '__main__':
    force_episode_export()

