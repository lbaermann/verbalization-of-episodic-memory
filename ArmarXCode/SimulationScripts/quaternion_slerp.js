
// https://github.com/mrdoob/three.js/blob/9602dee62612a9c2a34795711159abe0afae6317/src/math/Quaternion.js#L516

/*function slerp(ax, ay, az, aw, bx, by, bz, bw, t ) {

	const EPSILON = 0.00001;
	var result = {
		'qw': aw,
		'qx': ay,
		'qy': az,
		'qz': aw
	};
	const b = {
		'qw': bx,
		'qx': by,
		'qy': bz,
		'qz': bw
	};
	
	if ( t === 0 ) return result;
	if ( t === 1 ) return b;

	// http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/

	let cosHalfTheta = aw * bw + ax * bx + ay * by + az * bz;

	if ( cosHalfTheta < 0 ) {

		result['qw'] = - bw;
		result['qx'] = - bx;
		result['qy'] = - by;
		result['qz'] = - bz;

		cosHalfTheta = - cosHalfTheta;

	} else {

		result = b;

	}

	if ( cosHalfTheta >= 1.0 ) {

		result['qw'] = aw;
		result['qx'] = ax;
		result['qy'] = ay;
		result['qz'] = az;

		return result;

	}

	const sqrSinHalfTheta = 1.0 - cosHalfTheta * cosHalfTheta;

	if ( sqrSinHalfTheta <= EPSILON ) {

		const s = 1 - t;
		result['qw'] = s * aw + t * result['qw'];
		result['qx'] = s * ax + t * result['qx'];
		result['qy'] = s * ay + t * result['qy'];
		result['qz'] = s * az + t * result['qz'];

		let l = Math.sqrt( result['qx'] * result['qx'] + result['qy'] * result['qy'] + result['qz'] * result['qz'] + result['qw'] * result['qw'] )
		if ( l === 0 ) {
			result['qx'] = 0;
			result['qy'] = 0;
			result['qz'] = 0;
			result['qw'] = 1;
		} else {
			l = 1 / l;
			result['qx'] = result['qx'] * l;
			result['qy'] = result['qy'] * l;
			result['qz'] = result['qz'] * l;
			result['qw'] = result['qw'] * l;
		}
		return result;

	}

	const sinHalfTheta = Math.sqrt( sqrSinHalfTheta );
	const halfTheta = Math.atan2( sinHalfTheta, cosHalfTheta );
	const ratioA = Math.sin( ( 1 - t ) * halfTheta ) / sinHalfTheta,
		ratioB = Math.sin( t * halfTheta ) / sinHalfTheta;

	result['qw'] = ( aw * ratioA + result['qw'] * ratioB );
	result['qx'] = ( ax * ratioA + result['qx'] * ratioB );
	result['qy'] = ( ay * ratioA + result['qy'] * ratioB );
	result['qz'] = ( az * ratioA + result['qz'] * ratioB );

	return result;

}

function qslerp(a, b, t) {
	return slerp(a['qx'], a['qy'], a['qz'], a['qw'], b['qx'], b['qy'], b['qz'], b['qw'], t);
}*/

function qfromeuler( rx, ry, rz ) {

	// http://www.mathworks.com/matlabcentral/fileexchange/
	// 	20696-function-to-convert-between-dcm-euler-angles-quaternions-and-euler-vectors/
	//	content/SpinCalc.m

	rx = rx / 180 * Math.PI
	ry = ry / 180 * Math.PI
	rz = rz / 180 * Math.PI

	const cos = Math.cos;
	const sin = Math.sin;

	const c1 = cos( rx / 2 );
	const c2 = cos( ry / 2 );
	const c3 = cos( rz / 2 );

	const s1 = sin( rx / 2 );
	const s2 = sin( ry / 2 );
	const s3 = sin( rz / 2 );

	let qx = s1 * c2 * c3 - c1 * s2 * s3;
	let qy = c1 * s2 * c3 + s1 * c2 * s3;
	let qz = c1 * c2 * s3 - s1 * s2 * c3;
	let qw = c1 * c2 * c3 + s1 * s2 * s3;

	return {
		'qw': qw,
		'qx': qx,
		'qy': qy,
		'qz': qz
	};
}


