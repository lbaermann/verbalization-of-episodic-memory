
from pymongo import MongoClient
import sys
from random import random
c = MongoClient('localhost', int(sys.argv[1]))
m = c.memdb

used_objects = ['appletea', 'cornycerealbars', 'coffeefilters', 'corn', 'multivitaminjuice', 'nesquik', 'oil', 'orangejuice', 'popcorn', 'vanillacookies', 'vitaliscereal', 'bluecup', 'greencup', 'redcup']


snapshots = [name for name in m.list_collection_names() if name.startswith('Snapshot') and name.endswith('_x_objectInstances')]
for s in snapshots:
    all_obj_names = [obj['name'] for obj in m[s].find()]
    obj_names = [name for name in all_obj_names if name in used_objects]

    s = s.replace('Snapshot_', '').replace('_objectInstances', '')
    if len(obj_names) == 0: 
        continue

    if '_at_' in s:
        start_loc = s[s.find('at_') + 3: s.find('_', s.find('at_') + 3)]
        locations = ['countertop', 'sink', 'roundtable', 'placesetting2']
        if start_loc in locations:
            locations.remove(start_loc)
        if start_loc == 'besidessink':
            locations.remove('sink')
        for obj in obj_names:
            print(s, 'grasp', obj, 'left')
            print(s, 'grasp', obj, 'right')
            for loc in locations:
                print(s, 'move', obj, loc)
    else:
        for obj in obj_names:
            print(s, 'move', obj, 'countertop')
            print(s, 'move', obj, 'sink')
            print(s, 'grasp', obj, 'left')
            print(s, 'grasp', obj, 'right')

    if len(obj_names) == 1:
        continue

    locations = ['countertop', 'sink', 'roundtable', 'placesetting2']
    for o1 in obj_names:
        for o2 in obj_names:
            if o1 == o2: 
                continue
            print(s, 'grasp2', o1, o2)
            for l1 in locations:
                for l2 in locations:
                    print(s, 'move2', o1, o2, l1, l2)

    if len(obj_names) == 2:
        continue

    for o1 in obj_names:
        for o2 in obj_names:
            for o3 in obj_names:
                if o1 == o2 or o2 == o3 or o1 == o3:
                    continue
                for l1 in locations:
                    for l2 in locations:
                        print(s, 'move2grasp', o1, o2, l1, l2, o3, 'left')
                        print(s, 'move2grasp', o1, o2, l1, l2, o3, 'right')
                        for l3 in locations:
                            print(s, 'move3', o1, o2, o3, l1, l2, l3)

