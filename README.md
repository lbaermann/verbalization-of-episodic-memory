# Deep Epiosidc Memory for Verbalization of Robot Experience

This repository contains the code to reproduce the results of 
the paper "Deep Epiosidc Memory for Verbalization of Robot Experience" [Link will follow].

For the dataset and models as used in the paper, please refer to this [google drive link](https://drive.google.com/drive/folders/1zU23Bo99FLrsyELFwJ1fEIRmwcD_s97n?usp=sharing).

## Abstract

The ability to verbalize robot experience in natural language is key for a symbiotic human-robot interaction. 
While first works approached this problem using template-based verbalization on symbolic episode data only, we explore a novel way in which deep learning methods are used for the creation of an episodic memory from experiences as well as the verbalization of such experience in natural language.
To this end, we first collected a complex dataset consisting of more than a thousand multimodal robot episode recordings both from simulation as well as real robot executions, together with representative natural language questions and answers about the robot's past experience. 
Second, we propose and evaluate an episodic memory verbalization model consisting of a speech encoder and decoder based on the Transformer architecture, combined with an LSTM-based episodic memory auto-encoder, and evaluate the model on simulated and real data from robot execution examples.
Our experimental results provide a proof-of-concept for episodic-memory-based verbalization of robot experience.


## Brief code introduction

### Overview

This repository contains two top-level folders:
- `em_verbalization_net` contains the code for training and evaluating the models,
  based on the fairseq framework. It also contains the code for generating the dialog data.
- `ArmarXCode` contains scripts and ArmarX scenarios to reproduce the simulated episode dataset.
  

### em_verbalization_net

Our code is based on the [fairseq](https://github.com/pytorch/fairseq) framework.
First, setup your python environment according to the `requirements.txt` file.
Then, the provided data needs to be preprocessed.
Afterwards, you can train and evaluate the model.

#### Preprocessing, Training, Evaluation

To preprocess the data to train the EMV model, `cd experiments/deep_emv`, adjust the dataset path in `preprocess.sh` and run it.
For training the deep EM auto-encoder model only, do the same thing in `experiment/deep_em_autoenc`.
Also take a look at `experiments/preprocess_emv_dataset.sh` to see what files are assumed to exist.

To train a model, use `experiments/deep_emv/deep_emv.sh` (or `deep_em_autoenc.sh` in its directory).
Evaluation is done using the corresponding `<...>_eval.sh` script.
Check its content to see which variables need to be set / can be configured.

To generate responses on the small, additional test sets (human-annotated and real-robot recordings), 
use `final_test.sh`. Evaluation of the responses needs to be done by hand in this case.

#### Natural language data generation

For creating the grammar-generated natural language part of the dataset, invoke the help of `data_generator/main.py`.
The actual grammar can be adjusted or extended in `data_generator/episode_grammar.py`.

#### Model implementation

Models, tasks and dataset logic are implemented in the `model` folder and follow the fairseq class structure.
- `emv_dataset.py`, in conjuction with `em/dictionary.py` and `em/episode_dataset.py`, handles loading
  and batching the multimodal dataset.
- `emv_model.py` defines the EMV model with its two parallel encoders (speech and episode data),
  the speech decoder as well as the supplementary decoders.
- `emv_task.py` coordinates the training process by overriding many methods of `FairseqTask`.
- The `em` package contains different variants of episode encoders, as well as loss functions 
  and the auto-encoder model.
- The `t5` package contains a port of the 
  [huggingface T5 implementation](https://huggingface.co/transformers/model_doc/t5.html) 
  to fairseq, as well
  as specialized EMV task and model implementations using pretrained T5 models.

#### Scripted evaluation

The scripts for evaluation can be found in the `eval` directory. They are applied on the `fairseq-generate` output
files (`deep_emv_eval.sh` already invokes them through `_eval_generate_output.sh`). Note that all of these scripts can
only be applied to the grammar-generated speech data. Answers to human-annotated questions need to be evaluated by hand.

- `calc_gt_score.py` is the simplest script. It just checks how many outputs are equal to the target response. This
  score just exists as a lower bound for the other scripts.
- `semantic_eval.py` has some heuristics built in to check whether a model hypothesis is correct with respect to a given
  question and a target utterance. It does not inspect the episode data and therefore is only a quick approximation.
  It's output serves as a lower bound for the correct category of `categorizing_eval.py`. Additionally, this script is
  used during validation, since it provides a good approximation but is much faster.
- `categorizing_eval.py` basically reverse-engineers the dataset-generating grammar to asses to which evaluation
  category a hypothesis belongs, given the query, target and episode content. This is the script used to evaluate the
  results in our paper. It also calculates information precision and recall.

### ArmarXCode

This code is needed to (re-)create the simulated episode dataset in the ArmarX simulation.
If you do not want to install ArmarX, just use our published dataset provided by the link above.
Otherwise, the following steps are necessary:
1. To install ArmarX, please refer to its [documentation](https://armarx.humanoids.kit.edu/ArmarXDoc-armarx-installation-ubuntu-18-04.html).
2. Import the mongodb snapshot provided in `ArmarXCode/memdb` using the mongorestore tool.
3. `cd ArmarXCode/SimulationScripts`
3. Create many scenes by adjusting `create_scene_snapshots.js` and run 
   it using `mongo -d memdb create_scene_snaphsots.js`
4. Adjust and run `make_scenes_list.py > my_scene_list.txt`, 
   optionally filter using `filter_scenes_list.py`
5. Run the simulation using `simulation_run.sh my_scene_list.txt`.
   Also take a look at `split_simulation_run.sh` to see how it can be used in an 
   automated way.
